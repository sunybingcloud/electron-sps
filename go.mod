module bitbucket.org/sunybingcloud/electron

go 1.12

require (
	github.com/fatih/color v1.9.0
	github.com/gogo/protobuf v1.3.1
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b // indirect
	github.com/golang/protobuf v1.4.2
	github.com/google/uuid v1.1.2 // indirect
	github.com/jinzhu/copier v0.0.0-20190924061706-b57f9002281a
	github.com/mash/gokmeans v0.0.0-20170215130432-ea22cff45f59
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/mesos/mesos-go v0.0.11
	github.com/montanaflynn/stats v0.6.3
	github.com/pborman/uuid v1.2.1 // indirect
	github.com/pkg/errors v0.9.1
	github.com/pradykaushik/task-ranker v0.7.0
	github.com/prometheus/common v0.14.0 // indirect
	github.com/robfig/cron/v3 v3.0.1
	github.com/sajari/regression v1.0.1
	github.com/samuel/go-zookeeper v0.0.0-20200724154423-2164a8ac840e // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.6.0
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	golang.org/x/net v0.0.0-20200923182212-328152dc79b1 // indirect
	golang.org/x/sys v0.0.0-20200923182605-d9f96fdee20d // indirect
	gonum.org/v1/gonum v0.8.1 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
