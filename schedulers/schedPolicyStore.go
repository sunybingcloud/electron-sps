package schedulers

import (
	"bitbucket.org/sunybingcloud/electron/def"
	taskQFactory "bitbucket.org/sunybingcloud/electron/taskQueue/factory"
	"bitbucket.org/sunybingcloud/electron/utilities"
	"encoding/json"
	sched "github.com/mesos/mesos-go/api/v0/scheduler"
	"github.com/pkg/errors"
	"os"
	"sort"
)

// A copy of the ElectronScheduler instance for use by other modules if necessary.
var electronScheduler ElectronScheduler

func GetElectronSchedulerInstance() ElectronScheduler {
	return electronScheduler
}

// Names of different scheduling policies.
const (
	FF                            = "first-fit"
	FF_SORTED_WATTS               = FF + "_sorted-watts"
	FF_SORTED_OFFERS              = FF + "_sorted-offers"
	FF_SORTED_WATTS_SORTED_OFFERS = FF_SORTED_WATTS + "_sorted-offers"
	BP                            = "bin-packing"
	BP_SORTED_OFFERS              = BP + "_sorted-offers"
	BOTTOM_HEAVY                  = "bottom-heavy"
	MGM                           = "max-greedymins"
	MM                            = "max-min"
	TOP_HEAVY                     = "top-heavy"
	TOLERANCE_AWARE_SCHEDULER     = "tolerance-aware-scheduler"
	EDF                           = "earliest-deadline-first"
)

// Scheduling policy factory
var SchedPolicies = map[string]SchedPolicyState{
	FF:                            &FirstFit{},
	FF_SORTED_WATTS:               &FirstFitSortedWatts{},
	FF_SORTED_OFFERS:              &FirstFitSortedOffers{},
	FF_SORTED_WATTS_SORTED_OFFERS: &FirstFitSortedWattsSortedOffers{},
	BP:                            &BinPackSortedWatts{},
	BP_SORTED_OFFERS:              &BinPackSortedWattsSortedOffers{},
	BOTTOM_HEAVY:                  &BottomHeavy{},
	MGM:                           &MaxGreedyMins{},
	MM:                            &MaxMin{},
	TOP_HEAVY:                     &TopHeavy{},
	TOLERANCE_AWARE_SCHEDULER:     &ToleranceAwareScheduler{},
	EDF:                           &EarliestDeadlineFirst{},
}

// TaskQueues compatible with scheduling policies.
var CompatibleTaskQTypes = map[string]string{
	FF:                            taskQFactory.FCFS_Q,
	FF_SORTED_WATTS:               taskQFactory.FCFS_SORTABLE_Q,
	FF_SORTED_OFFERS:              taskQFactory.FCFS_Q,
	FF_SORTED_WATTS_SORTED_OFFERS: taskQFactory.FCFS_SORTABLE_Q,
	BP:                            taskQFactory.FCFS_SORTABLE_Q,
	BP_SORTED_OFFERS:              taskQFactory.FCFS_SORTABLE_Q,
	BOTTOM_HEAVY:                  taskQFactory.FCFS_Q,
	MGM:                           taskQFactory.FCFS_SORTABLE_Q,
	MM:                            taskQFactory.FCFS_SORTABLE_Q,
	TOP_HEAVY:                     taskQFactory.FCFS_Q,
	TOLERANCE_AWARE_SCHEDULER:     taskQFactory.FCFS_Q,
	EDF:                           taskQFactory.PRIORITY_Q,
}

// Tasks compatible with the task queues.
var CompatibleTaskTypes = map[string]string{
	FF:                        def.SIMPLE_TASK,
	FF_SORTED_WATTS:           def.SIMPLE_TASK,
	FF_SORTED_OFFERS:          def.SIMPLE_TASK,
	BP:                        def.SIMPLE_TASK,
	BP_SORTED_OFFERS:          def.SIMPLE_TASK,
	BOTTOM_HEAVY:              def.SIMPLE_TASK,
	MGM:                       def.SIMPLE_TASK,
	MM:                        def.SIMPLE_TASK,
	TOP_HEAVY:                 def.SIMPLE_TASK,
	TOLERANCE_AWARE_SCHEDULER: def.SIMPLE_TASK,
	EDF:                       def.DEADLINE_SENSITIVE_TASK,
}

// Scheduling policies to choose when switching
var schedPoliciesToSwitch = make(map[int]struct {
	spName string
	sp     SchedPolicyState
})

// Initialize scheduling policy characteristics using the provided config file.
func InitSchedPolicyCharacteristics(schedPoliciesConfigFilename string) error {
	var schedPolConfig map[string]baseSchedPolicyState
	if file, err := os.Open(schedPoliciesConfigFilename); err != nil {
		return errors.Wrap(err, "Error opening file")
	} else {
		err := json.NewDecoder(file).Decode(&schedPolConfig)
		if err != nil {
			return errors.Wrap(err, "Error unmarshalling")
		}

		// Initializing.
		// TODO: Be able to unmarshal a schedPolConfig JSON into any number of scheduling policies.
		for schedPolName, schedPolState := range SchedPolicies {
			switch t := schedPolState.(type) {
			case *FirstFit:
				t.TaskDistribution = schedPolConfig[schedPolName].TaskDistribution
				t.VarianceCpuSharePerTask = schedPolConfig[schedPolName].VarianceCpuSharePerTask
			case *BinPackSortedWatts:
				t.TaskDistribution = schedPolConfig[schedPolName].TaskDistribution
				t.VarianceCpuSharePerTask = schedPolConfig[schedPolName].VarianceCpuSharePerTask
			case *MaxMin:
				t.TaskDistribution = schedPolConfig[schedPolName].TaskDistribution
				t.VarianceCpuSharePerTask = schedPolConfig[schedPolName].VarianceCpuSharePerTask
			case *MaxGreedyMins:
				t.TaskDistribution = schedPolConfig[schedPolName].TaskDistribution
				t.VarianceCpuSharePerTask = schedPolConfig[schedPolName].VarianceCpuSharePerTask
			}
		}

		// Initialize schedPoliciesToSwitch to allow binary searching for scheduling policy switching.
		spInformation := map[string]float64{}
		for spName, sp := range SchedPolicies {
			spInformation[spName] = sp.GetInfo().taskDist
		}
		spInformationPairList := utilities.GetPairList(spInformation)
		// Sorting spInformationPairList in non-increasing order of taskDist.
		sort.SliceStable(spInformationPairList, func(i, j int) bool {
			return spInformationPairList[i].Value < spInformationPairList[j].Value
		})
		// Initializing scheduling policies that are setup for switching.
		index := 0
		for _, spInformationPair := range spInformationPairList {
			if spInformationPair.Value != 0 {
				schedPoliciesToSwitch[index] = struct {
					spName string
					sp     SchedPolicyState
				}{
					spName: spInformationPair.Key,
					sp:     SchedPolicies[spInformationPair.Key],
				}
				index++
			}
		}

		// Initializing the next and previous policy based on the the round-robin ordering.
		// The next policy for policy at N would correspond to the value at index N+1 in schedPoliciesToSwitch.
		for curPolicyIndex := 0; curPolicyIndex < len(schedPoliciesToSwitch); curPolicyIndex++ {
			info := struct {
				nextPolicyName string
				prevPolicyName string
			}{}
			if curPolicyIndex == 0 {
				info.prevPolicyName = schedPoliciesToSwitch[len(schedPoliciesToSwitch)-1].spName
			} else {
				info.prevPolicyName = schedPoliciesToSwitch[curPolicyIndex-1].spName
			}
			info.nextPolicyName = schedPoliciesToSwitch[(curPolicyIndex+1)%len(schedPoliciesToSwitch)].spName
			schedPoliciesToSwitch[curPolicyIndex].sp.UpdateLinks(info)
		}
	}

	return nil
}

// build the scheduler with the options being applied
func buildScheduler(s sched.Scheduler, opts ...SchedulerOptions) {
	s.(ElectronScheduler).init(opts...)
}

func GetElectronScheduler(opts ...SchedulerOptions) sched.Scheduler {
	s := &BaseScheduler{}
	buildScheduler(s, opts...)
	// Copying interface values is okay here as the underlying value is a pointer to
	// schedulers.BaseScheduler. So, the pointer gets copied but there is only one object.
	// Note: If ElectronScheduler is not implemented by *BaseScheduler, then copying the
	// interface values is not going to work as a deep copy of BaseScheduler would be made.
	// Refer https://research.swtch.com/interfaces
	electronScheduler = s
	return s
}
