package schedulers

import (
	"bitbucket.org/sunybingcloud/electron/def"
	elecLogDef "bitbucket.org/sunybingcloud/electron/logging/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"bitbucket.org/sunybingcloud/electron/utilities/mesosUtils"
	"bitbucket.org/sunybingcloud/electron/utilities/offerUtils"
	"bytes"
	"fmt"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	sched "github.com/mesos/mesos-go/api/v0/scheduler"
	"log"
	"sort"
)

type BinPackSortedWattsSortedOffers struct {
	baseSchedPolicyState
}

func (s *BinPackSortedWattsSortedOffers) ConsumeOffers(spc SchedPolicyContext, driver sched.SchedulerDriver, offers []*mesos.Offer) {
	baseSchedRef := spc.(*BaseScheduler)
	switch baseSchedRef.tasks.(type) {
	case taskQueue.SortableTaskQueue:
		if baseSchedRef.schedPolSwitchEnabled {
			baseSchedRef.tasks.(taskQueue.SortableTaskQueue).SortN(baseSchedRef.numTasksInSchedWindow, def.SortByWatts)
		} else {
			baseSchedRef.tasks.(taskQueue.SortableTaskQueue).Sort(def.SortByWatts)
		}
	default:
		// Do nothing. Task queue is not sortable.
	}
	baseSchedRef.LogOffersReceived(offers)

	// Sorting the offers
	sort.Sort(offerUtils.OffersSorter(offers))

	// Printing the sorted offers and the corresponding CPU resource availability
	buffer := bytes.Buffer{}
	buffer.WriteString(fmt.Sprint("Sorted Offers:\n"))
	for i := 0; i < len(offers); i++ {
		offer := offers[i]
		offerUtils.UpdateEnvironment(offer)
		offerCPU, _ := offerUtils.OfferAgg(offer)
		buffer.WriteString(fmt.Sprintf("Offer[%s].CPU = %f\n", offer.GetHostname(), offerCPU))
	}
	baseSchedRef.Log(elecLogDef.GENERAL, buffer.String())

	for _, offer := range offers {
		if baseSchedRef.ShutdownScheduling.IsClosed() {
			baseSchedRef.LogNoPendingTasksDeclineOffers(offer)
			driver.DeclineOffer(offer.Id, mesosUtils.LongFilter)
			baseSchedRef.LogNumberOfRunningTasks()
			continue
		}

		tasks := []*mesos.TaskInfo{}

		offerTaken := false
		totalCPU := 0.0
		totalRAM := 0.0
		var iter taskQueue.Iterator
		for iter = baseSchedRef.tasks.BeginIter(); iter.HasNext(); {
			task := iter.NextTask()
			// Don't take offer if it doesn't match our task's host requirement
			if offerUtils.HostMismatch(*offer.Hostname, task.GetHost()) {
				continue
			}

			for task.GetInstances() > 0 {
				// If scheduling policy switching enabled, then
				// stop scheduling if the #baseSchedRef.schedWindowSize tasks have been scheduled.
				if baseSchedRef.schedPolSwitchEnabled &&
					(s.numTasksScheduled >= baseSchedRef.schedWindowSize) {
					log.Printf("Stopped scheduling... Completed scheduling %d tasks.",
						s.numTasksScheduled)
					break // Offers will automatically get declined.
				}
				// Does the task fit
				if s.takePartlyConsumedOffer(offer, task, totalCPU, totalRAM) {

					offerTaken = true
					totalCPU += task.GetCpu()
					totalRAM += task.GetRam()
					baseSchedRef.LogCoLocatedTasks(offer.GetSlaveId().GoString())
					taskToSchedule := baseSchedRef.newTaskInfo(offer, task, s.resourcesToReserve(task, offer))
					tasks = append(tasks, taskToSchedule)

					baseSchedRef.LogSchedTrace(taskToSchedule, offer)
					task.DecrementInstances()
					s.numTasksScheduled++

					if task.GetInstances() <= 0 {
						// All instances of task have been scheduled, remove it
						iter.RemoveTraversedTask()

						if baseSchedRef.tasks.Len() <= 0 {
							baseSchedRef.ExamineShutdownOfScheduling()
						}
					}
				} else {
					break // Continue on to next task
				}
			}
		}

		if offerTaken {
			baseSchedRef.LogTaskStarting(nil, offer)
			LaunchTasks([]*mesos.OfferID{offer.Id}, tasks, driver)
		} else {

			// If there was no match for the task
			cpus, mem := offerUtils.OfferAgg(offer)
			baseSchedRef.LogInsufficientResourcesDeclineOffer(offer, cpus, mem)
			driver.DeclineOffer(offer.Id, mesosUtils.DefaultFilter)
		}
	}
}
