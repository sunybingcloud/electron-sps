package schedulers

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"bitbucket.org/sunybingcloud/electron/utilities/mesosUtils"
	"bitbucket.org/sunybingcloud/electron/utilities/offerUtils"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	sched "github.com/mesos/mesos-go/api/v0/scheduler"
	"github.com/pkg/errors"
	"math"
	"time"
)

// EarliestDeadlineFirst attempts to schedule tasks, stored in a priority queue,
// that are closest to their deadlines.
// If the task with the highest priority (closest to its deadline) cannot fit into any of
// the resource offers, then a NO-OP occurs and no task is allocated resources in the corresponding
// offer cycle.
// Note that unlike the classical EDF algorithm, this EDF does not support task preemption.
type EarliestDeadlineFirst struct {
	baseSchedPolicyState
	taskPtyUpdateSchedule string
}

const taskPtyUpdateSchedule = "0/5 * * * * *"

func (s *EarliestDeadlineFirst) Init(opts ...SchedPolicyOptions) {
	// Setting the task priority updates schedule to every 5 seconds.
	s.taskPtyUpdateSchedule = taskPtyUpdateSchedule
	s.baseSchedPolicyState.Init(opts...)
}

func (s EarliestDeadlineFirst) GetPriorityUpdateSchedule() string {
	return s.taskPtyUpdateSchedule
}

func (s EarliestDeadlineFirst) GetPriorityUpdateFunc() func(def.PriorityTask) {
	return func(task def.PriorityTask) {
		// -(Time to deadline in seconds). This would mean that a task closer to its deadline
		// would have a higher priority than a task that is farther from its deadline.
		// However, it is important to note that just using the remaining time (in seconds)
		// to determine the priority can result in unfairness by detrimentally impact wait-times
		// of tasks with relatively longer deadlines as the tasks with shorter deadlines would
		// end up getting a higher priority. This can in turn result in tasks with longer
		// deadlines not meeting their deadlines when the task queue consists of a large number
		// of tasks with short deadlines.
		now := time.Now()
		// Rounding up for safety.
		elapsedSeconds := int(math.Ceil(now.Sub(task.GetSubmittedTime()).Seconds()))
		var secondsToDeadline int
		if dst, ok := task.(*def.DeadlineSensitiveTask); ok {
			secondsToDeadline = *dst.SubmissionToDeadlineSeconds
		} else {
			// Should not be here.
		}
		secondsToDeadline -= -elapsedSeconds
		task.SetPriority(-secondsToDeadline)
	}
}

func (s *EarliestDeadlineFirst) ConsumeOffers(spc SchedPolicyContext, driver sched.SchedulerDriver, offers []*mesos.Offer) {
	baseSchedRef := spc.(*BaseScheduler)
	baseSchedRef.LogOffersReceived(offers)

	// Storing the Ids of the offers consumed.
	consumedOffers := make(map[string]struct{})
	// Store the tasks that fit into offers and the offer ID of the consumed offer.
	fitTasks := make(map[string][]*mesos.TaskInfo)
	// Resources consumed in each offer in this scheduling cycle.
	consumedResources := make(map[string]*struct {
		cpu float64
		ram float64
	})

	for _, offer := range offers {
		offerUtils.UpdateEnvironment(offer)
		consumedResources[*offer.Id.Value] = &struct {
			cpu float64
			ram float64
		}{0, 0}
	}

	// Decline all offers if scheduling is done (no more pending tasks).
	if baseSchedRef.ShutdownScheduling.IsClosed() {
		for _, offer := range offers {
			baseSchedRef.LogNoPendingTasksDeclineOffers(offer)
			_, err := driver.DeclineOffer(offer.Id, mesosUtils.LongFilter)
			if err != nil {
				baseSchedRef.LogElectronError(errors.Wrap(err, "failed to decline offer"))
			}
			continue
		}
		baseSchedRef.LogNumberOfRunningTasks()
		return
	}

	priorityTaskQ := baseSchedRef.tasks.(taskQueue.PriorityTaskQueue)
	nextTaskItem := priorityTaskQ.Peek()
	for nextTaskItem != nil {
		nextTask := nextTaskItem.GetTask()
		for _, offer := range offers {
			// Don't take the offer if it doesn't match our task's host requirement.
			if offerUtils.HostMismatch(*offer.Hostname, nextTask.GetHost()) {
				// No instance of this task can be fit into this offer.
				break // Move on to next task.
			}

			for nextTask.GetInstances() > 0 {
				// Does the task fit.
				consumedResourcesInOffer := consumedResources[*offer.Id.Value]
				if s.takePartlyConsumedOffer(offer, nextTask, consumedResourcesInOffer.cpu, consumedResourcesInOffer.ram) {

					// Updating the consumed resources in the offer.
					consumedResourcesInOffer.cpu += nextTask.GetCpu()
					consumedResourcesInOffer.ram += nextTask.GetRam()
					// Marking the offer as consumed.
					consumedOffers[*offer.Id.Value] = struct{}{}

					baseSchedRef.LogCoLocatedTasks(offer.GetSlaveId().GoString())
					taskToSchedule := baseSchedRef.newTaskInfo(offer, nextTask, s.resourcesToReserve(nextTask, offer))
					// Caching the task to schedule for the corresponding offer.
					fitTasks[*offer.Id.Value] = append(fitTasks[*offer.Id.Value], taskToSchedule)

					// One instance of the task has been consumed.
					nextTask.DecrementInstances()
					s.numTasksScheduled++
				} else {
					// Task does not fit into the offer.
					break // Move on to next offer.
				}
			}

			// If all the instances of the task have been scheduled, then
			// remove them task from the queue.
			if nextTask.GetInstances() <= 0 {
				break // Move on to next task.
			} else {
				// Move on to next offer.
			}
		}

		// Removing task from queue if all the instances have been allocated resources.
		if nextTask.GetInstances() <= 0 {
			priorityTaskQ.Pop()
			nextTaskItem = priorityTaskQ.Peek()
			// Examine whether scheduling needs to be shutdown.
			if priorityTaskQ.IsEmpty() {
				baseSchedRef.ExamineShutdownOfScheduling()
			}
		}
	}

	for _, offer := range offers {
		if _, ok := consumedOffers[*offer.Id.Value]; ok {
			for _, t := range fitTasks[*offer.Id.Value] {
				baseSchedRef.LogSchedTrace(t, offer)
			}
			LaunchTasks([]*mesos.OfferID{offer.Id}, fitTasks[*offer.Id.Value], driver)
		} else {
			// Offer was not consumed.
			// Declining offer.
			cpus, mem := offerUtils.OfferAgg(offer)
			baseSchedRef.LogInsufficientResourcesDeclineOffer(offer, cpus, mem)
			_, err := driver.DeclineOffer(offer.Id, mesosUtils.DefaultFilter)
			if err != nil {
				baseSchedRef.LogElectronError(errors.Wrap(err, "failed to decline offer"))
			}
		}
	}
}
