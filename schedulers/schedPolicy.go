package schedulers

import (
	"bitbucket.org/sunybingcloud/electron/def"
	elecLogDef "bitbucket.org/sunybingcloud/electron/logging/def"
	"bitbucket.org/sunybingcloud/electron/metrics"
	"bitbucket.org/sunybingcloud/electron/utilities/offerUtils"
	"fmt"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	"github.com/mesos/mesos-go/api/v0/mesosutil"
	sched "github.com/mesos/mesos-go/api/v0/scheduler"
	"github.com/pkg/errors"
	"github.com/pradykaushik/task-ranker/entities"
	"github.com/pradykaushik/task-ranker/strategies"
	"log"
	"strings"
	"time"
)

type SchedPolicyContext interface {
	// Change the state of scheduling.
	SwitchSchedPol(s SchedPolicyState)
	// Is a receiver of the results of task ranking.
	strategies.TaskRanksReceiver
}

type SchedPolicyState interface {
	// Initialization.
	Init(...SchedPolicyOptions)
	// Define the particular scheduling policy's methodology of resource offer consumption.
	ConsumeOffers(SchedPolicyContext, sched.SchedulerDriver, []*mesos.Offer)
	// Get information about the scheduling policy.
	GetInfo() (info struct {
		taskDist       float64
		varCpuShare    float64
		nextPolicyName string
		prevPolicyName string
	})
	// Update links to next and previous scheduling policy.
	UpdateLinks(info struct {
		nextPolicyName string
		prevPolicyName string
	})
	// Switch scheduling policy if necessary.
	SwitchIfNecessary(SchedPolicyContext)
	// Setter to initialize the alignment score function.
	AlignBy(func(def.Task, *mesos.Offer, offerUtils.OfferResources) (float64, error))
	// Receive the results of task ranking.
	ReceiveTaskRankingResults(SchedPolicyContext, entities.RankedTasks)
}

// PriorityBasedSchedPolicy is a SchedPolicyState that assumes to work with a PriorityTaskQueue.
type PriorityBasedSchedPolicy interface {
	SchedPolicyState
	GetPriorityUpdateFunc() func(def.PriorityTask)
	GetPriorityUpdateSchedule() string
}

type baseSchedPolicyState struct {
	SchedPolicyState
	// Keep track of the number of tasks that have been scheduled.
	numTasksScheduled int
	// Distribution of tasks that the scheduling policy is most appropriate for.
	// This distribution corresponds to the ratio of low power consuming tasks to
	// high power consuming tasks.
	TaskDistribution float64 `json:"taskDist"`
	// The average variance in cpu-share per task that this scheduling policy can cause.
	// Note: This number corresponds to a given workload.
	VarianceCpuSharePerTask float64 `json:"varCpuShare"`
	// Next and Previous scheduling policy in round-robin order.
	// This order is determined by the sorted order (non-decreasing or non-increasing) of taskDistribution.
	nextPolicyName string
	prevPolicyName string
	// Function to determine the alignment score during offer selection.
	alignmentScore func(def.Task, *mesos.Offer, offerUtils.OfferResources) (float64, error)
}

func (bsps *baseSchedPolicyState) Init(opts ...SchedPolicyOptions) {
	for _, opt := range opts {
		// applying options
		if err := opt(bsps); err != nil {
			log.Fatal(err)
		}
	}
}

func (baseSchedPolicyState) takeNewOffer(offer *mesos.Offer, task def.Task) bool {
	cpus, mem := offerUtils.OfferAgg(offer)

	if (cpus >= task.GetCpu()) && (mem >= task.GetRam()) {
		return true
	}
	return false
}

func (baseSchedPolicyState) takePartlyConsumedOffer(
	offer *mesos.Offer,
	task def.Task,
	consumedCPU, consumedRAM float64) bool {

	cpus, mem := offerUtils.OfferAgg(offer)

	if (cpus >= (consumedCPU + task.GetCpu())) && (mem >= (consumedRAM + task.GetRam())) {
		return true
	}
	return false
}

// Scheduling policy switching criteria.
// Takes a pointer to the BaseScheduler and returns the name of the scheduling policy to switch to.
type switchBy func(*BaseScheduler) string

var switchBasedOn map[string]switchBy = map[string]switchBy{
	"taskDist":        switchTaskDistBased,
	"round-robin":     switchRoundRobinBased,
	"rev-round-robin": switchRevRoundRobinBased,
}

func switchTaskDistBased(baseSchedRef *BaseScheduler) string {
	// Name of the scheduling policy to switch to.
	switchToPolicyName := ""
	// Record overhead to classify the tasks in the scheduling window and using the classification results
	// 	to determine the distribution of low power consuming and high power consuming tasks.
	startTime := time.Now()
	// Determine the distribution of tasks in the new scheduling window.
	taskDist, err := def.GetTaskDistributionInWindow(baseSchedRef.schedWindowSize, baseSchedRef.tasks.GetTasks())
	baseSchedRef.LogClsfnAndTaskDistOverhead(time.Now().Sub(startTime))
	baseSchedRef.Log(elecLogDef.GENERAL, fmt.Sprintf("Switching... TaskDistribution[%f]", taskDist))
	if err != nil {
		// All the tasks in the window were only classified into 1 cluster.
		// Max-Min and Max-GreedyMins would work the same way as Bin-Packing for this situation.
		// So, we have 2 choices to make. First-Fit or Bin-Packing.
		// If choose Bin-Packing, then there might be a performance degradation due to increase in
		// 	resource contention. So, First-Fit might be a better option to cater to the worst case
		// 	where all the tasks are power intensive tasks.
		// TODO: Another possibility is to do the exact opposite and choose Bin-Packing.
		// TODO[2]: Determine scheduling policy based on the distribution of tasks in the whole queue.
		switchToPolicyName = BP
	} else {
		// The tasks in the scheduling window were classified into 2 clusters, meaning that there is
		// 	some variety in the kind of tasks.
		// We now select the scheduling policy which is most appropriate for this distribution of tasks.
		first := schedPoliciesToSwitch[0]
		last := schedPoliciesToSwitch[len(schedPoliciesToSwitch)-1]
		if taskDist < first.sp.GetInfo().taskDist {
			switchToPolicyName = first.spName
		} else if taskDist > last.sp.GetInfo().taskDist {
			switchToPolicyName = last.spName
		} else {
			low := 0
			high := len(schedPoliciesToSwitch) - 1
			for low <= high {
				mid := (low + high) / 2
				if taskDist < schedPoliciesToSwitch[mid].sp.GetInfo().taskDist {
					high = mid - 1
				} else if taskDist > schedPoliciesToSwitch[mid].sp.GetInfo().taskDist {
					low = mid + 1
				} else {
					switchToPolicyName = schedPoliciesToSwitch[mid].spName
					break
				}
			}
			// We're here if low == high+1.
			// If haven't yet found the closest match.
			if switchToPolicyName == "" {
				lowDiff := schedPoliciesToSwitch[low].sp.GetInfo().taskDist - taskDist
				highDiff := taskDist - schedPoliciesToSwitch[high].sp.GetInfo().taskDist
				if lowDiff > highDiff {
					switchToPolicyName = schedPoliciesToSwitch[high].spName
				} else if highDiff > lowDiff {
					switchToPolicyName = schedPoliciesToSwitch[low].spName
				} else {
					// index doens't matter as the values at high and low are equidistant
					// 	from taskDist.
					switchToPolicyName = schedPoliciesToSwitch[high].spName
				}
			}
		}
	}
	return switchToPolicyName
}

// Switching based on a round-robin approach.
// Not being considerate to the state of TaskQueue or the state of the cluster.
func switchRoundRobinBased(baseSchedRef *BaseScheduler) string {
	// If haven't received any resource offers.
	if !baseSchedRef.hasReceivedResourceOffers {
		return schedPoliciesToSwitch[0].spName
	}
	return baseSchedRef.curSchedPolicy.GetInfo().nextPolicyName
}

// Switching based on a round-robin approach, but in the reverse order.
// Not being considerate to the state of the TaskQueue or the state of the cluster.
func switchRevRoundRobinBased(baseSchedRef *BaseScheduler) string {
	// If haven't received any resource offers.
	if !baseSchedRef.hasReceivedResourceOffers {
		return schedPoliciesToSwitch[len(schedPoliciesToSwitch)-1].spName
	}
	return baseSchedRef.curSchedPolicy.GetInfo().prevPolicyName
}

func (bsps *baseSchedPolicyState) SwitchIfNecessary(spc SchedPolicyContext) {
	baseSchedRef := spc.(*BaseScheduler)
	// Switching scheduling policy only if feature enabled from CLI.
	if baseSchedRef.schedPolSwitchEnabled {
		// Name of scheduling policy to switch to.
		switchToPolicyName := ""
		// Size of the new scheduling window.
		newSchedWindowSize := 0
		// If scheduling window has not been fixed, then determine the scheduling window based on the current
		// 	availability of resources on the cluster (Mesos perspective).
		if !baseSchedRef.toFixSchedWindow {
			// Need to compute the size of the scheduling window.
			// The next scheduling policy will schedule at max schedWindowSize number of tasks.
			newSchedWindowSize, baseSchedRef.numTasksInSchedWindow =
				baseSchedRef.schedWindowResStrategy.Apply(func() interface{} { return baseSchedRef.tasks })
		}

		// Now, we need to switch if the new scheduling window is > 0.
		if (!baseSchedRef.toFixSchedWindow && (newSchedWindowSize > 0)) ||
			(baseSchedRef.toFixSchedWindow && (baseSchedRef.schedWindowSize > 0)) {
			// If we haven't received any resource offers, then
			// 	check whether we need to fix the first scheduling policy to deploy.
			// 	If not, then determine the first scheduling policy based on the distribution of tasks
			//		in the scheduling window.
			// Else,
			// 	Check whether the currently deployed scheduling policy has already scheduled the
			// 		schedWindowSize number of tasks.
			// 	If yes, then we switch to the scheduling policy based on the distribution of tasks in
			//		the scheduling window.
			// 	If not, then we continue to use the currently deployed scheduling policy.
			if !baseSchedRef.hasReceivedResourceOffers {
				if baseSchedRef.nameOfFstSchedPolToDeploy != "" {
					switchToPolicyName = baseSchedRef.nameOfFstSchedPolToDeploy
					if !baseSchedRef.toFixSchedWindow {
						baseSchedRef.schedWindowSize = newSchedWindowSize
					}
				} else {
					// Decided to switch, so updating the window size.
					if !baseSchedRef.toFixSchedWindow {
						baseSchedRef.schedWindowSize = newSchedWindowSize
					}
					switchToPolicyName = switchBasedOn[baseSchedRef.schedPolSwitchCriteria](baseSchedRef)
				}
			} else {
				// Checking if the currently deployed scheduling policy has scheduled all the tasks in the window.
				if bsps.numTasksScheduled >= baseSchedRef.schedWindowSize {
					// Decided to switch, so updating the window size.
					if !baseSchedRef.toFixSchedWindow {
						baseSchedRef.schedWindowSize = newSchedWindowSize
					}
					switchToPolicyName = switchBasedOn[baseSchedRef.schedPolSwitchCriteria](baseSchedRef)
				} else {
					// We continue working with the currently deployed scheduling policy.
					log.Println("Continuing with the current scheduling policy...")
					log.Printf("TasksScheduled[%d], SchedWindowSize[%d]", bsps.numTasksScheduled,
						baseSchedRef.schedWindowSize)
					return
				}
			}
			// Switching scheduling policy.
			baseSchedRef.LogSchedPolicySwitch(switchToPolicyName, SchedPolicies[switchToPolicyName])
			baseSchedRef.SwitchSchedPol(SchedPolicies[switchToPolicyName])
			// Resetting the number of tasks scheduled as this is a new scheduling policy that has been
			// 	deployed.
			bsps.numTasksScheduled = 0
		} else {
			// We continue working with the currently deployed scheduling policy.
			log.Println("Continuing with the current scheduling policy...")
			log.Printf("TasksScheduled[%d], SchedWindowSize[%d]", bsps.numTasksScheduled,
				baseSchedRef.schedWindowSize)
			return
		}
	}
}

func (bsps *baseSchedPolicyState) GetInfo() (info struct {
	taskDist       float64
	varCpuShare    float64
	nextPolicyName string
	prevPolicyName string
}) {
	info.taskDist = bsps.TaskDistribution
	info.varCpuShare = bsps.VarianceCpuSharePerTask
	info.nextPolicyName = bsps.nextPolicyName
	info.prevPolicyName = bsps.prevPolicyName
	return info
}

func (bsps *baseSchedPolicyState) UpdateLinks(info struct {
	nextPolicyName string
	prevPolicyName string
}) {
	bsps.nextPolicyName = info.nextPolicyName
	bsps.prevPolicyName = info.prevPolicyName
}

func (bsps *baseSchedPolicyState) AlignBy(
	alignmentScoreFunction func(def.Task, *mesos.Offer, offerUtils.OfferResources) (float64, error)) {
	bsps.alignmentScore = alignmentScoreFunction
}

// Different functions for Alignment Score calculation.
// Each function uses different metrics.

// taskVector = [task.CPU, taskRAM], offerVector = [remaining CPU in offer, remaining memory in offer]
func alignByCpuAndMem(t def.Task, o *mesos.Offer, availRes offerUtils.OfferResources) (float64, error) {
	if o == nil {
		return 0.0, errors.New("Invalid offer passed for alignment score calculation")
	}
	return -((t.GetCpu() * availRes.CPU) + (t.GetRam() * availRes.RAM)), nil
}

// taskVector = [actual share of cpu resources, task.RAM], offerVector = [offer.host.runAvgCpuUtil, offer.RAM]
func alignByCpuUtilAndMem(t def.Task, o *mesos.Offer, availRes offerUtils.OfferResources) (float64, error) {
	if o == nil {
		return 0.0, errors.New("Invalid offer passed for alignment score calculation")
	}
	allocMetrics := metrics.GetAllocationMetricsListener().(*metrics.AllocationMetricsListener)
	utilMetrics := metrics.GetUtilizationMetricsListenerInstance().(*metrics.UtilizationMetricsListener)
	resAllocInfo, fitTaskInfo := allocMetrics.GetAllAllocationMetricsHost(*o.Hostname)

	// Calculating the actual share of CPU resources that task t is going to get.
	totalCpuAllocated := t.GetCpu()
	for _, info := range fitTaskInfo {
		totalCpuAllocated += info.CPU
	}
	for _, info := range resAllocInfo {
		totalCpuAllocated += info.CpuShare
	}

	// Actual share of CPU resources for task t = t.CPU / totalCpuAllocated
	actualShareCPUForTask := t.GetCpu() / totalCpuAllocated
	runAvgCpuUtil, _ := utilMetrics.GetHistAvgCpuUtilHost(*o.Hostname)
	return -((actualShareCPUForTask * runAvgCpuUtil) + (t.GetRam() * availRes.RAM)), nil
}

// taskVector = [], offerVector = [1 - offer.host.slackCpuPower].
//
// Host with least slack in CPU power consumption will have the highest alignment score.
// With this approach, the increase in cluster-wide power consumption depends on the least amount
// of slack in cpu power consumption across the candidate hosts.
func alignByCpuPowerSlack(_ def.Task, o *mesos.Offer, _ offerUtils.OfferResources) (float64, error) {
	utilMetrics := metrics.GetUtilizationMetricsListenerInstance().(*metrics.UtilizationMetricsListener)
	if histRunAvgCpuPower, err := utilMetrics.GetHistRunAvgPowerCpuHost(*o.Hostname); err != nil {
		return 0.0, err
	} else {
		tdpHost, err := utilMetrics.GetTDPHost(*o.Hostname)
		if err != nil {
			return 0.0, err
		}
		slackCpuPower := tdpHost - histRunAvgCpuPower
		return -slackCpuPower, nil
	}
}

// taskVector = [], offerVector = [1 - offer.host.slackCpuDramPower].
//
// Host with least slack in CPU+Dram power consumption will have the highest alignment score.
// With this approach, the increase in cluster-wide power consumption depends on the least amount
// of slack in cpu+dram power consumption across the candidate hosts.
func alignByCpuDramPowerSlack(_ def.Task, o *mesos.Offer, _ offerUtils.OfferResources) (float64, error) {
	utilMetrics := metrics.GetUtilizationMetricsListenerInstance().(*metrics.UtilizationMetricsListener)
	var histRunAvgCpuPower, histRunAvgDramPower float64
	var maxCpuDramPowerHost float64
	var err error

	if histRunAvgCpuPower, err = utilMetrics.GetHistRunAvgPowerCpuHost(*o.Hostname); err != nil {
		return 0.0, err
	}
	if histRunAvgDramPower, err = utilMetrics.GetHistRunAvgPowerDramHost(*o.Hostname); err != nil {
		return 0.0, err
	}

	if maxCpuDramPowerHost, err = utilMetrics.GetMaxCpuDramPowerHost(*o.Hostname); err != nil {
		return 0.0, err
	}

	return -(maxCpuDramPowerHost - (histRunAvgCpuPower + histRunAvgDramPower)), nil
}

// Host with the highest estimated cpu utilization will have the max alignment score.
func alignByCpuUtilEstimate(_ def.Task, o *mesos.Offer, _ offerUtils.OfferResources) (float64, error) {
	utilMetrics := metrics.GetUtilizationMetricsListenerInstance().(*metrics.UtilizationMetricsListener)
	return utilMetrics.GetCpuUtilEstimateHost(o.GetHostname())
}

// Align tasks with offers such that the offer with the highest (cpu-mem + cpuUtil-mem + (2 * cpuPowerSlack)) is selected.
// Assigning a higher weight to cpuPowerSlack alignment function to favor reduction in peak power consumption.
func weightedTaskOfferAlignment(t def.Task, o *mesos.Offer, availRes offerUtils.OfferResources) (float64, error) {
	alignCpuMemScore, _ := alignByCpuAndMem(t, o, availRes)
	alignCpuUtilMemScore, _ := alignByCpuUtilAndMem(t, o, availRes)
	alignCpuPowerSlackScore, _ := alignByCpuPowerSlack(t, o, availRes)
	return alignCpuMemScore + alignCpuUtilMemScore + (2 * alignCpuPowerSlackScore), nil
}

// Default alignment function.
// Provides equal weights to resource offers.
func equalWeightDefaultAlignment(_ def.Task, _ *mesos.Offer, _ offerUtils.OfferResources) (float64, error) {
	return 0, nil
}

func (bsps *baseSchedPolicyState) resourcesToReserve(task def.Task, offer *mesos.Offer) []*mesos.Resource {
	return []*mesos.Resource{
		mesosutil.NewScalarResource("cpus", task.GetCpu()),
		mesosutil.NewScalarResource("mem", task.GetRam()),
	}
}

// Receive the results of task ranking.
// This represents the default implementation whereby the task ranking results are just logged.
// Scheduling policies that want to use the ranking results should implement this function.
func (bsps *baseSchedPolicyState) ReceiveTaskRankingResults(spc SchedPolicyContext, rankedTasks entities.RankedTasks) {
	var rankedTasksInfo []string

	for host, tasks := range rankedTasks {
		rankedTasksInfo = append(rankedTasksInfo, fmt.Sprintf("%s::%v", host, tasks))
	}
	spc.(*BaseScheduler).Log(elecLogDef.TASK_RANKS, strings.Join(rankedTasksInfo, "|"))
}
