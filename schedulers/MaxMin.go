package schedulers

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"bitbucket.org/sunybingcloud/electron/utilities/mesosUtils"
	"bitbucket.org/sunybingcloud/electron/utilities/offerUtils"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	sched "github.com/mesos/mesos-go/api/v0/scheduler"
)

type MaxMin struct {
	baseSchedPolicyState
}

// Determine if the remaining space inside of the offer is enough for this
// task that we need to create. If it is, create a TaskInfo and return it.
func (s *MaxMin) CheckFit(
	spc SchedPolicyContext,
	i int,
	task def.Task,
	offer *mesos.Offer,
	totalCPU *float64,
	totalRAM *float64) (bool, *mesos.TaskInfo) {

	baseSchedRef := spc.(*BaseScheduler)
	// Does the task fit.
	if s.takePartlyConsumedOffer(offer, task, *totalCPU, *totalRAM) {

		*totalCPU += task.GetCpu()
		*totalRAM += task.GetRam()
		baseSchedRef.LogCoLocatedTasks(offer.GetSlaveId().GoString())

		taskToSchedule := baseSchedRef.newTaskInfo(offer, task, s.resourcesToReserve(task, offer))

		baseSchedRef.LogSchedTrace(taskToSchedule, offer)
		task.DecrementInstances()
		s.numTasksScheduled++

		if task.GetInstances() <= 0 {
			// All instances of task have been scheduled, remove it.
			baseSchedRef.tasks.RemoveAt(i)

			if baseSchedRef.tasks.Len() <= 0 {
				baseSchedRef.ExamineShutdownOfScheduling()
			}
		}

		return true, taskToSchedule
	}
	return false, nil
}

func (s *MaxMin) ConsumeOffers(spc SchedPolicyContext, driver sched.SchedulerDriver, offers []*mesos.Offer) {
	baseSchedRef := spc.(*BaseScheduler)
	switch baseSchedRef.tasks.(type) {
	case taskQueue.SortableTaskQueue:
		if baseSchedRef.schedPolSwitchEnabled {
			baseSchedRef.tasks.(taskQueue.SortableTaskQueue).SortN(baseSchedRef.numTasksInSchedWindow, def.SortByWatts)
		} else {
			baseSchedRef.tasks.(taskQueue.SortableTaskQueue).Sort(def.SortByWatts)
		}
	}
	baseSchedRef.LogOffersReceived(offers)

	for _, offer := range offers {
		offerUtils.UpdateEnvironment(offer)
		if baseSchedRef.ShutdownScheduling.IsClosed() {
			baseSchedRef.LogNoPendingTasksDeclineOffers(offer)
			driver.DeclineOffer(offer.Id, mesosUtils.LongFilter)
			baseSchedRef.LogNumberOfRunningTasks()
			continue
		}

		tasks := []*mesos.TaskInfo{}

		offerTaken := false
		totalCPU := 0.0
		totalRAM := 0.0

		// Assumes s.tasks is ordered in non-decreasing median max-peak order

		// Attempt to schedule a single instance of the heaviest workload available first.
		// Start from the back until one fits.

		direction := false // True = Min Max, False = Max Min
		var index int
		start := true // If false then index has changed and need to keep it that way
		for i := 0; i < baseSchedRef.tasks.Len(); i++ {
			// If scheduling policy switching enabled, then
			// stop scheduling if the #baseSchedRef.schedWindowSize tasks have been scheduled.
			if baseSchedRef.schedPolSwitchEnabled &&
				(s.numTasksScheduled >= baseSchedRef.schedWindowSize) {
				break // Offers will automatically get declined.
			}
			// We need to pick a min task or a max task
			// depending on the value of direction.
			if direction && start {
				index = 0
			} else if start {
				index = baseSchedRef.tasks.Len() - i - 1
			}

			task := baseSchedRef.tasks.At(index)

			// Don't take offer if it doesn't match our task's host requirement.
			if offerUtils.HostMismatch(*offer.Hostname, task.GetHost()) {
				continue
			}

			taken, taskToSchedule := s.CheckFit(spc, index, task, offer, &totalCPU, &totalRAM)

			if taken {
				offerTaken = true
				tasks = append(tasks, taskToSchedule)
				// Need to change direction and set start to true.
				// Setting start to true would ensure that index be set accurately again.
				direction = !direction
				start = true
				i--
			} else {
				// Need to move index depending on the value of direction.
				if direction {
					index++
					start = false
				} else {
					index--
					start = false
				}
			}
		}

		if offerTaken {
			baseSchedRef.LogTaskStarting(nil, offer)
			LaunchTasks([]*mesos.OfferID{offer.Id}, tasks, driver)
		} else {
			// If there was no match for the task
			cpus, mem := offerUtils.OfferAgg(offer)
			baseSchedRef.LogInsufficientResourcesDeclineOffer(offer, cpus, mem)
			driver.DeclineOffer(offer.Id, mesosUtils.DefaultFilter)
		}
	}
}
