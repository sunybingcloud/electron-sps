package schedulers

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/events"
	elecLogDef "bitbucket.org/sunybingcloud/electron/logging/def"
	elecSchedUtils "bitbucket.org/sunybingcloud/electron/schedulers/utilities"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"bitbucket.org/sunybingcloud/electron/utilities"
	"bitbucket.org/sunybingcloud/electron/utilities/mesosUtils"
	"bytes"
	"fmt"
	"github.com/golang/protobuf/proto"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	sched "github.com/mesos/mesos-go/api/v0/scheduler"
	"github.com/pkg/errors"
	taskranker "github.com/pradykaushik/task-ranker"
	"github.com/pradykaushik/task-ranker/entities"
	"log"
	"strings"
	"sync"
	"time"
)

type BaseScheduler struct {
	SchedPolicyContext
	// Current scheduling policy used for resource offer consumption.
	curSchedPolicy SchedPolicyState

	tasksCreated   int
	tasksRunning   int
	tasks          taskQueue.TaskQueue
	eventListeners map[events.EventType][]events.EventListener
	// Running tasks and the slave IDs of the corresponding mesos agents.
	Running                           map[string]map[string]bool
	classMapWatts                     bool
	TasksRunningMutex                 sync.Mutex
	HostNameToSlaveID                 map[string]string
	totalResourceAvailabilityRecorded bool

	// Whether the tasks are submitted via HTTP.
	tasksFromHTTP bool
	httpPort      int

	// This channel is closed when the HTTP server is shutdown.
	HTTPServerShutdown *utilities.SignalChannel

	// This channel is closed when the program receives an interrupt,
	// signalling that the program should shut down.
	ShutdownScheduling *utilities.SignalChannel
	// This channel is closed to indicate that it is okay to shutdown scheduling.
	// This is to be used only when tasks are being submitted through HTTP.
	canShutdownScheduling *utilities.SignalChannel
	// This channel is closed after shutdown is closed, and only when all
	// outstanding tasks have been cleaned up
	ShutdownFramework *utilities.SignalChannel

	schedTrace *log.Logger

	// Send the type of the message to be logged
	logMsgType chan elecLogDef.LogMessageType
	// Send the message to be logged
	logMsg chan string

	mutex sync.Mutex

	// Whether switching of scheduling policies at runtime has been enabled
	schedPolSwitchEnabled bool
	// Name of the first scheduling policy to be deployed, if provided.
	// This scheduling policy would be deployed first regardless of the distribution of tasks in the TaskQueue.
	// Note: Scheduling policy switching needs to be enabled.
	nameOfFstSchedPolToDeploy string
	// Scheduling policy switching criteria.
	schedPolSwitchCriteria string

	// Size of window of tasks that can be scheduled in the next offer cycle.
	// The window size can be adjusted to make the most use of every resource offer.
	// By default, the schedWindowSize would correspond to the number of remaining tasks that haven't yet
	// been scheduled.
	schedWindowSize int
	// Number of tasks in the window
	numTasksInSchedWindow int

	// Whether the scheduling window needs to be fixed.
	toFixSchedWindow bool // If yes, then schedWindowSize is initialized and kept unchanged.

	// Strategy to resize the schedulingWindow.
	schedWindowResStrategy elecSchedUtils.SchedWindowResizingStrategy

	// Indicate whether the any resource offers from mesos have been received.
	hasReceivedResourceOffers bool

	// If new tasks are submitted during an offer cycle, then we store them in this array so
	// as to not modify the current task queue that the scheduling policy is working on.
	tempNewTasks      []def.Task
	tempNewTasksMutex sync.Mutex

	// Task Ranker.
	taskRanker *taskranker.TaskRanker
}

func (s *BaseScheduler) init(opts ...SchedulerOptions) {
	for _, opt := range opts {
		// applying options
		if err := opt(s); err != nil {
			log.Fatal(err)
		}
	}
	s.Running = make(map[string]map[string]bool)
	s.HostNameToSlaveID = make(map[string]string)
	// Initializing the scheduling window resizing strategy.
	// This should be ignored if scheduling policy switching is not enabled.
	s.schedWindowResStrategy = elecSchedUtils.SchedWindowResizingCritToStrategy["fillNextOfferCycle"]
	// Initially no resource offers would have been received.
	s.hasReceivedResourceOffers = false
	// Initialize channels.
	s.canShutdownScheduling = utilities.NewSignalChannel()

	// If the scheduling policy is a PriorityBasedSchedPolicy and the task queue is
	// a PriorityTaskQueue, then setting periodic updates of task priorities.
	// Note that we only check for the current scheduling policy.
	// If current scheduling policy is not a PriorityBasedSchedPolicy,
	// and switching is enabled, then if the scheduler switches to a
	// PriorityBasedSchedPolicy, the periodic updates task priorities will not be initiated.
	if ptySchedPolicy, ok := s.curSchedPolicy.(PriorityBasedSchedPolicy); ok {
		if ptyTaskQ, ok := s.tasks.(taskQueue.PriorityTaskQueue); ok {
			ptyTaskQ.WithPeriodicUpdates(ptySchedPolicy.GetPriorityUpdateSchedule(), ptySchedPolicy.GetPriorityUpdateFunc())
			// Starting the periodic updates.
			if err := ptyTaskQ.StartPeriodicUpdates(); err != nil {
				s.LogElectronError(errors.New("failed to start periodic updates of task priorities"))
			}
		}
	}

	// If taskRanker has been initialized, then starting the task ranking process.
	if s.taskRanker != nil {
		s.taskRanker.Start()
	}

	// Running the shutdown loop.
	s.ShutdownMonitor()
}

func (s *BaseScheduler) ShutdownMonitor() {
	go func() {
		// If HTTP Server is running, then wait for HTTPServerShutdown signal.
		if s.tasksFromHTTP {
			s.HTTPServerShutdown.WaitTillClosed()
			// If there are no more pending tasks, then
			// 	shutdown scheduling.
			// Else, indicate that it is okay to shutdown scheduling
			// 	and return.
			s.tasks.Lock()
			defer s.tasks.Unlock()
			if s.tasks.Len() <= 0 {
				s.LogShutdownScheduling()
				if ptyTaskQ, ok := s.tasks.(taskQueue.PriorityTaskQueue); ok {
					ptyTaskQ.StopPeriodicUpdates()
				}
				s.ShutdownScheduling.Close()

				// Check if we need to shutdown the framework.
				// This is required when shutdown request is the first thing received,
				// where StatusUpdate(...) is not going to be called.
				// Note: If there are tasks running then breaking out of the shutdown loop is
				// okay as the shutting down of the framework would then be handled in
				// StatusUpdate(...).
				s.TasksRunningMutex.Lock()
				if s.tasksRunning <= 0 {
					s.LogShutdownFramework()
					s.ShutdownFramework.Close()
				}
				s.TasksRunningMutex.Unlock()
			} else {
				s.canShutdownScheduling.Close()
			}
		} else {
			// Do nothing.
			// This condition is taken care of in each scheduling policy.
			// TODO: Scheduling policies should not worry about shutting down channels.
		}
	}()
}

// Check whether it is okay to end scheduling. This will result in all the remaining
// resource offers getting declined.
func (s *BaseScheduler) ExamineShutdownOfScheduling() {
	s.LogTerminateScheduler()
	// If tasks are submitted through the CLI, then
	// we're done scheduling.
	// If the tasks are submitted through HTTP, and
	// it is okay to shutdown scheduling, then do so.
	// Note: We would be able to shutdown only if the
	// HTTP server has been shutdown.
	if !s.tasksFromHTTP {
		s.LogShutdownScheduling()
		if ptyTaskQ, ok := s.tasks.(taskQueue.PriorityTaskQueue); ok {
			ptyTaskQ.StopPeriodicUpdates()
		}
		s.ShutdownScheduling.Close()
	} else {
		if s.canShutdownScheduling.IsClosed() {
			s.LogShutdownScheduling()
			if ptyTaskQ, ok := s.tasks.(taskQueue.PriorityTaskQueue); ok {
				ptyTaskQ.StopPeriodicUpdates()
			}
			s.ShutdownScheduling.Close()
		} else {
			// New jobs can still be submitted.
		}
	}
}

func (s *BaseScheduler) Attach(l events.EventListener) {
	for _, et := range l.GetEventTypes() {
		if _, ok := s.eventListeners[et]; ok {
			s.eventListeners[et] = append(s.eventListeners[et], l)
		} else {
			s.eventListeners[et] = []events.EventListener{l}
		}
	}
}

func (s *BaseScheduler) NotifyListeners(et events.EventType, d events.EventData) {
	go func() {
		for _, l := range s.eventListeners[et] {
			if err := l.Update(et, d); err != nil {
				s.LogElectronError(err)
			}
		}
	}()
}

func (s *BaseScheduler) Receive(rankedTasks entities.RankedTasks) {
	s.curSchedPolicy.ReceiveTaskRankingResults(s, rankedTasks)
}

func (s *BaseScheduler) SwitchSchedPol(newSchedPol SchedPolicyState) {
	s.curSchedPolicy = newSchedPol
}

const (
	// containerLabelCliOption represents the docker command-line option used to assign
	// a label to a container.
	containerLabelCliOption = "label"

	// containerLabelKeyTaskID is a label key corresponding to the taskID of the task.
	containerLabelKeyTaskID = "electron_task_id"

	// containerLabelKeyTaskHost is a label key corresponding to the name of the host assigned to the task.
	containerLabelKeyTaskHost = "electron_task_hostname"

	// cAdvisorContainerLabelPrefix is the prefix that cAdvisor adds to all the container labels.
	cAdvisorContainerLabelPrefix = "container_label_"
)

func (s *BaseScheduler) newTaskInfo(offer *mesos.Offer, task def.Task, resources []*mesos.Resource) *mesos.TaskInfo {
	s.tasksCreated++

	// Generating taskID
	taskID := def.GenerateTaskID(task)
	s.NotifyListeners(events.TASK_OFFER_MATCH, events.NewTaskOfferMatchEventData(
		events.WithTaskID(taskID),
		events.WithTask(task),
		events.WithOffer(offer),
		events.WithHostname(*offer.Hostname),
		events.WithClassMapWattsEnabled(s.classMapWatts),
	))
	// Recording task resource requirements.
	def.RecordTaskResourceReqs(taskID, task)

	return &mesos.TaskInfo{
		Name: proto.String(taskID),
		TaskId: &mesos.TaskID{
			Value: proto.String(taskID),
		},
		SlaveId:   offer.SlaveId,
		Resources: resources,
		Command: &mesos.CommandInfo{
			Value: proto.String(task.GetCmd()),
		},
		Container: &mesos.ContainerInfo{
			Type: mesos.ContainerInfo_DOCKER.Enum(),
			Docker: &mesos.ContainerInfo_DockerInfo{
				Image:   proto.String(task.GetImage()),
				Network: mesos.ContainerInfo_DockerInfo_BRIDGE.Enum(), // Run everything isolated
				Parameters: []*mesos.Parameter{
					// Setting label for task with value as the taskID.
					// This allows for matching any container specific metrics to the corresponding tasks.
					// Note that this is a workaround as Mesos fixes the container names to mesos-<agent ID>
					// therefore making it difficult for us to map container names to task IDs.
					// By assigning a label that includes the taskID as the value, mapping container specific
					// metrics to corresponding task IDs becomes easier.
					{
						Key:   proto.String(containerLabelCliOption),
						Value: proto.String(strings.Join([]string{containerLabelKeyTaskID, taskID}, "=")),
					},
					// Setting label for task with value as hostname of consumed offer.
					{
						Key:   proto.String(containerLabelCliOption),
						Value: proto.String(strings.Join([]string{containerLabelKeyTaskHost, offer.GetHostname()}, "=")),
					},
				},
			},
		},
	}
}

func (s *BaseScheduler) OfferRescinded(_ sched.SchedulerDriver, offerID *mesos.OfferID) {
	s.LogOfferRescinded(offerID)
}
func (s *BaseScheduler) SlaveLost(_ sched.SchedulerDriver, slaveID *mesos.SlaveID) {
	s.LogSlaveLost(slaveID)
}
func (s *BaseScheduler) ExecutorLost(_ sched.SchedulerDriver, executorID *mesos.ExecutorID,
	slaveID *mesos.SlaveID, status int) {
	s.LogExecutorLost(executorID, slaveID)
}

func (s *BaseScheduler) Error(_ sched.SchedulerDriver, err string) {
	s.LogMesosError(err)
}

func (s *BaseScheduler) FrameworkMessage(
	driver sched.SchedulerDriver,
	executorID *mesos.ExecutorID,
	slaveID *mesos.SlaveID,
	message string) {
	s.LogFrameworkMessage(executorID, slaveID, message)
}

func (s *BaseScheduler) Registered(
	_ sched.SchedulerDriver,
	frameworkID *mesos.FrameworkID,
	masterInfo *mesos.MasterInfo) {
	s.LogFrameworkRegistered(frameworkID, masterInfo)
}

func (s *BaseScheduler) Reregistered(_ sched.SchedulerDriver, masterInfo *mesos.MasterInfo) {
	s.LogFrameworkReregistered(masterInfo)
}

func (s *BaseScheduler) Disconnected(sched.SchedulerDriver) {
	s.LogDisconnected()
}

func (s *BaseScheduler) ResourceOffers(driver sched.SchedulerDriver, offers []*mesos.Offer) {
	// Recording the total amount of resources available across the cluster.
	elecSchedUtils.RecordTotalResourceAvailability(offers)
	for _, offer := range offers {
		if _, ok := s.HostNameToSlaveID[offer.GetHostname()]; !ok {
			s.HostNameToSlaveID[offer.GetHostname()] = *offer.SlaveId.Value
		}
	}
	// Lock the task queue.
	// This is important to make sure that the task queue is unaltered during the scheduling cycle.
	s.tasks.Lock()
	// If receiving tasks via HTTP, add any newly submitted tasks to the task queue.
	s.addNewlySubmittedTasks()
	// Switch just before consuming the resource offers.
	s.curSchedPolicy.SwitchIfNecessary(s)
	//	s.Log(elecLogDef.GENERAL, fmt.Sprintf("SchedWindowSize[%d], #TasksInWindow[%d]",
	//		s.schedWindowSize, s.numTasksInSchedWindow))
	s.curSchedPolicy.ConsumeOffers(s, driver, offers)
	s.hasReceivedResourceOffers = true
	s.tasks.Unlock()
}

func (s *BaseScheduler) StatusUpdate(driver sched.SchedulerDriver, status *mesos.TaskStatus) {
	s.LogTaskStatusUpdate(status)
	s.NotifyListeners(events.TASK_STATUS_UPDATE, events.NewTaskStatusEventData(
		events.WithTaskStatus(status),
	))
	if *status.State == mesos.TaskState_TASK_RUNNING {
		// If this is our first time running into this Agent
		s.TasksRunningMutex.Lock()
		if _, ok := s.Running[*status.SlaveId.Value]; !ok {
			s.Running[*status.SlaveId.Value] = make(map[string]bool)
		}
		// Add task to list of tasks running on node
		s.Running[*status.SlaveId.Value][*status.TaskId.Value] = true
		s.tasksRunning++
		s.TasksRunningMutex.Unlock()
	} else if mesosUtils.IsTerminal(status.State) {
		// Update resource availability.
		elecSchedUtils.ResourceAvailabilityUpdate("ON_TASK_TERMINAL_STATE",
			*status.TaskId, *status.SlaveId)
		s.TasksRunningMutex.Lock()
		// If terminal state reached after TASK_RUNNING status updated, then
		// 1. decrement the number of tasks running.
		// 2. remove the task from the list of tasks running on the respective host.
		// This is done to prevent #tasksRunning to hold an incorrect value when terminal status update is
		// received before TASK_RUNNING status update (possible due to error in task resource specification).
		if _, ok := s.Running[*status.SlaveId.Value][*status.TaskId.Value]; ok {
			delete(s.Running[*status.SlaveId.Value], *status.TaskId.Value)
			s.tasksRunning--
		}
		s.TasksRunningMutex.Unlock()
		if s.tasksRunning == 0 {
			if s.ShutdownScheduling.IsClosed() {
				// If task ranker is running, stopping it.
				if s.taskRanker != nil {
					s.taskRanker.Stop()
				}
				s.LogShutdownFramework()
				s.ShutdownFramework.Close()
			}
		}
	}
}

// Check the temporary task storage (tempNewTasks) for any residue of tasks.
// If any tasks were submitted via HTTP during the previous offer cycle, they would be present here.
// Transfer the new tasks that are pending scheduling to the task queue and flush the temp storage.
func (s *BaseScheduler) addNewlySubmittedTasks() {
	s.tempNewTasksMutex.Lock()
	defer s.tempNewTasksMutex.Unlock()

	if s.tempNewTasks == nil {
		return
	}
	if len(s.tempNewTasks) > 0 {
		s.tasks.EnqueueTasks(s.tempNewTasks...)
		// Setting to nil will release underlying memory for garbage collection.
		// Ref: https://stackoverflow.com/a/16973160/9932695
		s.tempNewTasks = nil
	}
}

// This function is called from ElectronAPIHandler when new tasks are submitted via HTTP.
func (s *BaseScheduler) NewTasks(newTasks []def.Task) {
	now := time.Now() // Submission time.
	s.tempNewTasksMutex.Lock()
	defer s.tempNewTasksMutex.Unlock()
	// Temporarily storing the newly submitted tasks.
	// This will be flushed into the task queue before the next offer cycle.
	s.tempNewTasks = append(s.tempNewTasks, newTasks...)
	// Recording submission time.
	def.RecordSubmissionTime(s.tempNewTasks, now)
}

func (s *BaseScheduler) Log(lmt elecLogDef.LogMessageType, msg string) {
	s.mutex.Lock()
	s.logMsgType <- lmt
	s.logMsg <- msg
	s.mutex.Unlock()
}

func (s *BaseScheduler) LogTaskStarting(ts def.Task, offer *mesos.Offer) {
	lmt := elecLogDef.GENERAL
	msgColor := elecLogDef.LogMessageColors[lmt]
	var msg string
	if ts == nil {
		msg = msgColor.Sprintf("TASKS STARTING... host = [%s]", offer.GetHostname())
	} else {
		msg = msgColor.Sprintf("TASK STARTING... task = [%s], Instance = %d, host = [%s]",
			ts.GetName(), ts.GetInstances(), offer.GetHostname())
	}
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogTaskWattsConsideration(ts def.Task, host string, wattsToConsider float64) {
	lmt := elecLogDef.GENERAL
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := msgColor.Sprintf("Watts considered for task[%s] and host[%s] = %f Watts",
		ts.GetName(), host, wattsToConsider)
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogOffersReceived(offers []*mesos.Offer) {
	lmt := elecLogDef.GENERAL
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := msgColor.Sprintf("Received %d resource offers", len(offers))
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogNoPendingTasksDeclineOffers(offer *mesos.Offer) {
	lmt := elecLogDef.WARNING
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := msgColor.Sprintf("DECLINING OFFER for host[%s]... "+
		"No tasks left to schedule", offer.GetHostname())
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogNumberOfRunningTasks() {
	lmt := elecLogDef.GENERAL
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := msgColor.Sprintf("Number of tasks still Running = %d", s.tasksRunning)
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogCoLocatedTasks(slaveID string) {
	lmt := elecLogDef.GENERAL
	msgColor := elecLogDef.LogMessageColors[lmt]
	buffer := bytes.Buffer{}
	buffer.WriteString(fmt.Sprintln("Colocated with:"))
	s.TasksRunningMutex.Lock()
	for taskName := range s.Running[slaveID] {
		buffer.WriteString(fmt.Sprintln(taskName))
	}
	s.TasksRunningMutex.Unlock()
	msg := msgColor.Sprintf(buffer.String())
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogSchedTrace(taskToSchedule *mesos.TaskInfo, offer *mesos.Offer) {
	msg := fmt.Sprint(offer.GetHostname() + ":" + taskToSchedule.GetTaskId().GetValue())
	s.Log(elecLogDef.SCHED_TRACE, msg)
}

func (s *BaseScheduler) LogTerminateScheduler() {
	lmt := elecLogDef.GENERAL
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := msgColor.Sprint("Done scheduling all tasks!")
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogInsufficientResourcesDeclineOffer(offer *mesos.Offer,
	offerResources ...interface{}) {
	lmt := elecLogDef.WARNING
	msgColor := elecLogDef.LogMessageColors[lmt]
	buffer := bytes.Buffer{}
	buffer.WriteString(fmt.Sprintf("DECLINING OFFER for host[%s]... Offer has insufficient "+
		"resources to launch a task\n", *offer.Hostname))
	buffer.WriteString(fmt.Sprintf("Offer Resources <CPU: %f, RAM: %f>", offerResources...))
	msg := msgColor.Sprint(buffer.String())
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogOfferRescinded(offerID *mesos.OfferID) {
	lmt := elecLogDef.ERROR
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := msgColor.Sprintf("OFFER RESCINDED: OfferID = %s", offerID)
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogSlaveLost(slaveID *mesos.SlaveID) {
	lmt := elecLogDef.ERROR
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := msgColor.Sprintf("SLAVE LOST: SlaveID = %s", slaveID)
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogExecutorLost(executorID *mesos.ExecutorID, slaveID *mesos.SlaveID) {
	lmt := elecLogDef.ERROR
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := msgColor.Sprintf("EXECUTOR LOST: ExecutorID = %s, SlaveID = %s", executorID, slaveID)
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogFrameworkMessage(executorID *mesos.ExecutorID,
	slaveID *mesos.SlaveID, message string) {
	lmt := elecLogDef.GENERAL
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := msgColor.Sprintf("Received Framework message from executor [%s]: %s", executorID, message)
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogMesosError(err string) {
	lmt := elecLogDef.ERROR
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := msgColor.Sprintf("MESOS ERROR: %s", err)
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogElectronError(err error) {
	lmt := elecLogDef.ERROR
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := msgColor.Sprintf("ELECTRON ERROR: %v", err.Error())
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogFrameworkRegistered(frameworkID *mesos.FrameworkID,
	masterInfo *mesos.MasterInfo) {
	lmt := elecLogDef.SUCCESS
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := msgColor.Sprintf("FRAMEWORK REGISTERED! frameworkID = %s, master = %s",
		frameworkID, masterInfo)
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogFrameworkReregistered(masterInfo *mesos.MasterInfo) {
	lmt := elecLogDef.GENERAL
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := msgColor.Sprintf("Framework re-registered with master %s", masterInfo)
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogDisconnected() {
	lmt := elecLogDef.WARNING
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := msgColor.Sprint("Framework disconnected with master")
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogTaskStatusUpdate(status *mesos.TaskStatus) {
	var lmt elecLogDef.LogMessageType
	switch *status.State {
	case mesos.TaskState_TASK_ERROR, mesos.TaskState_TASK_FAILED,
		mesos.TaskState_TASK_KILLED, mesos.TaskState_TASK_LOST:
		lmt = elecLogDef.ERROR
	case mesos.TaskState_TASK_FINISHED:
		lmt = elecLogDef.SUCCESS
	default:
		lmt = elecLogDef.GENERAL
	}
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := elecLogDef.LogMessageColors[elecLogDef.GENERAL].Sprintf("Task Status received for task [%s] --> %s",
		*status.TaskId.Value, msgColor.Sprint(mesosUtils.NameFor(status.State)))
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogSchedPolicySwitch(name string, nextPolicy SchedPolicyState) {
	logSPS := func() {
		s.Log(elecLogDef.SPS, name)
	}
	if s.hasReceivedResourceOffers && (s.curSchedPolicy != nextPolicy) {
		logSPS()
	} else if !s.hasReceivedResourceOffers {
		logSPS()
	}
	// Logging the size of the scheduling window and the scheduling policy
	// 	that is going to schedule the tasks in the scheduling window.
	s.Log(elecLogDef.SCHED_WINDOW, fmt.Sprintf("%d %s", s.schedWindowSize, name))
}

func (s *BaseScheduler) LogClsfnAndTaskDistOverhead(overhead time.Duration) {
	// Logging the overhead in microseconds.
	s.Log(elecLogDef.CLSFN_TASKDIST_OVERHEAD, fmt.Sprintf("%f", float64(overhead.Nanoseconds())/1000.0))
}

func (s *BaseScheduler) LogShutdownScheduling() {
	lmt := elecLogDef.SUCCESS
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := msgColor.Sprint("Shutting down scheduling...")
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogShutdownFramework() {
	lmt := elecLogDef.SUCCESS
	msgColor := elecLogDef.LogMessageColors[lmt]
	msg := msgColor.Sprint("Shutting down framework...")
	s.Log(lmt, msg)
}

func (s *BaseScheduler) LogMetrics() {
	for _, listeners := range s.eventListeners {
		for _, l := range listeners {
			l.WriteLogs()
		}
	}
}
