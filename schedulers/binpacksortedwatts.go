package schedulers

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"bitbucket.org/sunybingcloud/electron/utilities/mesosUtils"
	"bitbucket.org/sunybingcloud/electron/utilities/offerUtils"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	sched "github.com/mesos/mesos-go/api/v0/scheduler"
)

type BinPackSortedWatts struct {
	baseSchedPolicyState
}

func (s *BinPackSortedWatts) ConsumeOffers(spc SchedPolicyContext, driver sched.SchedulerDriver, offers []*mesos.Offer) {
	baseSchedRef := spc.(*BaseScheduler)
	switch baseSchedRef.tasks.(type) {
	case taskQueue.SortableTaskQueue:
		if baseSchedRef.schedPolSwitchEnabled {
			baseSchedRef.tasks.(taskQueue.SortableTaskQueue).SortN(baseSchedRef.numTasksInSchedWindow, def.SortByWatts)
		} else {
			baseSchedRef.tasks.(taskQueue.SortableTaskQueue).Sort(def.SortByWatts)
		}
	default:
		// Do nothing. Task queue is not sortable.
	}
	baseSchedRef.LogOffersReceived(offers)

	for _, offer := range offers {
		offerUtils.UpdateEnvironment(offer)
		if baseSchedRef.ShutdownScheduling.IsClosed() {
			baseSchedRef.LogNoPendingTasksDeclineOffers(offer)
			driver.DeclineOffer(offer.Id, mesosUtils.LongFilter)
			baseSchedRef.LogNumberOfRunningTasks()
			continue
		}

		tasks := []*mesos.TaskInfo{}

		offerTaken := false
		totalCPU := 0.0
		totalRAM := 0.0
		var iter taskQueue.Iterator
		for iter = baseSchedRef.tasks.BeginIter(); iter.HasNext(); {
			task := iter.NextTask()
			// Don't take offer if it doesn't match our task's host requirement
			if offerUtils.HostMismatch(*offer.Hostname, task.GetHost()) {
				continue
			}

			for task.GetInstances() > 0 {
				// If scheduling policy switching enabled, then
				// stop scheduling if the #baseSchedRef.schedWindowSize tasks have been scheduled.
				if baseSchedRef.schedPolSwitchEnabled &&
					(s.numTasksScheduled >= baseSchedRef.schedWindowSize) {
					break // Offers will automatically get declined.
				}
				// Does the task fit
				if s.takePartlyConsumedOffer(offer, task, totalCPU, totalRAM) {

					offerTaken = true
					totalCPU += task.GetCpu()
					totalRAM += task.GetRam()
					baseSchedRef.LogCoLocatedTasks(offer.GetSlaveId().GoString())
					taskToSchedule := baseSchedRef.newTaskInfo(offer, task, s.resourcesToReserve(task, offer))
					tasks = append(tasks, taskToSchedule)

					baseSchedRef.LogSchedTrace(taskToSchedule, offer)
					task.DecrementInstances()
					s.numTasksScheduled++

					if task.GetInstances() <= 0 {
						iter.RemoveTraversedTask()

						if baseSchedRef.tasks.Len() <= 0 {
							baseSchedRef.ExamineShutdownOfScheduling()
						}
					}
				} else {
					break // Continue on to next task
				}
			}
		}

		if offerTaken {
			baseSchedRef.LogTaskStarting(nil, offer)
			LaunchTasks([]*mesos.OfferID{offer.Id}, tasks, driver)
		} else {

			// If there was no match for the task
			cpus, mem := offerUtils.OfferAgg(offer)
			baseSchedRef.LogInsufficientResourcesDeclineOffer(offer, cpus, mem)
			driver.DeclineOffer(offer.Id, mesosUtils.DefaultFilter)
		}
	}
}
