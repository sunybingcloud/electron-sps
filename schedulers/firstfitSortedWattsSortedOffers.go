package schedulers

import (
	"bitbucket.org/sunybingcloud/electron/def"
	elecLogDef "bitbucket.org/sunybingcloud/electron/logging/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"bitbucket.org/sunybingcloud/electron/utilities/mesosUtils"
	"bitbucket.org/sunybingcloud/electron/utilities/offerUtils"
	"bytes"
	"fmt"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	sched "github.com/mesos/mesos-go/api/v0/scheduler"
	"log"
	"sort"
)

// electronScheduler implements the Scheduler interface
type FirstFitSortedWattsSortedOffers struct {
	baseSchedPolicyState
}

func (s *FirstFitSortedWattsSortedOffers) ConsumeOffers(spc SchedPolicyContext, driver sched.SchedulerDriver,
	offers []*mesos.Offer) {
	baseSchedRef := spc.(*BaseScheduler)
	switch baseSchedRef.tasks.(type) {
	case taskQueue.SortableTaskQueue:
		if baseSchedRef.schedPolSwitchEnabled {
			baseSchedRef.tasks.(taskQueue.SortableTaskQueue).SortN(baseSchedRef.numTasksInSchedWindow, def.SortByWatts)
		} else {
			baseSchedRef.tasks.(taskQueue.SortableTaskQueue).Sort(def.SortByWatts)
		}
	default:
		// Do nothing. The task queue is not sortable.
	}
	baseSchedRef.LogOffersReceived(offers)
	// Sorting the offers
	sort.Sort(offerUtils.OffersSorter(offers))

	// Printing the sorted offers and the corresponding CPU resource availability
	buffer := bytes.Buffer{}
	buffer.WriteString(fmt.Sprintln("Sorted Offers:"))
	for i := 0; i < len(offers); i++ {
		offer := offers[i]
		offerUtils.UpdateEnvironment(offer)
		offerCPU, _ := offerUtils.OfferAgg(offer)
		buffer.WriteString(fmt.Sprintf("Offer[%s].CPU = %f\n", offer.GetHostname(), offerCPU))
	}
	baseSchedRef.Log(elecLogDef.GENERAL, buffer.String())

	for _, offer := range offers {
		if baseSchedRef.ShutdownScheduling.IsClosed() {
			baseSchedRef.LogNoPendingTasksDeclineOffers(offer)
			driver.DeclineOffer(offer.Id, mesosUtils.LongFilter)
			baseSchedRef.LogNumberOfRunningTasks()
			continue
		}

		tasks := []*mesos.TaskInfo{}

		// First fit strategy

		offerTaken := false
		var iter taskQueue.Iterator
		for iter = baseSchedRef.tasks.BeginIter(); iter.HasNext(); {
			// If scheduling policy switching enabled, then
			// stop scheduling if the #baseSchedRef.schedWindowSize tasks have been scheduled.
			if baseSchedRef.schedPolSwitchEnabled && (s.numTasksScheduled >= baseSchedRef.schedWindowSize) {
				log.Printf("Stopped scheduling... Completed scheduling %d tasks.",
					s.numTasksScheduled)
				break // Offers will automatically get declined.
			}
			task := iter.NextTask()

			// Don't take offer if it doesn't match our task's host requirement
			if offerUtils.HostMismatch(*offer.Hostname, task.GetHost()) {
				continue
			}

			// Decision to take the offer or not
			if s.takeNewOffer(offer, task) {

				baseSchedRef.LogCoLocatedTasks(offer.GetSlaveId().GoString())

				taskToSchedule := baseSchedRef.newTaskInfo(offer, task, s.resourcesToReserve(task, offer))
				tasks = append(tasks, taskToSchedule)

				baseSchedRef.LogTaskStarting(task, offer)
				LaunchTasks([]*mesos.OfferID{offer.Id}, tasks, driver)

				offerTaken = true

				baseSchedRef.LogSchedTrace(taskToSchedule, offer)
				task.DecrementInstances()
				s.numTasksScheduled++

				if task.GetInstances() <= 0 {
					// All instances of task have been scheduled, remove it
					iter.RemoveTraversedTask()

					if baseSchedRef.tasks.Len() <= 0 {
						baseSchedRef.ExamineShutdownOfScheduling()
					}
				}
				break // Offer taken, move on
			}
		}

		// If there was no match for the task
		if !offerTaken {
			cpus, mem := offerUtils.OfferAgg(offer)
			baseSchedRef.LogInsufficientResourcesDeclineOffer(offer, cpus, mem)
			driver.DeclineOffer(offer.Id, mesosUtils.DefaultFilter)
		}
	}
}
