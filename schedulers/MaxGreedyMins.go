package schedulers

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"bitbucket.org/sunybingcloud/electron/utilities/mesosUtils"
	"bitbucket.org/sunybingcloud/electron/utilities/offerUtils"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	sched "github.com/mesos/mesos-go/api/v0/scheduler"
)

type MaxGreedyMins struct {
	baseSchedPolicyState
}

// Determine if the remaining space inside of the offer is enough for this
// the task we need to create. If it is, create a TaskInfo and return it.
func (s *MaxGreedyMins) CheckFit(
	spc SchedPolicyContext,
	iter taskQueue.Iterator,
	task def.Task,
	offer *mesos.Offer,
	totalCPU *float64,
	totalRAM *float64) (bool, *mesos.TaskInfo) {

	baseSchedRef := spc.(*BaseScheduler)
	// Does the task fit
	if s.takePartlyConsumedOffer(offer, task, *totalCPU, *totalRAM) {

		*totalCPU += task.GetCpu()
		*totalRAM += task.GetRam()
		baseSchedRef.LogCoLocatedTasks(offer.GetSlaveId().GoString())

		taskToSchedule := baseSchedRef.newTaskInfo(offer, task, s.resourcesToReserve(task, offer))

		baseSchedRef.LogSchedTrace(taskToSchedule, offer)
		task.DecrementInstances()
		s.numTasksScheduled++

		if task.GetInstances() <= 0 {
			// All instances of task have been scheduled, remove it
			iter.RemoveTraversedTask()

			if baseSchedRef.tasks.Len() <= 0 {
				baseSchedRef.ExamineShutdownOfScheduling()
			}
		}

		return true, taskToSchedule
	}

	return false, nil
}

func (s *MaxGreedyMins) ConsumeOffers(spc SchedPolicyContext, driver sched.SchedulerDriver, offers []*mesos.Offer) {
	baseSchedRef := spc.(*BaseScheduler)
	switch baseSchedRef.tasks.(type) {
	case taskQueue.SortableTaskQueue:
		if baseSchedRef.schedPolSwitchEnabled {
			baseSchedRef.tasks.(taskQueue.SortableTaskQueue).SortN(baseSchedRef.numTasksInSchedWindow, def.SortByWatts)
		} else {
			baseSchedRef.tasks.(taskQueue.SortableTaskQueue).Sort(def.SortByWatts)
		}
	}
	baseSchedRef.LogOffersReceived(offers)

	for _, offer := range offers {
		offerUtils.UpdateEnvironment(offer)
		if baseSchedRef.ShutdownScheduling.IsClosed() {
			baseSchedRef.LogNoPendingTasksDeclineOffers(offer)
			driver.DeclineOffer(offer.Id, mesosUtils.LongFilter)
			baseSchedRef.LogNumberOfRunningTasks()
			continue
		}

		tasks := []*mesos.TaskInfo{}

		offerTaken := false
		totalCPU := 0.0
		totalRAM := 0.0

		// Assumes s.tasks is ordered in non-decreasing median max peak order

		// Attempt to schedule a single instance of the heaviest workload available first
		// Start from the back until one fits
		var rIter taskQueue.ReverseIterator
		for rIter = baseSchedRef.tasks.BeginReverseIter(); rIter.HasNext(); {
			// If scheduling policy switching enabled, then
			// stop scheduling if the #baseSchedRef.schedWindowSize tasks have been scheduled.
			if baseSchedRef.schedPolSwitchEnabled && (s.numTasksScheduled >= baseSchedRef.schedWindowSize) {
				break // Offers will automatically get declined.
			}
			task := rIter.NextTask()
			// Don't take offer if it doesn't match our task's host requirement
			if offerUtils.HostMismatch(*offer.Hostname, task.GetHost()) {
				continue
			}

			taken, taskToSchedule := s.CheckFit(spc, rIter, task, offer, &totalCPU, &totalRAM)

			if taken {
				offerTaken = true
				tasks = append(tasks, taskToSchedule)
				break
			}
		}

		// Pack the rest of the offer with the smallest tasks
		var iter taskQueue.Iterator
		for iter = baseSchedRef.tasks.BeginIter(); iter.HasNext(); {
			task := iter.NextTask()
			// Don't take offer if it doesn't match our task's host requirement
			if offerUtils.HostMismatch(*offer.Hostname, task.GetHost()) {
				continue
			}

			for task.GetInstances() > 0 {
				// If scheduling policy switching enabled, then
				// stop scheduling if the #baseSchedRef.schedWindowSize tasks have been scheduled.
				if baseSchedRef.schedPolSwitchEnabled && (s.numTasksScheduled >= baseSchedRef.schedWindowSize) {
					break // Offers will automatically get declined.
				}
				taken, taskToSchedule := s.CheckFit(spc, iter, task, offer, &totalCPU, &totalRAM)

				if taken {
					offerTaken = true
					tasks = append(tasks, taskToSchedule)
				} else {
					break // Continue on to next task
				}
			}
		}

		if offerTaken {
			baseSchedRef.LogTaskStarting(nil, offer)
			LaunchTasks([]*mesos.OfferID{offer.Id}, tasks, driver)
		} else {

			// If there was no match for the task
			cpus, mem := offerUtils.OfferAgg(offer)
			baseSchedRef.LogInsufficientResourcesDeclineOffer(offer, cpus, mem)
			driver.DeclineOffer(offer.Id, mesosUtils.DefaultFilter)
		}
	}
}
