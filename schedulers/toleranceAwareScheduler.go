package schedulers

import (
	"bitbucket.org/sunybingcloud/electron/def"
	elecLogDef "bitbucket.org/sunybingcloud/electron/logging/def"
	"bitbucket.org/sunybingcloud/electron/metrics"
	elecSchedUtils "bitbucket.org/sunybingcloud/electron/schedulers/utilities"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"bitbucket.org/sunybingcloud/electron/utilities/mesosUtils"
	"bitbucket.org/sunybingcloud/electron/utilities/offerUtils"
	"container/ring"
	"fmt"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	sched "github.com/mesos/mesos-go/api/v0/scheduler"
	"github.com/pkg/errors"
	"github.com/pradykaushik/task-ranker/entities"
	"math"
	"sort"
	"sync"
)

// ToleranceAwareScheduler is a scheduling policy that aims to allocate resources to tasks
// by factoring in their resource tolerance levels and of the running tasks.
type ToleranceAwareScheduler struct {
	baseSchedPolicyState
	// Keeping track of the consumed resources for every offer.
	// OfferID => {cpu, mem}
	consumedResourcesInOffer map[string]*struct{ cpu, mem float64 }
	// cpuUtilDeltas holds the changes in cpu utilization.
	// {host => {taskID => [delta1 delta2 ...]}}
	cpuUtilDeltas map[string]map[string]*histCpuUtilChangeInfoTask
	// Using an RWMutex as reads from cpuUtilDeltas can be much more frequent than writes.
	// In addition, writes are periodic while reads are not.
	cpuUtilDeltasMu sync.RWMutex
}

// histCpuUtilChangeInfoTask represents the information pertaining to the past cpu utilization of the task.
type histCpuUtilChangeInfoTask struct {
	// The latest cpu utilization value received. Used for calculating the next delta.
	latestCpuUtil float64
	// Changes in cpu utilization in the past window.
	deltas *ring.Ring
	// Number of values recorded so far.
	numValues int
	// Cpu utilization estimate for the next window.
	cpuUtilEstimate float64
	// validity of the estimate. If a new data point is received, then the estimate is no longer valid.
	validCpuUtilEstimate bool
	// Error in the estimation.
	err float64
}

// CpuUtilEstimationWindowSize is the max number of changes in cpu utilization to be considered for estimation.
const CpuUtilEstimationWindowSize int = 5

// estimateCpuUtil estimates the cpu utilization of the task using the changes in cpu utilization in the past window.
// The relevance of the previous changes reduces as they as newer changes are recorded. Therefore, the most recent
// fluctuations in cpu utilization have a higher relevance in the estimation of cpu utilization in the next window.
//
// Cite (Srinidhi) - https://bitbucket.org/sunybingcloud/cpuutil-estimation/src/estimation-1-exponential-value-decrease/
func (c *histCpuUtilChangeInfoTask) estimateCpuUtil() float64 {
	if c.validCpuUtilEstimate {
		return c.cpuUtilEstimate
	}
	if c.deltas.Len() > 0 {
		// recalculating the error.
		weightedIntervalDiff := 0.0
		age := 0
		for i := 0; i < c.deltas.Len(); i++ {
			c.deltas = c.deltas.Prev()
			if c.deltas.Value != nil {
				relevance := math.Pow(2, float64(age))
				weightedIntervalDiff += c.deltas.Value.(float64) * relevance
				age--
			}
		}

		c.cpuUtilEstimate = c.latestCpuUtil + weightedIntervalDiff - c.err
		c.validCpuUtilEstimate = true
		return c.cpuUtilEstimate
	}
	// If here, then we have no changes in cpu utilization recorded, then just returning the previous value recorded.
	return c.latestCpuUtil
}

func (s *ToleranceAwareScheduler) Init(opts ...SchedPolicyOptions) {
	s.baseSchedPolicyState.Init(opts...)
	s.cpuUtilDeltas = make(map[string]map[string]*histCpuUtilChangeInfoTask)
}

func (s *ToleranceAwareScheduler) remMem(offer *mesos.Offer) float64 {
	_, mem := offerUtils.OfferAgg(offer)
	// Subtracting any used memory.
	if usedRes, ok := s.consumedResourcesInOffer[*offer.Id.Value]; ok {
		mem -= (*usedRes).mem
	}
	return mem
}

func (s *ToleranceAwareScheduler) remCPU(offer *mesos.Offer) float64 {
	cpu, _ := offerUtils.OfferAgg(offer)
	// Subtracting any used CPU.
	if usedRes, ok := s.consumedResourcesInOffer[*offer.Id.Value]; ok {
		cpu -= (*usedRes).cpu
	}
	return cpu
}

func (s *ToleranceAwareScheduler) canFitMem(offer *mesos.Offer, taskMem float64) bool {
	return s.remMem(offer) >= taskMem
}

func (s *ToleranceAwareScheduler) canFitCPU(offer *mesos.Offer, taskCPU float64) bool {
	return s.remCPU(offer) >= taskCPU
}

// Find the offer that is the best-fit in terms of memory for the given task.
// Return the index of the respective offer.
// Note: The offers should already have been sorted in non-decreasing order of Memory.
func (s *ToleranceAwareScheduler) bestFitMem(offers []*mesos.Offer, task def.Task) (int, error) {
	first := offers[0]
	last := offers[len(offers)-1]
	if s.canFitMem(first, task.GetRam()) {
		return 0, nil
	}
	if s.remMem(last) <= task.GetRam() {
		return -1, errors.New("No offer has sufficient memory for task!")
	}
	low := 0
	high := len(offers) - 1
	bestFitIndex := -1
	for low <= high {
		mid := (low + high) / 2
		unusedMemMid := s.remMem(offers[mid])
		if task.GetRam() < unusedMemMid {
			high = mid - 1
		} else if task.GetRam() > unusedMemMid {
			low = mid + 1
		} else {
			bestFitIndex = mid
			break
		}
	}

	// If the best fit index hasn't yet been found.
	if bestFitIndex == -1 {
		bestFitIndex = low
	}

	return bestFitIndex, nil
}

func (s *ToleranceAwareScheduler) updateConsumedResourcesOffer(offerID string, task def.Task) {
	if s.consumedResourcesInOffer == nil {
		s.consumedResourcesInOffer = make(map[string]*struct{ cpu, mem float64 })
	}
	if consumedResources, ok := s.consumedResourcesInOffer[offerID]; ok {
		(*consumedResources).cpu += task.GetCpu()
		(*consumedResources).mem += task.GetRam()
	} else {
		s.consumedResourcesInOffer[offerID] = &struct {
			cpu, mem float64
		}{
			cpu: task.GetCpu(),
			mem: task.GetRam(),
		}
	}
}

// toleranceBreached checks if the tolerance for any of the running tasks on the given host
// is going to be breached by colocating the given task on the same host machine.
func (s *ToleranceAwareScheduler) toleranceBreached(
	spc SchedPolicyContext,
	offer *mesos.Offer,
	host string,
	resAllocInfo map[string]metrics.AllocationInfo,
	fitTasks map[string]metrics.FitTaskInfo,
	newTask def.Task) bool {

	totalAllocatedCpuShare := newTask.GetCpu()
	for _, allocInfo := range resAllocInfo {
		totalAllocatedCpuShare += allocInfo.CpuShare
	}

	// Tasks that have just been fit into offers.
	for _, fitTaskInfo := range fitTasks {
		totalAllocatedCpuShare += fitTaskInfo.CPU
	}

	// Tolerance checks for all running tasks.
	//
	// A tolerance breach is said to occur if the actual share of cpu that a task gets,
	// if this new task is to colocated on the same host, is less than its average cpu utilization in
	// the past 5 intervals of time.
	const formatString = "[TASK_ID:%s,NEW_ACTUAL_SHARE_CPU_MULTICORE:%f,ESTIMATED_CPU_UTIL:%f,TOLERANCE:%f,TOLERANCE_BREACH:%t]"
	hostResourceAvailability := elecSchedUtils.GetPerHostResourceAvailability(offer.SlaveId)
	s.cpuUtilDeltasMu.RLock()
	defer s.cpuUtilDeltasMu.RUnlock()

	if _, ok := s.cpuUtilDeltas[host]; ok {
		for taskID, histCpuUtilInfo := range s.cpuUtilDeltas[host] {
			if allocInfo, ok := resAllocInfo[taskID]; ok {
				newActualShareCpu := allocInfo.CpuShare / totalAllocatedCpuShare
				// Docker reports cpu utilization information as that of a single cpu.
				// So, if a task is using 50% of a machine that has 24 cpus, docker reports
				// the cpu utilization as 1200%.
				// For tolerance breach check,
				// 1. We need to either convert newActualShareCpu into a multi-core format (50% on a 24cpu machine = 1200%)
				// 2. Convert the task's cpu utilization, as reported by Task-Ranker, to a value that represents
				// 		the percentage of the whole system (1200% on a 24cpu machine = 50%).
				//
				// The resource availability tracker can be used to fetch the total CPU resources available for a given host.
				// Note, however, that this information is recorded based on what is advertized by the mesos agents.
				// 	So, if the agents are configured to advertize a different amount of CPU resources, this calculation
				// 	will provide an incorrect result.
				// TODO have the mesos agents advertize the processor architecture information.
				newActualShareCpuMulticore := 100 * (hostResourceAvailability.TotalCPU * newActualShareCpu)
				futureCpuUtilEstimate := histCpuUtilInfo.estimateCpuUtil()
				// Comparing maybe new actual share of cpu resources against the task's estimated cpu utilization
				// considering its tolerance.
				if ((1.0 - allocInfo.Tolerance) * futureCpuUtilEstimate) > newActualShareCpuMulticore {
					// Logging tolerance breach.
					spc.(*BaseScheduler).Log(elecLogDef.GENERAL,
						fmt.Sprintf(formatString,
							taskID, newActualShareCpuMulticore, futureCpuUtilEstimate, allocInfo.Tolerance, true))
					return true
				}
			}
		}
	}

	return false
}

// For each task, let O[] be the set of offers that satisfy the CPU and Memory requirements.
// If the tolerance for any of the currently running tasks will not be breached by launching the new task,
// this offer is tagged as a candidate offer.
// If the tolerance is going to be breached tasks, skip the offer.
// For each of the candidate offers, determine the alignment score.
// Launch the task on the host corresponding to the offer with the best alignment score.
func (s *ToleranceAwareScheduler) ConsumeOffers(spc SchedPolicyContext, driver sched.SchedulerDriver, offers []*mesos.Offer) {
	baseSchedRef := spc.(*BaseScheduler)
	baseSchedRef.LogOffersReceived(offers)

	// Updating the environment.
	for _, offer := range offers {
		offerUtils.UpdateEnvironment(offer)
	}

	// Declining all offers if scheduling has been shutdown.
	if baseSchedRef.ShutdownScheduling.IsClosed() {
		for _, offer := range offers {
			baseSchedRef.LogNoPendingTasksDeclineOffers(offer)
			_, err := driver.DeclineOffer(offer.Id, mesosUtils.LongFilter)
			if err != nil {
				baseSchedRef.LogElectronError(errors.Wrap(err, "failed to decline offer"))
			}
		}
		baseSchedRef.LogNumberOfRunningTasks()
		return
	}

	allocMetrics := metrics.GetAllocationMetricsListener().(*metrics.AllocationMetricsListener)
	hostInfo := metrics.GetHostInfoListenerInstance().(*metrics.HostInfoListener)

	// Storing the Ids of the offers consumed.
	consumedOffers := make(map[string]bool)
	// Store the tasks to be scheduled and the offer ID of the consumed offer.
	tasksToSchedule := make(map[string][]*mesos.TaskInfo)

	var iter taskQueue.Iterator
	for iter = baseSchedRef.tasks.BeginIter(); iter.HasNext(); {
		task := iter.NextTask()
		// If there are remaining instances to be scheduled.
		if task.GetInstances() > 0 {
			// If scheduling policy switching enabled, then
			// stop scheduling if the #baseSchedRef.schedWindowSize tasks have been scheduled.
			if baseSchedRef.schedPolSwitchEnabled &&
				(s.numTasksScheduled >= baseSchedRef.schedWindowSize) {
				break // Offers will automatically get declined.
			}

			// Sorting the offers in non-decreasing order of memory.
			// Note: Resources contained in an offer change after every iteration. Hence, we sort
			// the offers after every iteration. This also ensures that we are fair to the next task.
			sort.SliceStable(offers, func(i, j int) bool {
				return s.remMem(offers[i]) <= s.remMem(offers[j])
			})
			// Finding the index of the best-fit offer for task's memory requirement.
			bestFitOfferMemIndex, err := s.bestFitMem(offers, task)
			if err != nil {
				// No offer has sufficient amount of memory.
				// Move onto next task.
				continue
			}

			// For each task, there can be more than one candidate offer that the task can fit into.
			// Among the candidate offers, the best one is picked.
			var candidateOffersIndices []int

			for offerIndex := bestFitOfferMemIndex; offerIndex < len(offers); offerIndex++ {
				if s.canFitCPU(offers[offerIndex], task.GetCpu()) {
					hostname, err := hostInfo.GetHostname(*offers[offerIndex].SlaveId.Value)
					if err != nil {
						// This is the first time running into this offer. No other tasks have been
						// launched on this host.
						//
						// Consider this offer as a potential candidate.
						candidateOffersIndices = append(candidateOffersIndices, offerIndex)
					} else {
						// Other tasks might be running on the host.
						// Need to make sure that the already running tasks are not going to be affected.
						resAllocInfoHost, fitTasksInfoHost := allocMetrics.GetAllAllocationMetricsHost(hostname)
						if !s.toleranceBreached(spc, offers[offerIndex], hostname, resAllocInfoHost, fitTasksInfoHost, task) {
							// Consider this offer as a potential candidate.
							candidateOffersIndices = append(candidateOffersIndices, offerIndex)
						} else {
							// Tasks already running on the host corresponding to this offer
							// might get affected. Hence, not selecting this offer.
							// Move on to next offer.
						}
					}
				} else {
					// This offer is not a candidate offer - doesn't satisfy CPU requirement.
					// Move on to the next offer.
				}
			}

			// Determining the alignment scores for each of the candidate offers.
			chosenOfferIndex := -1
			var maxAlignmentScore float64
			firstIter := true
			for _, candidateOfferIndex := range candidateOffersIndices {
				score, _ := s.alignmentScore(
					task,
					offers[candidateOfferIndex],
					offerUtils.OfferResources{
						CPU: s.remCPU(offers[candidateOfferIndex]),
						RAM: s.remMem(offers[candidateOfferIndex]),
					})

				if firstIter {
					maxAlignmentScore = score
					firstIter = false
					chosenOfferIndex = candidateOfferIndex
				} else {
					if score > maxAlignmentScore {
						maxAlignmentScore = score
						chosenOfferIndex = candidateOfferIndex
					}
				}
			}

			// Fit task into chosen offer if present.
			if chosenOfferIndex != -1 {
				chosenOfferID := *offers[chosenOfferIndex].Id.Value
				baseSchedRef.LogCoLocatedTasks(offers[chosenOfferIndex].GetSlaveId().GoString())
				if _, ok := tasksToSchedule[chosenOfferID]; !ok {
					tasksToSchedule[chosenOfferID] = make([]*mesos.TaskInfo, 0, 0)
				}
				tasksToSchedule[chosenOfferID] = append(tasksToSchedule[chosenOfferID],
					baseSchedRef.newTaskInfo(offers[chosenOfferIndex], task,
						s.resourcesToReserve(task, offers[chosenOfferIndex])))
				consumedOffers[chosenOfferID] = true
				// Updating consumed resources.
				s.updateConsumedResourcesOffer(chosenOfferID, task)
				// Another instance of task has been scheduled.
				task.DecrementInstances()
				s.numTasksScheduled++

				if task.GetInstances() <= 0 {
					// All instances of task have been scheduled, remove it.
					iter.RemoveTraversedTask()

					if baseSchedRef.tasks.Len() <= 0 {
						baseSchedRef.ExamineShutdownOfScheduling()
					}
				}
			}
		} else {
			// If here, then number of instances specified as 0 in the submitted workload.
			iter.RemoveTraversedTask()
			if baseSchedRef.tasks.Len() <= 0 {
				baseSchedRef.ExamineShutdownOfScheduling()
			}
		}
	}

	for _, offer := range offers {
		if _, ok := consumedOffers[*offer.Id.Value]; ok {
			for _, t := range tasksToSchedule[*offer.Id.Value] {
				baseSchedRef.LogSchedTrace(t, offer)
			}
			LaunchTasks([]*mesos.OfferID{offer.Id}, tasksToSchedule[*offer.Id.Value], driver)
		} else {
			// Offer was not consumed.
			// Declining offer.
			cpus, mem := offerUtils.OfferAgg(offer)
			baseSchedRef.LogInsufficientResourcesDeclineOffer(offer, cpus, mem)
			driver.DeclineOffer(offer.Id, mesosUtils.DefaultFilter)
		}
	}

	// Clearing consumedResourcesInOffer so that future tasks are not affected.
	s.consumedResourcesInOffer = make(map[string]*struct{ cpu, mem float64 })
}

// ReceiveTaskRankingResults receives the task ranking results and records the changes in the cpu utilization of tasks.
//
// In addition, the cpu utilization estimation error is adjusted.
// Cite (Srinidhi) - https://bitbucket.org/sunybingcloud/cpuutil-estimation/src/estimation-1-exponential-value-decrease/
func (s *ToleranceAwareScheduler) ReceiveTaskRankingResults(_ SchedPolicyContext, rankedTasks entities.RankedTasks) {
	s.cpuUtilDeltasMu.Lock()
	defer s.cpuUtilDeltasMu.Unlock()
	for host, rankedColocatedTasks := range rankedTasks {
		for _, task := range rankedColocatedTasks {
			if _, ok := s.cpuUtilDeltas[string(host)]; !ok {
				s.cpuUtilDeltas[string(host)] = make(map[string]*histCpuUtilChangeInfoTask)
			}
			// If first time recording cpu utilization for task, then adding entry.
			if _, ok := s.cpuUtilDeltas[string(host)][task.ID]; !ok {
				s.cpuUtilDeltas[string(host)][task.ID] = &histCpuUtilChangeInfoTask{
					latestCpuUtil:        task.Weight,
					deltas:               ring.New(CpuUtilEstimationWindowSize),
					numValues:            0,
					cpuUtilEstimate:      0.0,
					validCpuUtilEstimate: false,
					err:                  0.0,
				}
			} else {
				// Recording the new change in cpu utilization.
				histCpuUtilInfo := s.cpuUtilDeltas[string(host)][task.ID]
				newDelta := task.Weight - histCpuUtilInfo.latestCpuUtil
				histCpuUtilInfo.deltas.Value = newDelta
				histCpuUtilInfo.deltas = histCpuUtilInfo.deltas.Next()
				// Updating the latest cpu utilization value.
				histCpuUtilInfo.latestCpuUtil = task.Weight
				histCpuUtilInfo.numValues++
				// Updating the estimation error based on the newly monitored cpu utilization.
				if histCpuUtilInfo.validCpuUtilEstimate {
					histCpuUtilInfo.err = ((histCpuUtilInfo.err * float64(histCpuUtilInfo.numValues-1)) +
						(histCpuUtilInfo.cpuUtilEstimate - task.Weight)) / float64(histCpuUtilInfo.numValues)
				}
				// Invalidating the most recent estimate.
				histCpuUtilInfo.validCpuUtilEstimate = false
			}
		}
	}
}
