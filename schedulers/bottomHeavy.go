package schedulers

import (
	"bitbucket.org/sunybingcloud/electron/constants"
	"bitbucket.org/sunybingcloud/electron/def"
	elecLogDef "bitbucket.org/sunybingcloud/electron/logging/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"bitbucket.org/sunybingcloud/electron/utilities/mesosUtils"
	"bitbucket.org/sunybingcloud/electron/utilities/offerUtils"
	"bytes"
	"fmt"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	sched "github.com/mesos/mesos-go/api/v0/scheduler"
)

/*
Tasks are categorized into small and large tasks based on watts requirements.
All the large tasks are packed into offers from agents belonging to power classes A and B, using Bin-Packing.
All the small tasks are spread among offers from agents belonging to power class C and D, using First-Fit.

Bin-Packing has the most effect when co-scheduling of tasks is increased. Large tasks typically utilize more resources and hence,
co-scheduling them has a great impact on the total power utilization.
*/

// BottomHeavy is a scheduling policy that attempts to pack large tasks into class A and class B hosts, while
// spreading smaller tasks across class C and class D hosts.
// Note - BottomHeavy can only work with a fixed size task queue.
type BottomHeavy struct {
	baseSchedPolicyState
	smallTasks, largeTasks []def.Task
}

// Shut down scheduler if no more tasks to schedule
func (s *BottomHeavy) shutDownIfNecessary(spc SchedPolicyContext) {
	baseSchedRef := spc.(*BaseScheduler)
	if len(s.smallTasks) <= 0 && len(s.largeTasks) <= 0 {
		baseSchedRef.ExamineShutdownOfScheduling()
	}
}

// create TaskInfo and log scheduling trace
func (s *BottomHeavy) createTaskInfoAndLogSchedTrace(spc SchedPolicyContext, offer *mesos.Offer,
	task def.Task) *mesos.TaskInfo {
	baseSchedRef := spc.(*BaseScheduler)
	baseSchedRef.LogCoLocatedTasks(offer.GetSlaveId().GoString())
	taskToSchedule := baseSchedRef.newTaskInfo(offer, task, s.resourcesToReserve(task, offer))
	baseSchedRef.LogSchedTrace(taskToSchedule, offer)
	task.DecrementInstances()
	return taskToSchedule
}

// Using BinPacking to pack large tasks into the given offers.
func (s *BottomHeavy) pack(spc SchedPolicyContext, offers []*mesos.Offer, driver sched.SchedulerDriver) {
	baseSchedRef := spc.(*BaseScheduler)
	for _, offer := range offers {
		if baseSchedRef.ShutdownScheduling.IsClosed() {
			baseSchedRef.LogNoPendingTasksDeclineOffers(offer)
			driver.DeclineOffer(offer.Id, mesosUtils.LongFilter)
			baseSchedRef.LogNumberOfRunningTasks()
			continue
		}

		tasks := []*mesos.TaskInfo{}
		totalCPU := 0.0
		totalRAM := 0.0
		offerTaken := false
		for i := 0; i < len(s.largeTasks); i++ {
			task := s.largeTasks[i]

			for task.GetInstances() > 0 {
				// Does the task fit
				// OR lazy evaluation. If ignore watts is set to true, second statement won't
				// be evaluated.
				if s.takePartlyConsumedOffer(offer, task, totalCPU, totalRAM) {
					offerTaken = true
					totalCPU += task.GetCpu()
					totalRAM += task.GetRam()
					tasks = append(tasks, s.createTaskInfoAndLogSchedTrace(spc, offer, task))

					if task.GetInstances() <= 0 {
						// All instances of task have been scheduled, remove it
						s.largeTasks = append(s.largeTasks[:i], s.largeTasks[i+1:]...)
						s.shutDownIfNecessary(spc)
					}
				} else {
					break // Continue on to next task
				}
			}
		}

		if offerTaken {
			baseSchedRef.LogTaskStarting(nil, offer)
			LaunchTasks([]*mesos.OfferID{offer.Id}, tasks, driver)
		} else {
			// If there was no match for the task
			cpus, mem := offerUtils.OfferAgg(offer)
			baseSchedRef.LogInsufficientResourcesDeclineOffer(offer, cpus, mem)
			driver.DeclineOffer(offer.Id, mesosUtils.DefaultFilter)
		}
	}
}

// Using First-Fit to spread small tasks among the given offers.
func (s *BottomHeavy) spread(spc SchedPolicyContext, offers []*mesos.Offer, driver sched.SchedulerDriver) {
	baseSchedRef := spc.(*BaseScheduler)
	for _, offer := range offers {
		if baseSchedRef.ShutdownScheduling.IsClosed() {
			baseSchedRef.LogNoPendingTasksDeclineOffers(offer)
			driver.DeclineOffer(offer.Id, mesosUtils.LongFilter)
			baseSchedRef.LogNumberOfRunningTasks()
			continue
		}

		tasks := []*mesos.TaskInfo{}
		offerTaken := false
		for i := 0; i < len(s.smallTasks); i++ {
			task := s.smallTasks[i]

			// Decision to take the offer or not
			if s.takeNewOffer(offer, task) {
				offerTaken = true
				tasks = append(tasks, s.createTaskInfoAndLogSchedTrace(spc, offer, task))
				baseSchedRef.LogTaskStarting(s.smallTasks[i], offer)
				LaunchTasks([]*mesos.OfferID{offer.Id}, tasks, driver)

				if task.GetInstances() <= 0 {
					// All instances of task have been scheduled, remove it
					s.smallTasks = append(s.smallTasks[:i], s.smallTasks[i+1:]...)
					s.shutDownIfNecessary(spc)
				}
				break // Offer taken, move on
			}
		}

		if !offerTaken {
			// If there was no match for the task
			cpus, mem := offerUtils.OfferAgg(offer)
			baseSchedRef.LogInsufficientResourcesDeclineOffer(offer, cpus, mem)
			driver.DeclineOffer(offer.Id, mesosUtils.DefaultFilter)
		}
	}
}

func (s *BottomHeavy) ConsumeOffers(spc SchedPolicyContext, driver sched.SchedulerDriver, offers []*mesos.Offer) {
	baseSchedRef := spc.(*BaseScheduler)
	baseSchedRef.LogOffersReceived(offers)
	switch baseSchedRef.tasks.(type) {
	case taskQueue.SortableTaskQueue:
		baseSchedRef.tasks.(taskQueue.SortableTaskQueue).Sort(def.SortByWatts)
	default:
		// Do nothing. Task queue is not sortable.
	}
	// Classification done based on MMPU watts requirements, into 2 clusters.
	classifiedTasks := def.ClassifyTasks(baseSchedRef.tasks.GetTasks(), 2)
	// Separating small tasks from large tasks.
	s.smallTasks = classifiedTasks[0].Tasks
	s.largeTasks = classifiedTasks[1].Tasks

	// We need to separate the offers into
	// offers from ClassA and ClassB and offers from ClassC and ClassD.
	// Nodes in ClassA and ClassB will be packed with the large tasks.
	// Small tasks will be spread out among the nodes in ClassC and ClassD.
	offersHeavyPowerClasses := []*mesos.Offer{}
	offersLightPowerClasses := []*mesos.Offer{}

	for _, offer := range offers {
		offerUtils.UpdateEnvironment(offer)
		if baseSchedRef.ShutdownScheduling.IsClosed() {
			baseSchedRef.LogNoPendingTasksDeclineOffers(offer)
			driver.DeclineOffer(offer.Id, mesosUtils.LongFilter)
			baseSchedRef.LogNumberOfRunningTasks()
			continue
		}

		if _, ok := constants.PowerClasses["A"][*offer.Hostname]; ok {
			offersHeavyPowerClasses = append(offersHeavyPowerClasses, offer)
		}
		if _, ok := constants.PowerClasses["B"][*offer.Hostname]; ok {
			offersHeavyPowerClasses = append(offersHeavyPowerClasses, offer)
		}
		if _, ok := constants.PowerClasses["C"][*offer.Hostname]; ok {
			offersLightPowerClasses = append(offersLightPowerClasses, offer)
		}
		if _, ok := constants.PowerClasses["D"][*offer.Hostname]; ok {
			offersLightPowerClasses = append(offersLightPowerClasses, offer)
		}

	}

	buffer := bytes.Buffer{}
	buffer.WriteString(fmt.Sprintln("Packing Large tasks into ClassAB offers:"))
	for _, o := range offersHeavyPowerClasses {
		buffer.WriteString(fmt.Sprintln(*o.Hostname))

	}
	baseSchedRef.Log(elecLogDef.GENERAL, buffer.String())
	buffer.Reset()
	// Packing tasks into offersHeavyPowerClasses
	s.pack(spc, offersHeavyPowerClasses, driver)

	buffer.WriteString(fmt.Sprintln("Packing Small tasks among ClassCD offers:"))
	for _, o := range offersLightPowerClasses {
		buffer.WriteString(*o.Hostname)
	}
	baseSchedRef.Log(elecLogDef.GENERAL, buffer.String())
	// Spreading tasks among offersLightPowerClasses
	s.spread(spc, offersLightPowerClasses, driver)
}
