EXECUTABLE=electron
executable=$(EXECUTABLE)

all: build

build:
	go build -v -o $(executable)

build-linux:
	env GOARCH=amd64 GOOS=linux go build -v -o $(executable)

run: build
	go run ./$(executable)

test: 
	go test ./...

test-verbose:
	go test -v ./...

test-module:
	go test $(module)

test-module-verbose:
	go test -v $(module)

format:
	go fmt ./...

clean:
	go clean
	rm -r $(executable)
