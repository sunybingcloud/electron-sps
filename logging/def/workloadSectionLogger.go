package logging

type WorkloadSectionLogger struct {
	loggerObserverImpl
}

func (wsl WorkloadSectionLogger) Log(message string) {
	wsl.logObserverSpecifics[workloadSectionLogger].logFile.Println(message)
}
