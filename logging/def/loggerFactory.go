package logging

import (
	logUtils "bitbucket.org/sunybingcloud/electron/logging/utils"
	"strings"
	"time"
)

// Names of different loggers
const (
	conLogger                   = "console-logger"
	schedTraceLogger            = "schedTrace-logger"
	pcpLogger                   = "pcp-logger"
	degColLogger                = "degCol-logger"
	spsLogger                   = "schedPolicySwitch-logger"
	clsfnTaskDistOverheadLogger = "classificationOverhead-logger"
	schedWindowLogger           = "schedWindow-logger"
	runtimeMetricsLogger        = "runtime-metrics-logger"
	utilizationMetricsLogger    = "utilization-metrics-logger"
	allocationMetricsLogger     = "allocation-metrics-logger"
	allocationStatsLogger       = "allocation-stats-logger"
	workloadSectionLogger       = "workload-sectionTag-logger"
	rankedTasksLogger           = "ranked-tasks-logger"
)

// Logger class factory
var Loggers = map[string]loggerObserver{
	conLogger:                   nil,
	schedTraceLogger:            nil,
	pcpLogger:                   nil,
	degColLogger:                nil,
	spsLogger:                   nil,
	clsfnTaskDistOverheadLogger: nil,
	schedWindowLogger:           nil,
	runtimeMetricsLogger:        nil,
	utilizationMetricsLogger:    nil,
	allocationMetricsLogger:     nil,
	allocationStatsLogger:       nil,
	workloadSectionLogger:       nil,
	rankedTasksLogger:           nil,
}

// Logger options to help initialize loggers
type loggerOption func(l loggerObserver) error

func withLogDirectory(startTime time.Time, prefix string) loggerOption {
	return func(l loggerObserver) error {
		l.(*loggerObserverImpl).setLogDirectory(logUtils.GetLogDir(startTime, prefix))
		return nil
	}
}

// This loggerOption initializes the specifics for each loggerObserver
func withLoggerSpecifics(prefix string) loggerOption {
	return func(l loggerObserver) error {
		l.(*loggerObserverImpl).logObserverSpecifics = map[string]*specifics{
			conLogger:                   &specifics{},
			schedTraceLogger:            &specifics{},
			pcpLogger:                   &specifics{},
			degColLogger:                &specifics{},
			spsLogger:                   &specifics{},
			clsfnTaskDistOverheadLogger: &specifics{},
			schedWindowLogger:           &specifics{},
			runtimeMetricsLogger:        &specifics{},
			utilizationMetricsLogger:    &specifics{},
			allocationMetricsLogger:     &specifics{},
			allocationStatsLogger:       &specifics{},
			workloadSectionLogger:       &specifics{},
			rankedTasksLogger:           &specifics{},
		}
		l.(*loggerObserverImpl).setLogFilePrefix(prefix)
		l.(*loggerObserverImpl).setLogFile()
		return nil
	}
}

// Build and assign all loggers
func attachAllLoggers(lg *LoggerDriver, startTime time.Time, prefix string) {
	loi := &loggerObserverImpl{}
	loi.init(withLogDirectory(startTime, strings.Split(prefix, startTime.Format("20060102150405"))[0]),
		withLoggerSpecifics(prefix))
	Loggers[conLogger] = &ConsoleLogger{
		loggerObserverImpl: *loi,
	}
	Loggers[schedTraceLogger] = &SchedTraceLogger{
		loggerObserverImpl: *loi,
	}
	Loggers[pcpLogger] = &PCPLogger{
		loggerObserverImpl: *loi,
	}
	Loggers[degColLogger] = &DegColLogger{
		loggerObserverImpl: *loi,
	}
	Loggers[spsLogger] = &SchedPolicySwitchLogger{
		loggerObserverImpl: *loi,
	}
	Loggers[clsfnTaskDistOverheadLogger] = &ClsfnTaskDistOverheadLogger{
		loggerObserverImpl: *loi,
	}
	Loggers[schedWindowLogger] = &SchedWindowLogger{
		loggerObserverImpl: *loi,
	}
	Loggers[runtimeMetricsLogger] = &RuntimeMetricsLogger{
		loggerObserverImpl: *loi,
	}
	Loggers[utilizationMetricsLogger] = &UtilizationMetricsLogger{
		loggerObserverImpl: *loi,
	}
	Loggers[allocationMetricsLogger] = &AllocationMetricsLogger{
		loggerObserverImpl: *loi,
	}
	Loggers[allocationStatsLogger] = &AllocationStatsLogger{
		loggerObserverImpl: *loi,
	}
	Loggers[workloadSectionLogger] = &WorkloadSectionLogger{
		loggerObserverImpl: *loi,
	}
	Loggers[rankedTasksLogger] = &RankedTasksLogger{
		loggerObserverImpl: *loi,
	}

	for _, lmt := range GetLogMessageTypes() {
		switch lmt {
		case SCHED_TRACE.String():
			lg.attach(SCHED_TRACE, Loggers[schedTraceLogger])
		case GENERAL.String():
			lg.attach(GENERAL, Loggers[conLogger])
		case WARNING.String():
			lg.attach(WARNING, Loggers[conLogger])
		case ERROR.String():
			lg.attach(ERROR, Loggers[conLogger])
		case SUCCESS.String():
			lg.attach(SUCCESS, Loggers[conLogger])
		case PCP.String():
			lg.attach(PCP, Loggers[pcpLogger])
		case DEG_COL.String():
			lg.attach(DEG_COL, Loggers[degColLogger])
		case SPS.String():
			lg.attach(SPS, Loggers[spsLogger])
		case CLSFN_TASKDIST_OVERHEAD.String():
			lg.attach(CLSFN_TASKDIST_OVERHEAD, Loggers[clsfnTaskDistOverheadLogger])
		case SCHED_WINDOW.String():
			lg.attach(SCHED_WINDOW, Loggers[schedWindowLogger])
		case RUNTIME_METRICS.String():
			lg.attach(RUNTIME_METRICS, Loggers[runtimeMetricsLogger])
		case UTILIZATION_METRICS.String():
			lg.attach(UTILIZATION_METRICS, Loggers[utilizationMetricsLogger])
		case ALLOCATION_METRICS.String():
			lg.attach(ALLOCATION_METRICS, Loggers[allocationMetricsLogger])
		case ALLOCATION_STATS.String():
			lg.attach(ALLOCATION_STATS, Loggers[allocationStatsLogger])
		case WORKLOAD_SECTION.String():
			lg.attach(WORKLOAD_SECTION, Loggers[workloadSectionLogger])
		case TASK_RANKS.String():
			lg.attach(TASK_RANKS, Loggers[rankedTasksLogger])

		}
	}
}
