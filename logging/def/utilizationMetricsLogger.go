package logging

type UtilizationMetricsLogger struct {
	loggerObserverImpl
}

func (uml UtilizationMetricsLogger) Log(message string) {
	if message != "" {
		uml.logObserverSpecifics[utilizationMetricsLogger].logFile.Println(message)
	}
}
