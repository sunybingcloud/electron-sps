package logging

import (
	"fmt"
	"log"
	"os"
)

// Logging platform
type loggerObserver interface {
	Log(message string)
	setLogFile()
	setLogFilePrefix(prefix string)
	setLogDirectory(dirName string)
	init(opts ...loggerOption)
}

type specifics struct {
	logFilePrefix string
	logFile       *log.Logger
}

type loggerObserverImpl struct {
	logFile              *log.Logger
	logObserverSpecifics map[string]*specifics
	logDirectory         string
}

func (loi *loggerObserverImpl) init(opts ...loggerOption) {
	for _, opt := range opts {
		// applying logger options
		if err := opt(loi); err != nil {
			log.Fatal(err)
		}
	}
}

func (loi loggerObserverImpl) Log(message string) {}

// Requires logFilePrefix to have already been set
func (loi *loggerObserverImpl) setLogFile() {
	for prefix, ls := range loi.logObserverSpecifics {
		if logFile, err := os.Create(ls.logFilePrefix); err != nil {
			log.Fatal("Unable to create logFile: ", err)
		} else {
			fmt.Printf("Creating logFile with pathname: %s, and prefix: %s\n", ls.logFilePrefix, prefix)
			ls.logFile = log.New(logFile, "", log.LstdFlags)
		}
	}
}

func (loi *loggerObserverImpl) setLogFilePrefix(prefix string) {
	// Setting logFilePrefix for pcp logger
	pcpLogFilePrefix := prefix + ".pcplog"
	if loi.logDirectory != "" {
		pcpLogFilePrefix = loi.logDirectory + "/" + pcpLogFilePrefix
	}
	loi.logObserverSpecifics[pcpLogger].logFilePrefix = pcpLogFilePrefix

	// Setting logFilePrefix for console logger
	consoleLogFilePrefix := prefix + "_console.log"
	if loi.logDirectory != "" {
		consoleLogFilePrefix = loi.logDirectory + "/" + consoleLogFilePrefix
	}
	loi.logObserverSpecifics[conLogger].logFilePrefix = consoleLogFilePrefix

	// Setting logFilePrefix for schedTrace logger
	schedTraceLogFilePrefix := prefix + "_schedTrace.log"
	if loi.logDirectory != "" {
		schedTraceLogFilePrefix = loi.logDirectory + "/" + schedTraceLogFilePrefix
	}
	loi.logObserverSpecifics[schedTraceLogger].logFilePrefix = schedTraceLogFilePrefix

	// Setting logFilePrefix for degCol logger
	degColLogFilePrefix := prefix + "_degCol.log"
	if loi.logDirectory != "" {
		degColLogFilePrefix = loi.logDirectory + "/" + degColLogFilePrefix
	}
	loi.logObserverSpecifics[degColLogger].logFilePrefix = degColLogFilePrefix

	// Setting logFilePrefix for schedulingPolicySwitch logger
	schedPolicySwitchLogFilePrefix := prefix + "_schedPolicySwitch.log"
	if loi.logDirectory != "" {
		schedPolicySwitchLogFilePrefix = loi.logDirectory + "/" + schedPolicySwitchLogFilePrefix
	}
	loi.logObserverSpecifics[spsLogger].logFilePrefix = schedPolicySwitchLogFilePrefix

	// Setting logFilePrefix for clsfnTaskDist logger.
	// Execution time of every call to def.GetTaskDistribution(...) would be recorded and logged in this file.
	// The overhead would be logged in microseconds.
	clsfnTaskDistOverheadLogFilePrefix := prefix + "_classificationOverhead.log"
	if loi.logDirectory != "" {
		clsfnTaskDistOverheadLogFilePrefix = loi.logDirectory + "/" + clsfnTaskDistOverheadLogFilePrefix
	}
	loi.logObserverSpecifics[clsfnTaskDistOverheadLogger].logFilePrefix = clsfnTaskDistOverheadLogFilePrefix

	// Setting logFilePrefix for schedWindow logger.
	// Going to log the time stamp when the scheduling window was determined
	// 	and the size of the scheduling window.
	schedWindowLogFilePrefix := prefix + "_schedWindow.log"
	if loi.logDirectory != "" {
		schedWindowLogFilePrefix = loi.logDirectory + "/" + schedWindowLogFilePrefix
	}
	loi.logObserverSpecifics[schedWindowLogger].logFilePrefix = schedWindowLogFilePrefix

	// Setting logFilePrefix for runtime metrics logger.
	// Going to log runtime metrics collected and recorded by runtime metrics listener.
	runtimeMetricsLogFilePrefix := prefix + "_runtimeMetrics.log"
	if loi.logDirectory != "" {
		runtimeMetricsLogFilePrefix = loi.logDirectory + "/" + runtimeMetricsLogFilePrefix
	}
	loi.logObserverSpecifics[runtimeMetricsLogger].logFilePrefix = runtimeMetricsLogFilePrefix

	// Setting logFilePrefix for utilization metrics logger.
	// Going to log utilization metrics collected and recorded by utilization metrics listener.
	utilizationMetricsLogFilePrefix := prefix + "_utilMetrics.log"
	if loi.logDirectory != "" {
		utilizationMetricsLogFilePrefix = loi.logDirectory + "/" + utilizationMetricsLogFilePrefix
	}
	loi.logObserverSpecifics[utilizationMetricsLogger].logFilePrefix = utilizationMetricsLogFilePrefix

	// Setting logFilePrefix for allocation metrics logger.
	// Going to log allocation metrics collected and recorded by allocation metrics listener.
	allocationMetricsLogFilePrefix := prefix + "_allocationMetrics.log"
	if loi.logDirectory != "" {
		allocationMetricsLogFilePrefix = loi.logDirectory + "/" + allocationMetricsLogFilePrefix
	}
	loi.logObserverSpecifics[allocationMetricsLogger].logFilePrefix = allocationMetricsLogFilePrefix

	// Setting logFilePrefix for allocation stats logger.
	// Going to log allocation stats recorded by allocation metrics listener.
	allocationStatsLogFilePrefix := prefix + "_allocationStats.log"
	if loi.logDirectory != "" {
		allocationStatsLogFilePrefix = loi.logDirectory + "/" + allocationStatsLogFilePrefix
	}
	loi.logObserverSpecifics[allocationStatsLogger].logFilePrefix = allocationStatsLogFilePrefix

	// Setting logFilePrefix for workload section tag logger.
	// Going to log the start of a new workload section.
	workloadSectionLogFilePrefix := prefix + "_workloadSections.log"
	if loi.logDirectory != "" {
		workloadSectionLogFilePrefix = loi.logDirectory + "/" + workloadSectionLogFilePrefix
	}
	loi.logObserverSpecifics[workloadSectionLogger].logFilePrefix = workloadSectionLogFilePrefix

	// Setting logFilePrefix for ranked tasks logger.
	rankedTasksLoggerPrefix := prefix + "_rankedTasks.log"
	if loi.logDirectory != "" {
		rankedTasksLoggerPrefix = loi.logDirectory + "/" + rankedTasksLoggerPrefix
	}
	loi.logObserverSpecifics[rankedTasksLogger].logFilePrefix = rankedTasksLoggerPrefix
}

func (loi *loggerObserverImpl) setLogDirectory(dirName string) {
	loi.logDirectory = dirName
}
