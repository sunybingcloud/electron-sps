package logging

type SchedWindowLogger struct {
	loggerObserverImpl
}

func (swl SchedWindowLogger) Log(message string) {
	// Logging size of scheduling window, and the name of the scheduling policy that is scheduling tasks in the window, to mentioned file
	swl.logObserverSpecifics[schedWindowLogger].logFile.Println(message)
}
