package logging

import "github.com/fatih/color"

// Defining enums of log message types
var logMessageNames []string

// Possible log message types
var (
	ERROR                   = messageNametoMessageType("ERROR")
	WARNING                 = messageNametoMessageType("WARNING")
	GENERAL                 = messageNametoMessageType("GENERAL")
	SUCCESS                 = messageNametoMessageType("SUCCESS")
	SCHED_TRACE             = messageNametoMessageType("SCHED_TRACE")
	PCP                     = messageNametoMessageType("PCP")
	DEG_COL                 = messageNametoMessageType("DEG_COL")
	SPS                     = messageNametoMessageType("SPS")
	CLSFN_TASKDIST_OVERHEAD = messageNametoMessageType("CLSFN_TASKDIST_OVERHEAD")
	SCHED_WINDOW            = messageNametoMessageType("SCHED_WINDOW")
	RUNTIME_METRICS         = messageNametoMessageType("RUNTIME_METRICS")
	UTILIZATION_METRICS     = messageNametoMessageType("UTILIZATION_METRICS")
	ALLOCATION_METRICS      = messageNametoMessageType("ALLOCATION_METRICS")
	ALLOCATION_STATS        = messageNametoMessageType("ALLOCATION_STATS")
	WORKLOAD_SECTION        = messageNametoMessageType("WORKLOAD_SECTION")
	TASK_RANKS              = messageNametoMessageType("TASK_RANKS")
)

// Text colors for the different types of log messages.
var LogMessageColors map[LogMessageType]*color.Color = map[LogMessageType]*color.Color{
	ERROR:   color.New(color.FgRed, color.Bold),
	WARNING: color.New(color.FgYellow, color.Bold),
	GENERAL: color.New(color.FgWhite, color.Bold),
	SUCCESS: color.New(color.FgGreen, color.Bold),
}

type LogMessageType int

func (lmt LogMessageType) String() string {
	return logMessageNames[lmt]
}

func GetLogMessageTypes() []string {
	return logMessageNames
}

func messageNametoMessageType(messageName string) LogMessageType {
	// Appending messageName to LogMessageNames
	logMessageNames = append(logMessageNames, messageName)
	// Mapping messageName to int
	return LogMessageType(len(logMessageNames) - 1)
}
