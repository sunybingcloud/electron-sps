package logging

type RankedTasksLogger struct {
	loggerObserverImpl
}

func (trl RankedTasksLogger) Log(message string) {
	if message != "" {
		trl.logObserverSpecifics[rankedTasksLogger].logFile.Println(message)
	}
}
