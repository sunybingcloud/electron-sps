package logging

type AllocationStatsLogger struct {
	loggerObserverImpl
}

func (asl AllocationStatsLogger) Log(message string) {
	if message != "" {
		asl.logObserverSpecifics[allocationStatsLogger].logFile.Println(message)
	}
}
