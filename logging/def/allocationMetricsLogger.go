package logging

type AllocationMetricsLogger struct {
	loggerObserverImpl
}

func (aml AllocationMetricsLogger) Log(message string) {
	if message != "" {
		aml.logObserverSpecifics[allocationMetricsLogger].logFile.Println(message)
	}
}
