package pcp

import (
	"bitbucket.org/sunybingcloud/electron/events"
	elecLogDef "bitbucket.org/sunybingcloud/electron/logging/def"
	"bitbucket.org/sunybingcloud/electron/metrics"
	"bitbucket.org/sunybingcloud/electron/rapl"
	"bitbucket.org/sunybingcloud/electron/utilities"
	"bufio"
	"fmt"
	"log"
	"math"
	"os/exec"
	"sync"
	"syscall"
	"time"
)

// Opportunistic power-capping policy attempts to lower control peak power surges by tightening the
// power-cap when the cpu utilization increases and loosening the power-cap when the cpu utilization
// reduces. This ensures that the power-cap will never be too strict as the lowest cap is 50%.
func StartPCPLogAndOpportunisticCap(
	quit *utilities.SignalChannel,
	logMType chan elecLogDef.LogMessageType,
	logMsg chan string,
	wg *sync.WaitGroup,
	listeners ...events.EventListener) {

	const pcpCommand string = "pmdumptext -m -l -f '' -t 1.0 -d , -c config"
	cmd := exec.Command("sh", "-c", pcpCommand)
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}

	pipe, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatal(err)
	}
	//cmd.Stdout = stdout

	scanner := bufio.NewScanner(pipe)

	stopCapping := make(chan struct{})
	ticker := time.NewTicker(5 * time.Second)
	// hostInfo := metrics.GetHostInfoListenerInstance().(*metrics.HostInfoListener)
	umListener := metrics.GetUtilizationMetricsListenerInstance().(*metrics.UtilizationMetricsListener)

	wg.Add(1)
	go func(ticker *time.Ticker, done chan struct{}, wg *sync.WaitGroup) {
		defer wg.Done()
		// Get names of columns.
		scanner.Scan()

		// Write to logfile.
		logMType <- elecLogDef.PCP
		logMsg <- scanner.Text()

		// Notifying the listeners of the PCP event and passing the headers.
		for _, l := range listeners {
			err := l.Update(events.PCP, &events.PCPEventData{Val: scanner.Text()})
			if err != nil {
				logMType <- elecLogDef.ERROR
				logMsg <- err.Error()
			}
		}

		// Set of active hosts for which PCP data is retrieved.
		activeHosts := make(map[string]struct{})
		uncappedHosts := make(map[string]struct{})

		// Throw away first set of results
		scanner.Scan()

		for scanner.Scan() {
			select {
			case <-done:
				ticker.Stop()
				// uncapping all hosts.
				for host := range activeHosts {
					err := rapl.Cap(host, "rapl", 100)
					if err != nil {
						logMType <- elecLogDef.ERROR
						logMsg <- fmt.Sprintf("failed to uncap host[%s]", host)
					} else {
						logMType <- elecLogDef.ERROR
						logMsg <- fmt.Sprintf("uncapped host[%s]", host)
					}
				}
				return
			case <-ticker.C:
				for host, cpuUtilEstimate := range umListener.GetCpuUtilEstimates() {
					activeHosts[host] = struct{}{}
					// computing cap value and rounding to 2 decimal places.
					if cpuUtilEstimate < 0 {
						cpuUtilEstimate = 0
					}
					capTo := math.Min(100, math.Ceil(100*((100+cpuUtilEstimate)/2))/100)
					if capTo == 100.0 {
						if _, ok := uncappedHosts[host]; ok {
							continue // Not uncapping again.
						} else {
							uncappedHosts[host] = struct{}{}
						}
					} else if _, ok := uncappedHosts[host]; ok {
						delete(uncappedHosts, host) // After applying cap, host will no longer be uncapped.
					}
					if raplErr := rapl.Cap(host, "rapl", capTo); raplErr != nil {
						logMType <- elecLogDef.WARNING
						logMsg <- fmt.Sprintf("Could not cap host[%s]: %s", host, raplErr)
					} else {
						logMType <- elecLogDef.GENERAL
						logMsg <- fmt.Sprintf("CPU_UTIL_EST[%s] = %f. Capped to %f %%",
							host, cpuUtilEstimate, capTo)
					}
				}
			default:
				// Write to logfile.
				logMType <- elecLogDef.PCP
				logMsg <- scanner.Text()
				// Notifying the listeners of the PCP event and passing the headers.
				for _, l := range listeners {
					err := l.Update(events.PCP, &events.PCPEventData{Val: scanner.Text()})
					if err != nil {
						logMType <- elecLogDef.ERROR
						logMsg <- err.Error()
					}
				}
			}
		}
	}(ticker, stopCapping, wg)

	logMType <- elecLogDef.GENERAL
	logMsg <- "PCP logging started"

	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}

	pgid, err := syscall.Getpgid(cmd.Process.Pid)

	quit.WaitTillClosed()
	logMType <- elecLogDef.GENERAL
	logMsg <- "Stopping PCP logging in 5 seconds"
	time.Sleep(5 * time.Second)
	close(stopCapping)

	// http://stackoverflow.com/questions/22470193/why-wont-go-kill-a-child-process-correctly
	// kill process and all children processes
	syscall.Kill(-pgid, 15)
	wg.Done()
	return
}
