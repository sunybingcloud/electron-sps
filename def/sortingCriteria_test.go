package def

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSortingCriteria(t *testing.T) {
	task := &SimpleTask{CPU: 1.0, RAM: 1024.0, Watts: 50.0}
	assert.Equal(t, 1.0, SortByCPU(task))
	assert.Equal(t, 1024.0, SortByRAM(task))
	assert.Equal(t, 50.0, SortByWatts(task))
}
