package def

import (
	"bitbucket.org/sunybingcloud/electron/utilities/validation"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewTaskValidator(t *testing.T) {
	inst := 10
	var validTask Task
	validTask = &SimpleTask{
		Name:      "minife",
		CPU:       3.0,
		RAM:       4096,
		Watts:     50,
		Image:     "rdelvalle/minife:electron1",
		CMD:       "cd src && mpirun -np 1 miniFE.x -nx 100 -ny 100 -nz 100",
		Instances: &inst,
	}

	getValidator := func(t Task) validation.ValidatorFunc {
		return NewTaskValidator(t, t.GetValidators())
	}

	validator := getValidator(validTask)
	assert.NoError(t, validation.Validate("invalid task definition", validator))

	// task with invalid name.
	var taskInvalidName Task
	taskInvalidName = &SimpleTask{
		Name:      "",
		CPU:       3.0,
		RAM:       4096,
		Watts:     50,
		Image:     "rdelvalle/minife:electron1",
		CMD:       "cd src && mpirun -np 1 miniFE.x -nx 100 -ny 100 -nz 100",
		Instances: &inst,
	}
	validator = getValidator(taskInvalidName)
	assert.Error(t, validation.Validate("invalid task name", validator))

	// task with invalid image.
	var taskInvalidImage Task
	taskInvalidImage = &SimpleTask{
		Name:      "minife",
		CPU:       3.0,
		RAM:       4096,
		Watts:     50,
		CMD:       "cd src && mpirun -np 1 miniFE.x -nx 100 -ny 100 -nz 100",
		Instances: &inst,
	}

	validator = getValidator(taskInvalidImage)
	assert.Error(t, validation.Validate("invalid task image", validator))

	// task with invalid resources.
	var taskInvalidResources Task
	taskInvalidResources = &SimpleTask{
		Name:      "minife",
		CPU:       0.0,
		RAM:       0,
		Watts:     50,
		CMD:       "cd src && mpirun -np 1 miniFE.x -nx 100 -ny 100 -nz 100",
		Instances: &inst,
	}

	validator = getValidator(taskInvalidResources)
	assert.Error(t, validation.Validate("invalid task resources", validator))
}
