package def

import (
	"math"
	"time"
)

// DeadlineSensitiveTask represents a priority task that has a deadline
// associated with it.
type DeadlineSensitiveTask struct {
	SimpleTask

	// SubmissionToDeadlineSeconds is the max number of seconds from submission time
	// that the task can take to complete execution.
	SubmissionToDeadlineSeconds *int `json:"secondsToDeadline"`
	// Priority of the task.
	priority *int
}

func (t DeadlineSensitiveTask) GetPriority() int {
	if t.priority != nil {
		return *t.priority
	}
	// Default computation of priority.
	// -(Time to deadline in seconds). This would mean that a task closer to its deadline
	// would have a higher priority than a task that is farther from its deadline.
	// However, it is important to note that just using the remaining time (in seconds)
	// to determine the priority can result in unfairness by detrimentally impact wait-times
	// of tasks with relatively longer deadlines as the tasks with shorter deadlines would
	// end up getting a higher priority. This can in turn result in tasks with longer
	// deadlines not meeting their deadlines when the task queue consists of a large number
	// of tasks with short deadlines.
	now := time.Now()
	// Rounding up for safety.
	elapsedSeconds := int(math.Ceil(now.Sub(t.SubmittedTime).Seconds()))
	secondsToDeadline := *t.SubmissionToDeadlineSeconds - elapsedSeconds
	return -secondsToDeadline
}

// SetPriority sets the priority of the task to the given value.
// If this is called, then it is assumed that a method, other than the default method
// in GetPriority(), is used to compute the priority of the task.
// The new method is therefore given precedence.
func (t *DeadlineSensitiveTask) SetPriority(p int) {
	// Overwriting the existing priority of the task.
	t.priority = &p
}

func (t *DeadlineSensitiveTask) GetValidators() []func(Task) error {
	return []func(Task) error{
		withNameValidator(),
		withImageValidator(),
		withResourceValidator(),
		withDeadlineValidator(),
	}
}
