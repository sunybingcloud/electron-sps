package def

import (
	"bitbucket.org/sunybingcloud/electron/constants"
	"time"
)

type SimpleTask struct {
	Name         string             `json:"name"`
	CPU          float64            `json:"cpu"`
	RAM          float64            `json:"ram"`
	Watts        float64            `json:"watts"`
	Image        string             `json:"image"`
	CMD          string             `json:"cmd"`
	Instances    *int               `json:"inst"`
	Host         string             `json:"host"`
	TaskID       string             `json:"taskID"`
	ClassToWatts map[string]float64 `json:"class_to_watts"`
	// Time when the task was submitted to the framework for scheduling.
	SubmittedTime time.Time
	// Tolerance of the task to a detrimental impact on execution time.
	// The share of processing power that a task gets decides the duration of its execution.
	// Thus, Tolerance is represented as the percentage reduction in the allocated processing
	// power, when compared to the requested processing power (Task#Watts value), that is
	// acceptable to allow for fitting of the respective task into a resource offer from Mesos.
	Tolerance float64 `json:"tolerance"`
	// Section in the workload of which the task was a part.
	Section string
}

func (t SimpleTask) GetName() string {
	return t.Name
}

func (t SimpleTask) GetCpu() float64 {
	return t.CPU
}

func (t SimpleTask) GetRam() float64 {
	return t.RAM
}

func (t SimpleTask) GetWatts() float64 {
	return t.Watts
}

func (t SimpleTask) GetImage() string {
	return t.Image
}

func (t SimpleTask) GetCmd() string {
	return t.CMD
}

func (t SimpleTask) GetInstances() int {
	return *t.Instances
}

func (t *SimpleTask) DecrementInstances() {
	*t.Instances--
}

func (t SimpleTask) GetHost() string {
	return t.Host
}

// Update the host on which the task needs to be scheduled.
func (t *SimpleTask) UpdateHost(newHost string) bool {
	// Validation
	isCorrectHost := false
	for existingHost, _ := range constants.Hosts {
		if newHost == existingHost {
			isCorrectHost = true
		}
	}
	if !isCorrectHost {
		return false
	} else {
		t.Host = newHost
		return true
	}
}

func (t SimpleTask) GetTaskID() string {
	return t.TaskID
}

// Set the taskID of the task.
func (t *SimpleTask) SetTaskID(taskID string) bool {
	// Validation
	if taskID == "" {
		return false
	} else {
		t.TaskID = taskID
		return true
	}
}

func (t SimpleTask) GetClassToWatts() map[string]float64 {
	return t.ClassToWatts
}

func (t SimpleTask) GetSubmittedTime() time.Time {
	return t.SubmittedTime
}

func (t *SimpleTask) SetSubmittedTime(subTime time.Time) {
	t.SubmittedTime = subTime
}

func (t SimpleTask) GetTolerance() float64 {
	return t.Tolerance
}

func (t SimpleTask) GetWorkloadSection() string {
	return t.Section
}

func (t *SimpleTask) SetWorkloadSection(section string) {
	t.Section = section
}

func (t SimpleTask) GetValidators() []func(Task) error {
	return []func(Task) error{
		withNameValidator(),
		withImageValidator(),
		withResourceValidator(),
	}
}
