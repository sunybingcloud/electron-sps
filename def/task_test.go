package def

import (
	"bitbucket.org/sunybingcloud/electron/constants"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestTasksFromJSON(t *testing.T) {
	tasks, err := TasksFromJSON("../workload_sample.json", SIMPLE_TASK)
	assert.Equal(t, 1, len(tasks))
	assert.NoError(t, err)

	instances := 20
	var refTask Task
	refTask = &SimpleTask{
		Name:      "minife",
		CPU:       1.5,
		RAM:       4096,
		Watts:     49.95,
		Image:     "rdelvalle/minife:electron1",
		CMD:       "cd src && mpirun -np 3 miniFE.x -nx 100 -ny 100 -nz 100",
		Instances: &instances,
		ClassToWatts: map[string]float64{
			"A": 54.50,
			"B": 43.13,
			"C": 49.95,
		},
		Tolerance: 0.02,
	}

	assert.True(t, reflect.DeepEqual(refTask, tasks[0]))

	tasks, err = TasksFromJSON("../deadline_sensitive_workload_sample.json", DEADLINE_SENSITIVE_TASK)
	assert.Equal(t, 1, len(tasks))
	assert.NoError(t, err)

	timeToDeadline := 100
	refTask = &DeadlineSensitiveTask{
		SimpleTask: SimpleTask{
			Name:      "minife",
			CPU:       1.5,
			RAM:       4096,
			Watts:     49.95,
			Image:     "rdelvalle/minife:electron1",
			CMD:       "cd src && mpirun -np 3 miniFE.x -nx 100 -ny 100 -nz 100",
			Instances: &instances,
			ClassToWatts: map[string]float64{
				"A": 54.50,
				"B": 43.13,
				"C": 49.95,
			},
			Tolerance: 0.02,
		},
		SubmissionToDeadlineSeconds: &timeToDeadline,
	}

	assert.True(t, reflect.DeepEqual(refTask, tasks[0]))
}

func TestTask_UpdateHost(t *testing.T) {
	var task Task
	task = &SimpleTask{}
	constants.Hosts["host1"] = struct{}{}
	task.UpdateHost("host1")
	assert.Equal(t, task.GetHost(), "host1", "failed to update host of task")
}

func TestTask_SetTaskID(t *testing.T) {
	instances := 1
	var task Task
	task = &SimpleTask{
		Name:      "test-task",
		Instances: &instances,
		Section:   "test-section",
	}

	taskID := GenerateTaskID(task)
	task.SetTaskID(taskID)
	assert.Equal(t, taskID, task.GetTaskID(), "failed to set task ID")
}

func TestWattsToConsider(t *testing.T) {
	var task Task
	task = &SimpleTask{
		Name:  "minife",
		Watts: 49.95,
		ClassToWatts: map[string]float64{
			"A": 54.50,
			"B": 43.13,
			"C": 49.95,
		},
	}

	powerClass := "A"
	classAttribute := "class"
	offerClassA := &mesos.Offer{
		Attributes: []*mesos.Attribute{
			{
				Name: &classAttribute,
				Text: &mesos.Value_Text{Value: &powerClass},
			},
		},
	}
	// without class to watts mapping.
	wattsClassMapWattsDisabled, err := WattsToConsider(task, false, offerClassA)
	assert.NoError(t, err)
	assert.Equal(t, task.GetWatts(), wattsClassMapWattsDisabled)

	// with class to watts mapping.
	wattsClassMapWattsEnabled, err := WattsToConsider(task, true, offerClassA)
	assert.NoError(t, err)
	assert.Equal(t, task.GetClassToWatts()["A"], wattsClassMapWattsEnabled)

	// offer.powerClass not present in task.ClassToWatts.
	powerClass = "D"
	offerClassD := &mesos.Offer{
		Attributes: []*mesos.Attribute{
			{
				Name: &classAttribute,
				Text: &mesos.Value_Text{Value: &powerClass},
			},
		},
	}

	_, err = WattsToConsider(task, true, offerClassD)
	assert.Error(t, err)
}

func TestCompare(t *testing.T) {
	var task1 Task
	var task2 Task

	task1 = &SimpleTask{
		Name:   "test1-task",
		TaskID: "test-taskid",
	}

	task2 = &SimpleTask{
		TaskID: "test-taskid",
	}

	assert.True(t, Compare(task1, task1))
	assert.True(t, Compare(task1, task2))
}
