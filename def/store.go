package def

const (
	SIMPLE_TASK             = "simple"
	DEADLINE_SENSITIVE_TASK = "deadline-sensitive"
)

// types stores the mapping of task type names to the actual task types available.
var types = map[string]Task{
	SIMPLE_TASK:             &SimpleTask{},
	DEADLINE_SENSITIVE_TASK: &DeadlineSensitiveTask{},
}

// taskSliceTypes stores the mapping of task type names to the slice type of the task.
// This is required when unmarshaling the workload json into a slice of tasks.
var unmarshalers = map[string]tasksUnmarshaler{
	SIMPLE_TASK:             &simpleTaskSliceUnmarshaler{},
	DEADLINE_SENSITIVE_TASK: &deadlineSensitiveTaskSliceUnmarshaler{},
}
