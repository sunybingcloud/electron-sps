package def

import (
	"github.com/stretchr/testify/assert"
	"regexp"
	"testing"
)

func TestGenerateTaskID(t *testing.T) {
	instances := 1
	var task Task
	task = &SimpleTask{
		Name:      "test-task",
		Instances: &instances,
		Section:   "test-task-section",
	}

	const correctTaskIDRegex = "electron-test-task-1-"
	match, err := regexp.Match(correctTaskIDRegex, []byte(GenerateTaskID(task)))
	assert.NoError(t, err)
	assert.True(t, match)
}
