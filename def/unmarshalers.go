package def

import (
	"encoding/json"
	"github.com/pkg/errors"
)

// tasksUnmarshaler defines an interface to be implemented by unmarshalers of slices
// of different task types.
type tasksUnmarshaler interface {
	json.Unmarshaler
	// Return the unmarshaled tasks as a slice of Task.
	asTasks() []Task
}

// Simple Tasks.
// simpleTaskSliceUnmarshaler represents a slice of SimpleTask, that implements the json.Unmarshaler interface.
type simpleTaskSliceUnmarshaler []*SimpleTask

func (sTSliceU *simpleTaskSliceUnmarshaler) UnmarshalJSON(data []byte) error {
	// Validating whether the given tasks are simple tasks.
	// If they contain fields not in SimpleTask, validation fails.

	// Unmarshaling a dummy SimpleTask to get a set of expected fields.
	var expectedFields map[string]interface{}
	dummySimpleTaskBytes := `
	{
		"name": "dummyTask",
		"cpu": 1.0,
		"ram": 1024.0,
		"watts": 50.0,
		"image": "dummy-image",
		"cmd": "dummy-cmd",
		"inst": 1,
		"host": "dummy-host",
		"class_to_watts": {"A": 100.0, "B": 50.0},
		"tolerance": 100.0
	}
	`
	err := json.Unmarshal([]byte(dummySimpleTaskBytes), &expectedFields)
	if err != nil {
		return errors.Wrap(err, "failed to validate workload due to internal error")
	}

	// Unmarshaling the given tasks to get the fields provided for each task.
	var providedTasks []map[string]interface{}
	err = json.Unmarshal(data, &providedTasks)
	if err != nil {
		return errors.Wrap(err, "failed to validate workload")
	}

	// Checking whether all the fields present in providedTasks are in expectedFields.
	if len(providedTasks) == 0 {
		return errors.New("no tasks present in workload")
	}
	for _, providedFieldsForTask := range providedTasks {
		for key := range providedFieldsForTask {
			if _, ok := expectedFields[key]; !ok {
				return errors.New("task is not a simple task")
			}
		}
	}

	// If here, then validation passed.
	// Now unmarshaling workload into []SimpleTask.
	var tempTasks []*SimpleTask
	if err := json.Unmarshal(data, &tempTasks); err != nil {
		return errors.Wrap(err, "failed to unmarshal workload")
	}

	*sTSliceU = tempTasks
	return nil
}

func (sTSliceU simpleTaskSliceUnmarshaler) asTasks() []Task {
	var tasks []Task
	for _, t := range sTSliceU {
		tasks = append(tasks, t)
	}
	return tasks
}

// Deadline Sensitive Tasks.
type deadlineSensitiveTaskSliceUnmarshaler []*DeadlineSensitiveTask

func (dsTSlice *deadlineSensitiveTaskSliceUnmarshaler) UnmarshalJSON(data []byte) error {
	// Validating whether the given tasks are deadline sensitive tasks.
	// If they contain fields not in DeadlineSensitiveTask, validation fails.

	// Unmarshaling a dummy DeadlineSensitiveTask to get a set of expected fields.
	var expectedFields map[string]interface{}
	dummyDeadlineSensitiveTaskBytes := `
	{
		"name": "dummyTask",
		"cpu": 1.0,
		"ram": 1024.0,
		"watts": 50.0,
		"image": "dummy-image",
		"cmd": "dummy-cmd",
		"inst": 1,
		"host": "dummy-host",
		"class_to_watts": {"A": 100.0, "B": 50.0},
		"tolerance": 100.0,
		"secondsToDeadline": 100
	}
	`
	err := json.Unmarshal([]byte(dummyDeadlineSensitiveTaskBytes), &expectedFields)
	if err != nil {
		return errors.Wrap(err, "failed to validate workload due to internal error")
	}

	// Unmarshaling the given tasks to get the fields provided for each task.
	var providedTasks []map[string]interface{}
	err = json.Unmarshal(data, &providedTasks)
	if err != nil {
		return errors.Wrap(err, "failed to validate workload")
	}

	// Checking whether all the fields present in providedTasks are in expectedFields.
	if len(providedTasks) == 0 {
		return errors.New("no tasks present in workload")
	}
	for _, providedFieldsForTask := range providedTasks {
		for key := range providedFieldsForTask {
			if _, ok := expectedFields[key]; !ok {
				return errors.New("task is not a deadline sensitive task")
			}
		}
	}

	// If here, then validation passed.
	// Now unmarshaling workload into []DeadlineSensitiveTask.
	var tempTasks []*DeadlineSensitiveTask
	if err := json.Unmarshal(data, &tempTasks); err != nil {
		return errors.Wrap(err, "failed to unmarshal workload")
	}

	*dsTSlice = tempTasks
	return nil
}

func (dsTSlice deadlineSensitiveTaskSliceUnmarshaler) asTasks() []Task {
	var tasks []Task
	for _, t := range dsTSlice {
		tasks = append(tasks, t)
	}
	return tasks
}
