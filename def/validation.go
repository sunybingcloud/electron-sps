package def

import (
	"bitbucket.org/sunybingcloud/electron/utilities/validation"
	"github.com/pkg/errors"
	"regexp"
)

func NewTaskValidator(t Task, funcs []func(Task) error) validation.ValidatorFunc {
	return func() error {
		for _, f := range funcs {
			if err := f(t); err != nil {
				return err
			}
		}

		return nil
	}
}

// withNameValidator returns a validator that checks whether the task has a valid name.
func withNameValidator() func(Task) error {
	return func(t Task) error {
		// Task name cannot be empty string.
		if t.GetName() == "" {
			return errors.New("task name cannot be empty string")
		}

		// Task name cannot contain tabs or spaces.
		matched, _ := regexp.MatchString("\\t+|\\s+", t.GetName())
		if matched {
			return errors.New("task name cannot contain tabs or spaces")
		}

		return nil
	}
}

// withResourceValidator returns a validator that checks whether the resource requirements are valid.
// This currently only checks for the traditional resources such as CPU and Memory.
func withResourceValidator() func(Task) error {
	return func(t Task) error {
		// CPU value cannot be 0.
		if t.GetCpu() == 0.0 {
			return errors.New("CPU resource for task cannot be 0")
		}

		// RAM value cannot be 0.
		if t.GetRam() == 0.0 {
			return errors.New("Memory resource for task cannot be 0")
		}

		return nil
	}
}

// withImageValidator returns a validator that checks whether a docker image has indeed been provided.
// This currently only checks whether the image is an empty string or not.
// To be able to check whether the image actually exists on docker hub, one would need to check the registry.
func withImageValidator() func(Task) error {
	return func(t Task) error {
		if t.GetImage() == "" {
			return errors.New("valid image needs to be provided for task")
		}
		return nil
	}
}

// withDeadlineValidator returns a validator that checks whether the time to deadline specified for a deadline sensitive
// task is greater than 0.
func withDeadlineValidator() func(Task) error {
	return func(t Task) error {
		switch t.(type) {
		case *DeadlineSensitiveTask:
			if *t.(*DeadlineSensitiveTask).SubmissionToDeadlineSeconds <= 0 {
				return errors.New("time to deadline must be a value > 0 seconds")
			}
			return nil
		default:
			return errors.New("cannot validate deadline of task: not a deadline sensitive task")
		}
	}
}
