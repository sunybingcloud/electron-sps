package def

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSimpleTaskSlice_UnmarshalJSON(t *testing.T) {
	var u simpleTaskSliceUnmarshaler

	t.Run("valid simple task", func(t *testing.T) {
		validDummySimpleTasksBytes := `
		[{
			"name": "dummyTask",
			"cpu": 1.0,
			"ram": 1024.0,
			"watts": 50.0,
			"image": "dummy-image",
			"cmd": "dummy-cmd",
			"inst": 1,
			"host": "dummy-host",
			"class_to_watts": {"A": 100.0, "B": 50.0},
			"tolerance": 100.0
		}]
		`
		assert.NoError(t, json.Unmarshal([]byte(validDummySimpleTasksBytes), &u))
	})

	t.Run("invalid simple task with unexpected attribute", func(t *testing.T) {
		invalidDummySimpleTasksBytes := `
		[{
			"name": "dummyTask",
			"cpu": 1.0,
			"ram": 1024.0,
			"watts": 50.0,
			"image": "dummy-image",
			"cmd": "dummy-cmd",
			"inst": 1,
			"host": "dummy-host",
			"class_to_watts": {"A": 100.0, "B": 50.0},
			"tolerance": 100.0,
			"testAttribute": "test-value"
		}]
		`
		assert.Error(t, json.Unmarshal([]byte(invalidDummySimpleTasksBytes), &u))
	})
}

func TestDeadlineSensitiveTaskSlice_UnmarshalJSON(t *testing.T) {
	var u deadlineSensitiveTaskSliceUnmarshaler

	t.Run("valid deadline sensitive task", func(t *testing.T) {
		validDummyDeadlineSensitiveTasksBytes := `
		[{
			"name": "dummyTask",
			"cpu": 1.0,
			"ram": 1024.0,
			"watts": 50.0,
			"image": "dummy-image",
			"cmd": "dummy-cmd",
			"inst": 1,
			"host": "dummy-host",
			"class_to_watts": {"A": 100.0, "B": 50.0},
			"tolerance": 100.0,
			"secondsToDeadline": 100
		}]
		`
		assert.NoError(t, json.Unmarshal([]byte(validDummyDeadlineSensitiveTasksBytes), &u))
	})

	t.Run("invalid deadline sensitive task with unexpected attribute", func(t *testing.T) {
		invalidDummyDeadlineSensitiveTasksBytes := `
		[{
			"name": "dummyTask",
			"cpu": 1.0,
			"ram": 1024.0,
			"watts": 50.0,
			"image": "dummy-image",
			"cmd": "dummy-cmd",
			"inst": 1,
			"host": "dummy-host",
			"class_to_watts": {"A": 100.0, "B": 50.0},
			"tolerance": 100.0,
			"secondsToDeadline": 100,
			"testAttribute": "test-value"
		}]
		`
		assert.Error(t, json.Unmarshal([]byte(invalidDummyDeadlineSensitiveTasksBytes), &u))
	})
}
