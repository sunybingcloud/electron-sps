package def

import (
	"github.com/satori/go.uuid"
	"strconv"
	"strings"
)

const IDDelimiter = "-"
const IDPrefix = "electron"

// Generate a unique task ID given the following,
// 1. Task configuration (name, instances, etc.)
func GenerateTaskID(task Task) string {
	var elements []string
	// Appending the framework name.
	elements = append(elements, IDPrefix)
	// Appending the task name.
	elements = append(elements, task.GetName())
	// Appending the instance.
	elements = append(elements, strconv.Itoa(task.GetInstances()))
	// Generating the UUID
	id := uuid.NewV4()
	elements = append(elements, id.String())
	// If the workload section information has been provided, then adding that to the taskID.
	if task.GetWorkloadSection() != "" {
		// section name for testing.
		elements = append(elements, strings.Join(strings.Split(task.GetWorkloadSection(), " "), ""))
	}
	// Joining elements with '-' as delimiter.
	return strings.Join(elements, IDDelimiter)
}
