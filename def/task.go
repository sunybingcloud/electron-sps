package def

import (
	"bitbucket.org/sunybingcloud/electron/utilities/offerUtils"
	"bitbucket.org/sunybingcloud/electron/utilities/validation"
	"encoding/json"
	"fmt"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	"github.com/pkg/errors"
	"os"
	"time"
)

// Interface defines an interfaces to be implemented by all tasks types.
type Task interface {
	// GetName of the task.
	GetName() string
	// GetCpu requirement of the task.
	GetCpu() float64
	// GetRam requirement of the task.
	GetRam() float64
	// GetWatts requirement of the task.
	GetWatts() float64
	// GetImage name for the task.
	GetImage() string
	// GetCmd to run the task.
	GetCmd() string
	// GetInstances of the task that are yet to be scheduled.
	GetInstances() int
	// DecrementInstances reduces the instance count by one.
	DecrementInstances()
	// GetHost on which the task is to be allocated resources.
	GetHost() string
	// UpdateHost updates the hostname on which the task needs to be scheduled.
	// Returns boolean indicating whether the given hostname is valid.
	UpdateHost(string) bool
	// GetTaskID returns the unique id for the task.
	GetTaskID() string
	// SetTaskID sets the task id of the task to the given unique id.
	// Returns whether the given task id is valid.
	SetTaskID(string) bool
	// GetClassToWatts returns mapping of the powerclass to the watts requirement.
	GetClassToWatts() map[string]float64
	// GetSubmittedTime returns the time when the task was submitted to the scheduler.
	GetSubmittedTime() time.Time
	// SetSubmittedTime sets the submitted time of the task to the given time.
	SetSubmittedTime(time.Time)
	// GetTolerance of the task towards a detrimental impact on performance due to a reduced share of CPU resources.
	GetTolerance() float64
	// GetWorkloadSection returns the workload section of which the task was a part.
	GetWorkloadSection() string
	// SetWorkloadSection sets the workload section of the task to the one given.
	SetWorkloadSection(string)
	// GetValidators returns the set of validators that need to be run to validate the task.
	GetValidators() []func(Task) error
}

type PriorityTask interface {
	Task
	// GetPriority returns the priority of the task.
	GetPriority() int
	// SetPriority sets the priority of the task.
	SetPriority(int)
}

// PQTaskItem represents an element in the priority queue.
// This wrapper stores the priority task and the current index of the task.
type PQTaskItem struct {
	Task PriorityTask
	// index of the task in the priority queue.
	Index int
}

func (pqi PQTaskItem) GetTask() PriorityTask {
	return pqi.Task
}

func fetchValue(val interface{}) Task {
	switch val.(type) {
	case *SimpleTask:
		return val.(*SimpleTask)
	}
	return nil
}

func UnmarshalTasks(taskType string, decoder *json.Decoder) ([]Task, error) {
	var tasks []Task

	if _, ok := types[taskType]; !ok {
		return nil, errors.New("invalid task type specified")
	}

	// Retrieving unmarshaler for the given task type.
	var unmarshaler = unmarshalers[taskType]
	err := decoder.Decode(unmarshaler)
	if err != nil {
		return nil, errors.Wrap(err, "Error unmarshalling")
	}

	// Adding unmarshaled tasks.
	tasks = append(tasks, unmarshaler.asTasks()...)

	// Validating tasks.
	for _, task := range tasks {
		err := validation.Validate("invalid task definition",
			NewTaskValidator(task, task.GetValidators()))
		if err != nil {
			return tasks, err
		}
	}
	return tasks, nil
}

func TasksFromJSON(uri string, taskType string) ([]Task, error) {
	file, err := os.Open(uri)
	if err != nil {
		return nil, errors.Wrap(err, "Error opening file")
	}

	return UnmarshalTasks(taskType, json.NewDecoder(file))
}

/*
 Determine the watts value to consider for each task.

 This value could either be task.Watts or task.ClassToWatts[<power class>]
 If task.ClassToWatts is not present, then return task.Watts (this would be for workloads which don't have classMapWatts)
*/
func WattsToConsider(task Task, classMapWatts bool, offer *mesos.Offer) (float64, error) {
	if classMapWatts {
		// checking if GetClassToWatts was present in the workload.
		if task.GetClassToWatts() != nil {
			// If watts requirement not specified for offer.PowerClass, then return error.
			if _, ok := task.GetClassToWatts()[offerUtils.PowerClass(offer)]; ok {
				return task.GetClassToWatts()[offerUtils.PowerClass(offer)], nil
			}
			return task.GetWatts(), errors.New(fmt.Sprintf("watts requirement not provided for powerclass: "+
				"task[%s], powerclass[%s]", task.GetName(), offerUtils.PowerClass(offer)))
		} else {
			// Checking whether task.GetWatts is 0.0. If yes, then throwing an error.
			if task.GetWatts() == 0.0 {
				return task.GetWatts(), errors.New("Configuration error in task. GetWatts attribute is 0 for " + task.GetName())
			}
			return task.GetWatts(), nil
		}
	} else {
		// Checking whether task.GetWatts is 0.0. If yes, then throwing an error.
		if task.GetWatts() == 0.0 {
			return task.GetWatts(), errors.New("Configuration error in task. GetWatts attribute is 0 for " + task.GetName())
		}
		return task.GetWatts(), nil
	}
}

// Compare two tasks.
func Compare(task1 Task, task2 Task) bool {
	// If comparing the same pointers (checking the addresses).
	if task1 == task2 {
		return true
	}
	if task1.GetTaskID() != task2.GetTaskID() {
		return false
	} else {
		return true
	}
}
