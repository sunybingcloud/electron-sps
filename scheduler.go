package main // import bitbucket.org/sunybingcloud/electron

import (
	"bitbucket.org/sunybingcloud/electron/events"
	elecHTTP "bitbucket.org/sunybingcloud/electron/http"
	elecLogDef "bitbucket.org/sunybingcloud/electron/logging/def"
	"bitbucket.org/sunybingcloud/electron/metrics"
	"bitbucket.org/sunybingcloud/electron/pcp"
	"bitbucket.org/sunybingcloud/electron/schedulers"
	"bitbucket.org/sunybingcloud/electron/utilities"
	"bitbucket.org/sunybingcloud/electron/utilities/httpUtils"
	"context"
	"flag"
	"fmt"
	"github.com/gogo/protobuf/proto"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	sched "github.com/mesos/mesos-go/api/v0/scheduler"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"time"
)

var master = flag.String("master", "xavier:5050", "Location of leading Mesos master")
var tasksFile = flag.String("workload", "", "JSON file containing task definitions")
var pcplogPrefix = flag.String("logPrefix", "", "Prefix for pcplog")
var hiThreshold = flag.Float64("hiThreshold", 0.0, "Upperbound for when we should start capping")
var loThreshold = flag.Float64("loThreshold", 0.0, "Lowerbound for when we should start uncapping")
var classMapWatts = flag.Bool("classMapWatts", false, "Enable mapping of watts to power class of node")
var schedPolicyName = flag.String("schedPolicy", "first-fit", "Name of the scheduling policy to be used.\n\tUse option -listSchedPolicies to get the names of available scheduling policies")
var listSchedPolicies = flag.Bool("listSchedPolicies", false, "List the names of the pluaggable scheduling policies.")
var enableSchedPolicySwitch = flag.Bool("switchSchedPolicy", false, "Enable switching of scheduling policies at runtime.")
var schedPolConfigFile = flag.String("schedPolConfig", "", "Config file that contains information for each scheduling policy.")
var fixFirstSchedPol = flag.String("fixFirstSchedPol", "", "Name of the scheduling policy to be deployed first, regardless of the distribution of tasks, provided switching is enabled.")
var fixSchedWindow = flag.Bool("fixSchedWindow", false, "Fix the size of the scheduling window that every deployed scheduling policy should schedule, provided switching is enabled.")
var schedWindowSize = flag.Int("schedWindowSize", 200, "Size of the scheduling window if fixSchedWindow is set.")
var schedPolSwitchCriteria = flag.String("schedPolSwitchCriteria", "taskDist", "Scheduling policy switching criteria.")
var port = flag.Int("port", 4545, "Port on which the HTTP server listens for task submissions. For this httpServer needs to be enabled.")
var tasksFromHTTP = flag.Bool("httpServer", false, "Run electron as an HTTP Server that accepts tasks via HTTP.")
var powerCapping = flag.String("powercap", "", "Powercapping algorithm. (default (''), extrema, "+
	"prog-extrema, opportunistic).")
var powerSpecs = flag.String("powerSpecs", "", "JSON file containing power consumption specs of each host in the cluster.")
var cpuArchInfo = flag.String("cpuArchInfo", "", "JSON file containing cpu architecture information of each host in the cluster.")
var alignmentScoreFunction = flag.String("alignScore", "", "Task to Offer alignment score function name.")
var listTaskOfferAlignmentOptions = flag.Bool("listTaskOfferAlignmentOptions", false, "List the possible methods for task-offer alignment.")
var enableTaskRanking = flag.Bool("rankTasks", false, "Enable ranking of tasks using task-ranker (https://github.com/pradykaushik/task-ranker).")
var taskRankingStrategy = flag.String("taskRankStrategy", "cpushares", "Name of the task ranking strategy. (default: cpushares)")
var prometheusEndpoint = flag.String("prometheusEndpoint", "http://localhost:9090", "Prometheus Endpoint (default http://localhost:9090.")
var taskRankerDataFetchIntervalSeconds = flag.Uint("taskRankerDataFetchIntervalSeconds", 5, "Number of seconds between successive fetches of time series data from prometheus (default 5s).")
var prometheusScrapeIntervalSeconds = flag.Uint("prometheusScrapeIntervalSeconds", 1, "Prometheus scrape interval as specified in Prometheus configuration (default 1s).")

// Short hand args
func init() {
	flag.StringVar(master, "m", "xavier:5050", "Location of leading Mesos master (shorthand)")
	flag.StringVar(tasksFile, "w", "", "JSON file containing task definitions (shorthand)")
	flag.StringVar(pcplogPrefix, "p", "", "Prefix for pcplog (shorthand)")
	flag.Float64Var(hiThreshold, "ht", 700.0, "Upperbound for when we should start capping (shorthand)")
	flag.Float64Var(loThreshold, "lt", 400.0, "Lowerbound for when we should start uncapping (shorthand)")
	flag.BoolVar(classMapWatts, "cmw", false, "Enable mapping of watts to power class of node (shorthand)")
	flag.StringVar(schedPolicyName, "sp", "first-fit", "Name of the scheduling policy to be used.\n	Use option -listSchedPolicies to get the names of available scheduling policies (shorthand)")
	flag.BoolVar(listSchedPolicies, "lsp", false, "Names of the pluaggable scheduling policies. (shorthand)")
	flag.BoolVar(enableSchedPolicySwitch, "ssp", false, "Enable switching of scheduling policies at runtime.")
	flag.StringVar(schedPolConfigFile, "spConfig", "", "Config file that contains information for each scheduling policy (shorthand).")
	flag.StringVar(fixFirstSchedPol, "fxFstSchedPol", "", "Name of the scheduling gpolicy to be deployed first, regardless of the distribution of tasks, provided switching is enabled (shorthand).")
	flag.BoolVar(fixSchedWindow, "fixSw", false, "Fix the size of the scheduling window that every deployed scheduling policy should schedule, provided switching is enabled (shorthand).")
	flag.IntVar(schedWindowSize, "swSize", 200, "Size of the scheduling window if fixSchedWindow is set (shorthand).")
	flag.StringVar(schedPolSwitchCriteria, "spsCriteria", "taskDist", "Scheduling policy switching criteria (shorthand).")
	flag.BoolVar(tasksFromHTTP, "httpSrv", false, "Run electron as an HTTP Server that accepts tasks via HTTP. (shorthand).")
	flag.StringVar(powerCapping, "pc", "", "Powercapping algorithm. (default (''), extrema, "+
		"prog-extrema, opportunistic) (shorthand).")
	flag.StringVar(powerSpecs, "powSp", "", "JSON file containing power consumption specs of each host in the cluster (shorthand).")
	flag.StringVar(cpuArchInfo, "cpuArch", "", "JSON file containing cpu architecture information of each host in the cluster (shorthand).")
	flag.StringVar(alignmentScoreFunction, "asf", "", "Task to Offer alignment score function name (shorthand).")
	flag.BoolVar(listTaskOfferAlignmentOptions, "lTskOfferAlignOpt", false, "List the possible methods for task-offer alignment (shorthand).")
	flag.BoolVar(enableTaskRanking, "rTasks", false, "Enable ranking of tasks using task-ranker (https://github.com/pradykaushik/task-ranker).")
	flag.StringVar(taskRankingStrategy, "rtStrategy", "cpushares", "Name of the task ranking strategy. (default: cpushares) (shorthand)")
	flag.StringVar(prometheusEndpoint, "promEndPt", "http://localhost:9090", "Prometheus Endpoint (default http://localhost:9090. (shorthand)")
	flag.UintVar(taskRankerDataFetchIntervalSeconds, "trDfIntSec", 5, "Number of seconds between successive fetches by task ranker of time series data from prometheus (default 5s). (shorthand)")
	flag.UintVar(prometheusScrapeIntervalSeconds, "promScrapeIntSec", 1, "Prometheus scrape interval as specified in Prometheus configuration (default 1s). (shorthand)")
}

func listAllSchedulingPolicies() {
	fmt.Println("Scheduling Policies")
	fmt.Println("-------------------")
	for policyName := range schedulers.SchedPolicies {
		fmt.Println(policyName)
	}
}

func listAllTaskOfferAlignmentOptions() {
	fmt.Println("Task-Offer Alignment Options")
	fmt.Println("----------------------------")
	fmt.Println("cpu-mem => (task.cpu * offer.availCpu) + (task.mem * offer.availMem)")
	fmt.Println("cpuUtil-mem => (task.weightedShareCpu[offer.host] * offer.host.5secHistAvgCpuUtil) + (task.mem * offer.availMem)")
	fmt.Println("cpuPowerSlack => Select offer with the max value for -(offer.host.TDP - offer.host.5secHistAvgCpuPower)")
	fmt.Println("cpuDramPowerSlack => Select offer with max value for -((offer.host.TDP + offer.host.maxDramPower) " +
		"- (offer.host.5secHistRunAvgCpuPower + offer.host.5secHistRunAvgDramPower))")
	fmt.Println("cpuUtilEst => Select offer corresponding to the host estimated " +
		"demonstrate the highest cpu utilization in the next window.")
	fmt.Println("weighted => Select offer with the max value for (cpu-mem + cpuUtil-mem + (2 * cpuPowerSlack)).")
}

func main() {
	flag.Parse()

	var wg sync.WaitGroup

	// checking to see if we need to just list the pluggable scheduling policies.
	if *listSchedPolicies {
		listAllSchedulingPolicies()
		os.Exit(0)
	}

	// Checking to see if we need to just list the possible task-offer alignment options.
	if *listTaskOfferAlignmentOptions {
		listAllTaskOfferAlignmentOptions()
		os.Exit(0)
	}

	// Creating logger and attaching different logging platforms.
	startTime := time.Now()
	formattedStartTime := startTime.Format("20060102150405")
	// Checking if prefix contains any special characters
	if strings.Contains(*pcplogPrefix, "/") {
		log.Fatal("log file prefix should not contain '/'.")
	}
	logPrefix := *pcplogPrefix + "_" + formattedStartTime
	logger := elecLogDef.BuildLogger(startTime, logPrefix)
	// Logging channels.
	logMType := make(chan elecLogDef.LogMessageType)
	logMsg := make(chan string)
	go logger.Listen(logMType, logMsg)

	// First we need to build the scheduler using scheduler options.
	var schedOptions []schedulers.SchedulerOptions = make([]schedulers.SchedulerOptions, 0, 10)

	// OPTIONAL PARAMETERS
	// Scheduling Policy Name
	// If non-default scheduling policy given, checking if name exists.
	if *schedPolicyName != "first-fit" {
		if _, ok := schedulers.SchedPolicies[*schedPolicyName]; !ok {
			// invalid scheduling policy
			log.Println("Invalid scheduling policy given. The possible scheduling policies are:")
			listAllSchedulingPolicies()
			os.Exit(1)
		}
		// If tolerance-aware-scheduler then checking if powerSpecs file provided.
		if *schedPolicyName == "tolerance-aware-scheduler" {
			if *powerSpecs == "" {
				log.Println("Power specifications file needs to be provided to be able to run tolerance-aware-scheduler.")
				os.Exit(1)
			}
		}
	}

	// Task Ranker.
	// If task ranking enabled, then configuring the task ranker with the specified taskRanksReceiver.
	if *enableTaskRanking {
		schedOptions = append(schedOptions,
			schedulers.WithTaskRanker(
				*prometheusEndpoint,
				*taskRankingStrategy,
				*taskRankerDataFetchIntervalSeconds,
				*prometheusScrapeIntervalSeconds))
	}

	// CHANNELS AND FLAGS.
	shutdownScheduling := utilities.NewSignalChannel()
	shutdownFramework := utilities.NewSignalChannel()
	httpServerShutdown := utilities.NewSignalChannel()
	pcpLog := utilities.NewSignalChannel()

	// Logging channels.
	// These channels are used by the framework to log messages.
	// The channels are used to send the type of log message and the message string.
	schedOptions = append(schedOptions, schedulers.WithLoggingChannels(logMType, logMsg))

	// Shutdown indicator channels.
	// These channels are used to notify,
	// 1. scheduling is complete.
	// 2. all scheduled tasks have completed execution and framework can shutdown.
	schedOptions = append(schedOptions, schedulers.WithShutdownScheduling(shutdownScheduling))
	schedOptions = append(schedOptions, schedulers.WithShutdownFramework(shutdownFramework))

	// If here, then valid scheduling policy name provided.
	if (*tasksFile == "") && !*tasksFromHTTP {
		logger.WriteLog(elecLogDef.ERROR, "No file containing tasks specification"+
			" provided.")
		os.Exit(1)
	}
	schedOptions = append(schedOptions,
		schedulers.WithSchedPolicy(
			*schedPolicyName,
			*tasksFile,
			*tasksFromHTTP,
			schedulers.WithAlignmentScoreFunction(*alignmentScoreFunction)))

	// Scheduling Policy Switching.
	if *enableSchedPolicySwitch {
		// Scheduling policy config file required.
		if spcf := *schedPolConfigFile; spcf == "" {
			logger.WriteLog(elecLogDef.ERROR, "No file containing characteristics for"+
				" scheduling policies")
			os.Exit(1)
		} else {
			// Initializing the characteristics of the scheduling policies.
			schedulers.InitSchedPolicyCharacteristics(spcf)
			schedOptions = append(schedOptions, schedulers.WithSchedPolSwitchEnabled(*enableSchedPolicySwitch, *schedPolSwitchCriteria))
			// Fix First Scheduling Policy.
			schedOptions = append(schedOptions, schedulers.WithNameOfFirstSchedPolToFix(*fixFirstSchedPol))
			// Fix Scheduling Window.
			schedOptions = append(schedOptions, schedulers.WithFixedSchedulingWindow(*fixSchedWindow, *schedWindowSize))
		}
	}

	// EVENT LISTENERS.
	eventListeners := []events.EventListener{
		// HostInfoListener to maintain hostname <-> slave ID mapping.
		metrics.NewHostInfoListenerInstance(logMType, logMsg),
	}
	// Checking to see if power specs and cpu architecture information provided.
	var umL events.EventListener
	if (*powerSpecs != "") && (*cpuArchInfo != "") {
		// Monitor runtime metrics for each task.
		rmL := metrics.NewRuntimeMetricsListener(logMType, logMsg)
		// Cluster-wide resource utilization metrics.
		umL = metrics.NewUtilizationMetricsListener(*powerSpecs, *cpuArchInfo, logMType, logMsg)
		// Also monitoring resource allocation metrics for all tasks.
		amL := metrics.NewAllocationMetricsListener(logMType, logMsg)

		// Allocation metrics listener uses resource utilization data to determine weighted shares per task.
		umL.(events.EventSource).Attach(amL)
		// Runtime metrics listener uses historic average allocated weight share of resources as one of the
		// factors for estimating execution time.
		amL.(events.EventSource).Attach(rmL)

		eventListeners = append(eventListeners, rmL)
		eventListeners = append(eventListeners, umL)
		eventListeners = append(eventListeners, amL)
	}
	schedOptions = append(schedOptions, schedulers.WithEventListeners(eventListeners))

	// If CMW is enabled then for each task the class_to_watts mapping is used to
	//      fit tasks into offers.
	// If CMW is disabled, then the Median of Medians Max Peak Power Usage value is used
	//	as the watts value for each task.
	if *classMapWatts {
		logger.WriteLog(elecLogDef.GENERAL, "Power class to Watts requirement "+
			"mapping being used...")
		schedOptions = append(schedOptions, schedulers.WithClassMapWatts(*classMapWatts))
	}
	// REQUIRED PARAMETERS.
	// PCP logging, Power capping and High and Low thresholds.
	var noPowercap bool
	var extrema bool
	var progExtrema bool
	var opportunisticCapper bool
	var powercapValues = map[string]struct{}{
		"":              {},
		"extrema":       {},
		"prog-extrema":  {},
		"opportunistic": {},
	}
	if _, ok := powercapValues[*powerCapping]; !ok {
		logger.WriteLog(elecLogDef.ERROR, "Incorrect power-capping algorithm specified.")
		os.Exit(1)
	} else {
		// Indicating which powercapping algorithm to use, if any.
		// The pcp-logging with/without powercapping will be run after the
		// scheduler has been configured.
		if *powerCapping == "" {
			noPowercap = true
		} else {
			if *powerCapping == "extrema" {
				extrema = true
			} else if *powerCapping == "prog-extrema" {
				progExtrema = true
			} else if *powerCapping == "opportunistic" {
				opportunisticCapper = true
			}
			// High and Low Thresholds.
			// These values are not used to configure the scheduler.
			// hiThreshold and loThreshold are passed to the powercappers.
			if *hiThreshold < *loThreshold {
				logger.WriteLog(elecLogDef.ERROR, "High threshold is of a"+
					" lower value than low threshold.")
				os.Exit(1)
			}
		}
	}

	// HTTP Server.
	// If httpServer is disabled, then path of file containing workload needs to be provided.
	if *tasksFromHTTP {
		// Validating port.
		err := httpUtils.ValidatePort(*port)
		if err != nil {
			logger.WriteLog(elecLogDef.ERROR, err.Error())
			os.Exit(1)
		}
		schedOptions = append(schedOptions, schedulers.WithTasksFromHTTPServer(*tasksFromHTTP, *port))
		schedOptions = append(schedOptions, schedulers.WithHTTPServerShutdown(httpServerShutdown))
	}

	// Scheduler.
	scheduler := schedulers.GetElectronScheduler(schedOptions...)
	// Starting the HTTP server if necessary.
	// Note: ElectronAPIHandler needs to instantiated after the scheduler has been configured
	// 	and built. This is because ElectronAPIHandler retrieves a copy of the
	// 	ElectronScheduler (The value of which is nil before being configured).
	if *tasksFromHTTP {
		compatibleTaskType := schedulers.CompatibleTaskTypes[*schedPolicyName]
		server := &http.Server{
			Handler: elecHTTP.GetElectronAPIHandler(compatibleTaskType, httpServerShutdown, logMType, logMsg),
		}
		wg.Add(1)
		// Gracefully shutting down the HTTP server.
		go func() {
			httpServerShutdown.WaitTillClosed()
			msgColor := elecLogDef.LogMessageColors[elecLogDef.WARNING]
			msg := msgColor.Sprint("Shutting down HTTP server...")
			// logger.WriteLog(elecLogDef.WARNING, msg)
			logMType <- elecLogDef.WARNING
			logMsg <- msg
			server.Shutdown(context.TODO())
			wg.Done()
		}()
		wg.Add(1)
		// Starting the server.
		log.Println("Starting the HTTP Server on port ", *port, "...")
		go func() {
			listener, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
			if err != nil {
				log.Println(err)
			} else {
				server.Serve(listener)
			}
			wg.Done()
		}()
	}

	// Scheduler driver.
	driver, err := sched.NewMesosSchedulerDriver(sched.DriverConfig{
		Master: *master,
		Framework: &mesos.FrameworkInfo{
			Name: proto.String("Electron"),
			User: proto.String(""),
		},
		Scheduler: scheduler,
	})
	if err != nil {
		logger.WriteLog(elecLogDef.ERROR, fmt.Sprintf("Unable to create scheduler driver: %s", err))
		os.Exit(1)
	}

	// Starting PCP logging.
	wg.Add(1)
	if noPowercap {
		if umL == nil {
			go pcp.Start(pcpLog, logMType, logMsg, scheduler, &wg)
		} else {
			go pcp.Start(pcpLog, logMType, logMsg, scheduler, &wg, umL)
		}
	} else if extrema {
		if umL == nil {
			go pcp.StartPCPLogAndExtremaDynamicCap(pcpLog, *hiThreshold, *loThreshold, logMType, logMsg, &wg)
		} else {
			go pcp.StartPCPLogAndExtremaDynamicCap(pcpLog, *hiThreshold, *loThreshold, logMType, logMsg, &wg, umL)
		}
	} else if progExtrema {
		if umL == nil {
			go pcp.StartPCPLogAndProgressiveExtremaCap(pcpLog, *hiThreshold, *loThreshold, logMType, logMsg, &wg)
		} else {
			go pcp.StartPCPLogAndProgressiveExtremaCap(pcpLog, *hiThreshold, *loThreshold, logMType, logMsg, &wg, umL)
		}
	} else if opportunisticCapper {
		if umL == nil {
			go pcp.StartPCPLogAndOpportunisticCap(pcpLog, logMType, logMsg, &wg)
		} else {
			go pcp.StartPCPLogAndOpportunisticCap(pcpLog, logMType, logMsg, &wg, umL)
		}
	}

	// Take a second between starting PCP log and continuing.
	time.Sleep(1 * time.Second)

	// Attempt to handle SIGINT to not leave pmdumptext running.
	// Catch interrupt.
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt, os.Kill)
		s := <-c
		if s != os.Interrupt {
			pcpLog.Close()
			return
		}

		log.Println("Received SIGINT... stopping")

		if *tasksFromHTTP {
			httpServerShutdown.Close()
		}
		scheduler.(schedulers.ElectronScheduler).LogShutdownScheduling()
		shutdownScheduling.Close()
		scheduler.(schedulers.ElectronScheduler).LogShutdownFramework()
		shutdownFramework.Close()
	}()

	wg.Add(1)
	// Listen on scheduling channels to shutdown framework when scheduling is complete
	// and all the tasks have completed execution.
	go func() {

		defer wg.Done()

		// Wait till scheduling has been terminated.
		// Signals we have scheduled every task.
		shutdownScheduling.WaitTillClosed()
		// Wait till its time to shutdown the framework.
		shutdownFramework.WaitTillClosed()
		// All tasks have finished execution. Okay to shutdown.
		pcpLog.Close()
		time.Sleep(5 * time.Second) //Wait for PCP to log a few more seconds
		// Logging metrics collected and recorded by the scheduler.
		// All the metrics are logged once, just before the shutdown of the
		// framework.
		// Note: LogMetrics internally calls EventListener#WriteLogs(). Event listeners can dump any
		// pending data to the corresponding log files at this point.
		scheduler.(schedulers.ElectronScheduler).LogMetrics()
		// Stopping monitoring of all metrics.
		for _, l := range eventListeners {
			l.Stop()
		}

		// Done shutting down
		driver.Stop(false)

	}()

	// Starting the scheduler driver.
	if status, err := driver.Run(); err != nil {
		log.Printf("Framework stopped with status %s and error: %s\n", status.String(), err.Error())
	}
	logMType <- elecLogDef.GENERAL
	logMsg <- "Exiting..."
	wg.Wait()
	// All goroutines have completed.
	// Closing logging channels.
	close(logMType)
	close(logMsg)
}
