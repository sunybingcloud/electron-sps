package http

import (
	"bitbucket.org/sunybingcloud/electron/def"
	elecLogDef "bitbucket.org/sunybingcloud/electron/logging/def"
	"bitbucket.org/sunybingcloud/electron/schedulers"
	"bitbucket.org/sunybingcloud/electron/utilities"
	"encoding/json"
	"io"
	"net/http"
	"strings"
	"sync"
)

type ElectronAPIHandler struct {
	serveMux           map[string]func(http.ResponseWriter, *http.Request) func(*ElectronAPIHandler)
	baseSchedRef       schedulers.ElectronScheduler
	taskType           string
	httpServerShutdown *utilities.SignalChannel
	lmt                chan elecLogDef.LogMessageType
	lmsg               chan string
	sync.Mutex
}

// URLs
var (
	newTasks = "/submit"
	shutdown = "/shutdown"
)

// Form variables.
const (
	FORM_ELEMENT_SECTION = "section"
	FORM_ELEMENT_JOB     = "job"
)

// Number of bytes of the job to store in memory.
// Given a sufficiently large number to keep most of the job in memory.
const JOB_PART_IN_MEMORY = 16 << 20 // 16 MegaBytes

// Error Messages
var (
	undefinedURLErrString = func(url string) string {
		return "electron: Undefined URL[" + url + "]"
	}

	invalidJobFormatErrString = func(err error) string {
		return "electron: Submitted job format is invalid. " + err.Error() + "\n"
	}

	noJobSubmittedErrString = func() string {
		return "electron: No job submitted\n"
	}

	formParsingErrString = func(err error) string {
		return "electron: Error parsing form. " + err.Error() + "\n"
	}
)

func GetElectronAPIHandler(
	taskType string,
	serverShutdown *utilities.SignalChannel,
	logMType chan elecLogDef.LogMessageType,
	logMsg chan string) *ElectronAPIHandler {

	e := &ElectronAPIHandler{}
	e.taskType = taskType
	e.serveMux = make(map[string]func(http.ResponseWriter, *http.Request) func(*ElectronAPIHandler))
	e.serveMux[newTasks] = submitTasks
	e.serveMux[shutdown] = shutdownServer

	e.baseSchedRef = schedulers.GetElectronSchedulerInstance()
	e.httpServerShutdown = serverShutdown

	// Logging channels.
	e.lmt = logMType
	e.lmsg = logMsg

	return e
}

func (e *ElectronAPIHandler) ServeHTTP(rw http.ResponseWriter, rq *http.Request) {
	// Retrieving the handle for the specified URL.
	if requestHandler, ok := e.serveMux[rq.URL.String()]; ok {
		requestHandler(rw, rq)(e)
		return
	}

	// Writing error message.
	io.WriteString(rw, undefinedURLErrString(rq.URL.String()))
}

func (e *ElectronAPIHandler) logWorkloadSection(section string) {
	e.Lock()
	defer e.Unlock()
	e.lmt <- elecLogDef.WORKLOAD_SECTION
	e.lmsg <- section
}

func submitTasks(rw http.ResponseWriter, rq *http.Request) func(*ElectronAPIHandler) {
	return func(e *ElectronAPIHandler) {
		if err := rq.ParseMultipartForm(JOB_PART_IN_MEMORY); err != nil {
			io.WriteString(rw, formParsingErrString(err))
		} else {
			workloadSection := rq.Form.Get(FORM_ELEMENT_SECTION)
			workloadSectionExists := false
			if workloadSection != "" {
				workloadSectionExists = true
				e.logWorkloadSection(workloadSection)
			}
			// Note that here 'job' refers to a set of tasks submitted at the same time for scheduling.
			// It does not in any way refer to set of tasks having dependencies.
			job := rq.Form.Get(FORM_ELEMENT_JOB)
			switch job {
			case "":
				io.WriteString(rw, noJobSubmittedErrString())
			default:
				decoder := json.NewDecoder(strings.NewReader(job))
				var newTasks []def.Task
				newTasks, err = def.UnmarshalTasks(e.taskType, decoder)
				if err != nil {
					io.WriteString(rw, invalidJobFormatErrString(err))
				} else {
					if workloadSectionExists {
						// Just embedding section name in each task.
						// remove this.
						for i := 0; i < len(newTasks); i++ {
							newTasks[i].SetWorkloadSection(workloadSection)
						}
					}
					e.baseSchedRef.NewTasks(newTasks)
				}
			}
		}
	}
}

// Gracefully shutdown the HTTP server.
// After this, no more tasks can be submitted using the HTTP api.
func shutdownServer(http.ResponseWriter, *http.Request) func(*ElectronAPIHandler) {
	return func(e *ElectronAPIHandler) {
		// Closing the channel to indicate that the HTTP server has been shutdown.
		e.httpServerShutdown.Close()
	}
}
