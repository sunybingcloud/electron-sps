/*
A utility to calculate the running average.

One should implement Val() and ID() to use this utility.
*/

package runAvg

import (
	"container/list"
	"errors"
)

type Interface interface {
	// Value to use for running average calculation.
	Val() float64
	// Unique ID associated with element.
	// Should be used when random removal of elements is required.
	ID() string
}

type RunAvgCalculator struct {
	considerationWindow     *list.List
	considerationWindowSize int
	currentSum              float64
}

// Compute the running average by adding 'data' to the window.
// Updating currentSum to get constant time complexity for every running average computation.
func (r *RunAvgCalculator) calculate(data Interface) float64 {
	if r.considerationWindow.Len() < r.considerationWindowSize {
		r.considerationWindow.PushBack(data)
		r.currentSum += data.Val()
	} else {
		// removing the element at the front of the window.
		elementToRemove := r.considerationWindow.Front()
		r.currentSum -= elementToRemove.Value.(Interface).Val()
		r.considerationWindow.Remove(elementToRemove)

		// adding new element to the window
		r.considerationWindow.PushBack(data)
		r.currentSum += data.Val()
	}
	return r.currentSum / float64(r.considerationWindow.Len())
}

/*
If element with given ID present in the window, then remove it and return (removeElement, nil).
Else, return (nil, error)
*/
func (r *RunAvgCalculator) removeFromWindow(id string) (interface{}, error) {
	for element := r.considerationWindow.Front(); element != nil; element = element.Next() {
		if elementToRemove := element.Value.(Interface); elementToRemove.ID() == id {
			r.considerationWindow.Remove(element)
			r.currentSum -= elementToRemove.Val()
			return elementToRemove, nil
		}
	}
	return nil, errors.New("Error: Element not found in the window.")
}

// Taking windowSize as a parameter to allow for sliding window implementation.
func (r *RunAvgCalculator) Calc(data Interface) float64 {
	return r.calculate(data)
}

// Resize window.
func (r *RunAvgCalculator) ResizeWindow(newWindowSize int) {
	r.considerationWindowSize = newWindowSize
}

// Remove element from the window if it is present.
func (r *RunAvgCalculator) Remove(id string) (interface{}, error) {
	return r.removeFromWindow(id)
}

// initialize the parameters of the running average calculator
func Init(windowSize int) *RunAvgCalculator {
	// Setting parameters to default values.
	// Could also set racSingleton to nil but this leads to unnecessary overhead of creating
	// another instance when Calc is called.
	return &RunAvgCalculator{
		considerationWindow:     list.New(),
		considerationWindowSize: windowSize,
		currentSum:              0.0,
	}
}
