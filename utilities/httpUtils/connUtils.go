package httpUtils

import (
	"fmt"
	"github.com/pkg/errors"
	"net"
)

func ValidatePort(port int) error {
	conn, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", port))
	defer conn.Close()
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("Port[%d] is invalid!", port))
	}
	return nil
}
