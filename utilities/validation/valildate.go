package validation

import "github.com/pkg/errors"

type ValidatorFunc func() error

func Validate(baseErrMsg string, validatorFuncs ...ValidatorFunc) error {
	for _, f := range validatorFuncs {
		if err := f(); err != nil {
			return errors.Wrap(err, baseErrMsg)
		}
	}
	return nil
}
