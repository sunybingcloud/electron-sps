Electron: A power budget manager
======================================

To Do:

 * Write test code for each scheduler (This should be after the design change)
 * Have a centralised logFile that can be filtered by identifier. All electron logs should go into this file.
 * Convert def#WattsToConsider(...) to be a receiver of def.Task and change the name of it to Watts(...).
 * Handle powerclass not configured on a node condition. As of now, an assumption is made that the powerclass is configured
 * Refine the sorting algorithm that sorts the clusters of tasks retrieved using the kmeans algorithm. This also involves the reduction in time complexity of the same.
 * Config file input instead of command line arguments? Config file should accept same parameters as command line.
   for all the nodes.

**Requires [Performance Co-Pilot](http://pcp.io/) tool pmdumptext to be installed on the
machine on which electron is launched for logging to work and PCP collector agents installed
on the Mesos Agents**

Compatible with the following versions,

* Mesos 1.6.0
* Go 1.12.9

Package management is using [go modules](https://blog.golang.org/using-go-modules).
To download all dependencies, run `make test`.


How to run (Use the --help option to get information about other command-line options):

`./electron -workload <workload json>`

By default, runtime metrics are monitored and logged. In addition to this, it is also possible to monitor
and log resource allocation metrics for each task, and resource utilization of the cluster.<br>
To monitor these additional metrics a specification file (JSON) needs to be provided that contains the 
following information for each host in the cluster (look at [powerSpecs.json](powerSpecs.json)).

 * Idle power consumption of CPU (Watts).
 * Idle power consumption of DRAM (Watts). 
 * Thermal Design Power (Watts).
 * Max power consumption (CPU + DRAM) (Watts).

Use the `-powerSpecs` flag to specify the location of the specification file.

`./electron -workload <workload json> -powerSpecs <power spec file location>`

To run electron with Watts as Resource, run the following command,

`./electron -workload <workload json> -wattsAsAResource`

To run electron with Scheduling Policy Switching Enabled, run the following command,

`./electron -workload <workload json> -ssp -spConfig <schedPolicy config file>`

To be able to submit tasks via HTTP, run electron with the -httpServer flag. Use the below command.

`./electron -httpServer -port <port>`

Run the following `curl` command to submit a task, as JSON, to electron via HTTP.

```
curl -X POST --header "Content-Type: multipart/form-data" \
--form 'section=<section name>' \
--form 'job=<workload json>' \
http:<hostname>:<port>/submit
```
_Possible workload section names are *low-power-consuming*, *high-power-consuming*, *sparse*, and *dense*_.

Workload schema:

```
[
   {
      "name": "minife",
      "cpu": 3.0,
      "ram": 4096,
      "watts": 63.141,
      "class_to_watts": {
        "A": 93.062,
        "B": 65.552,
        "C": 57.897,
        "D": 60.729
      },
      "image": "rdelvalle/minife:electron1",
      "cmd": "cd src && mpirun -np 3 miniFE.x -nx 100 -ny 100 -nz 100",
      "inst": 10
   },
   {
      "name": "dgemm",
      "cpu": 3.0,
      "ram": 32,
      "watts": 85.903,
      "class_to_watts": {
        "A": 114.789,
        "B": 89.133,
        "C": 82.672,
        "D": 81.944
      },
      "image": "rdelvalle/dgemm:electron1",
      "cmd": "/./mt-dgemm 1024",
      "inst": 10
   }
]
```
