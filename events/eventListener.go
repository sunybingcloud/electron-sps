package events

// Listener to a type of event.
type EventListener interface {
	// Get types event listening to.
	// A listener might be listening to more than one type of event.
	GetEventTypes() []EventType
	// Update state based on occurrence of the corresponding event.
	// If the type of data provided is invalid, then error is thrown.
	Update(EventType, EventData) error

	String() string

	// Persist logs.
	// It is possible that WriteLogs() is called multiple times, as a listener can be listening for
	// multiple events. In this case, it is the job of the listener to persist the logs only once.
	// Note: If any event listener wants to log all the data only once, then they can implement this. Otherwise,
	// it is recommended to write another function that is called internally to periodically log data.
	WriteLogs()

	// Stop monitoring metrics.
	Stop()
}
