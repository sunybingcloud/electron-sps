package events

import "reflect"

var eventNames []string

// Event Types.
var (
	// PCP data retrieved.
	PCP = eventNameToEventType("PCP")

	// Task fit into a Mesos resource offer.
	TASK_OFFER_MATCH = eventNameToEventType("TASK_OFFER_MATCH")

	// A generic task status event.
	TASK_STATUS_UPDATE = eventNameToEventType("TASK_STATUS_UPDATE")

	// No more tasks left to schedule. Pending task queue is now empty.
	PENDING_TASK_QUEUE_EMPTY = eventNameToEventType("PENDING_TASK_QUEUE_EMPTY")

	// All scheduled tasks have completed execution. Scheduler is now going to be shutdown.
	SHUTDOWN_SCHEDULER = eventNameToEventType("SHUTDOWN_SCHEDULER")

	// Power Specifications of each host.
	POWER_SPECS_BROADCAST = eventNameToEventType("POWER_SPECS_BROADCAST")

	// Resource utilization of hosts has updated.
	RESOURCE_UTILIZATION_UPDATE = eventNameToEventType("RESOURCE_UTILIZATION_UPDATE")

	// Allocation statistics available for task that just completed execution.
	ALLOCATION_STATS_UPDATE = eventNameToEventType("ALLOCATION_STATS_AVAILABLE")
)

type EventType int

func (et EventType) String() string {
	return eventNames[et]
}

func eventNameToEventType(eventName string) EventType {
	// Appending eventName to eventNames.
	eventNames = append(eventNames, eventName)
	// Mapping eventName to int.
	return EventType(len(eventNames) - 1)
}

type InvalidEventTypeError struct {
	GivenType     EventType
	RequiredTypes string
}

func (e *InvalidEventTypeError) Error() string {
	errMsg := "Invalid event type! Given: " + e.GivenType.String() +
		", RequiredTypes: " + e.RequiredTypes
	return errMsg
}

// Error with regard to invalid type of data provided.
// EventListener for a certain EventType might require data of a certain type.
// If the data passed isn't of the required type then this error is thrown.
type InvalidDataForEventError struct {
	GivenType    reflect.Type
	RequiredType reflect.Type
}

// Return the error message.
func (e *InvalidDataForEventError) Error() string {
	errMsg := "Invalid type for event data! Given: " + e.GivenType.String() +
		", Required: " + e.RequiredType.String()
	return errMsg
}
