package events

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"fmt"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	"log"
	"reflect"
)

// Data corresponding to an event.
// For each EventType, corresponding EventData defined.
type EventData interface {
	// Get the corresponding data.
	Get() interface{}
	// Getters for commonly shared information.
	GetTaskID() string
	GetHostname() string
}

type EventDataOptions func(e EventData) error

// BASE EVENT DATA encapsulating information that is shared as part of more of than 1 type of event.
type baseEventData struct {
	taskID   string
	hostname string
}

func WithTaskID(taskID string) EventDataOptions {
	return func(e EventData) error {
		switch e.(type) {
		case *TaskOfferMatchEventData:
			e.(*TaskOfferMatchEventData).baseEventData.taskID = taskID
			return nil
		case *AllocationStatsUpdateEventData:
			e.(*AllocationStatsUpdateEventData).baseEventData.taskID = taskID
			return nil
		default:
			return &InvalidEventDataOptionError{
				GivenType:     reflect.TypeOf(e),
				RequiredTypes: []reflect.Type{reflect.TypeOf(PCPEventData{})},
			}
		}
	}
}

func WithHostname(hostname string) EventDataOptions {
	return func(e EventData) error {
		switch e.(type) {
		case *TaskOfferMatchEventData:
			e.(*TaskOfferMatchEventData).baseEventData.hostname = hostname
		case *AllocationStatsUpdateEventData:
			e.(*AllocationStatsUpdateEventData).baseEventData.hostname = hostname
		default:
			return &InvalidEventDataOptionError{
				GivenType: reflect.TypeOf(e),
				RequiredTypes: []reflect.Type{
					reflect.TypeOf(TaskOfferMatchEventData{}),
					reflect.TypeOf(AllocationStatsUpdateEventData{}),
				},
			}
		}
		return nil
	}
}

// Data for PCP event.
type PCPEventData struct {
	Val string
}

func (d PCPEventData) Get() interface{} {
	return d.Val
}

func (d PCPEventData) GetTaskID() string {
	return ""
}

func (d PCPEventData) GetHostname() string {
	return ""
}

func NewPCPEventData(options ...EventDataOptions) *PCPEventData {
	p := &PCPEventData{}
	for _, opt := range options {
		// applying options
		if err := opt(p); err != nil {
			log.Fatal(err)
		}
	}
	return p
}

func WithPCPData(pcpData string) EventDataOptions {
	return func(e EventData) error {
		switch e.(type) {
		case *PCPEventData:
			e.(*PCPEventData).Val = pcpData
			return nil
		default:
			return &InvalidEventDataOptionError{
				GivenType:     reflect.TypeOf(e),
				RequiredTypes: []reflect.Type{reflect.TypeOf(PCPEventData{})},
			}
		}
	}
}

// Data for TASK_OFFER_MATCH event.
type TaskOfferMatchEventData struct {
	Task                 def.Task
	Offer                *mesos.Offer
	ClassMapWattsEnabled bool
	baseEventData
}

func (d TaskOfferMatchEventData) Get() interface{} {
	return d
}

func (d TaskOfferMatchEventData) GetTaskID() string {
	return d.baseEventData.taskID
}

func (d TaskOfferMatchEventData) GetHostname() string {
	return d.baseEventData.hostname
}

func NewTaskOfferMatchEventData(options ...EventDataOptions) *TaskOfferMatchEventData {
	t := &TaskOfferMatchEventData{}
	for _, opt := range options {
		// applying options.
		if err := opt(t); err != nil {
			log.Fatal(err)
		}
	}
	return t
}

func WithTask(task def.Task) EventDataOptions {
	return func(e EventData) error {
		switch e.(type) {
		case *TaskOfferMatchEventData:
			e.(*TaskOfferMatchEventData).Task = task
			return nil
		default:
			return &InvalidEventDataOptionError{
				GivenType:     reflect.TypeOf(e),
				RequiredTypes: []reflect.Type{reflect.TypeOf(TaskOfferMatchEventData{})},
			}
		}
	}
}

func WithOffer(offer *mesos.Offer) EventDataOptions {
	return func(e EventData) error {
		switch e.(type) {
		case *TaskOfferMatchEventData:
			e.(*TaskOfferMatchEventData).Offer = offer
			return nil
		default:
			return &InvalidEventDataOptionError{
				GivenType:     reflect.TypeOf(e),
				RequiredTypes: []reflect.Type{reflect.TypeOf(TaskOfferMatchEventData{})},
			}
		}
	}
}

func WithClassMapWattsEnabled(cmwEnabled bool) EventDataOptions {
	return func(e EventData) error {
		switch e.(type) {
		case *TaskOfferMatchEventData:
			e.(*TaskOfferMatchEventData).ClassMapWattsEnabled = cmwEnabled
			return nil
		default:
			return &InvalidEventDataOptionError{
				GivenType:     reflect.TypeOf(e),
				RequiredTypes: []reflect.Type{reflect.TypeOf(TaskOfferMatchEventData{})},
			}
		}
	}
}

// Data for Task Status events.
// This includes TASK_STARTING, TASK_FINISHED, TASK_ERROR, TASK_FAILED.
type TaskStatusEventData struct {
	Status *mesos.TaskStatus
}

func (d TaskStatusEventData) Get() interface{} {
	return d.Status
}

func (d TaskStatusEventData) GetTaskID() string {
	return ""
}

func (d TaskStatusEventData) GetHostname() string {
	return ""
}

func NewTaskStatusEventData(options ...EventDataOptions) *TaskStatusEventData {
	t := &TaskStatusEventData{}
	for _, opt := range options {
		// applying options.
		if err := opt(t); err != nil {
			log.Fatal(err)
		}
	}
	return t
}

func WithTaskStatus(status *mesos.TaskStatus) EventDataOptions {
	return func(e EventData) error {
		switch e.(type) {
		case *TaskStatusEventData:
			e.(*TaskStatusEventData).Status = status
			return nil
		default:
			return &InvalidEventDataOptionError{
				GivenType:     reflect.TypeOf(e),
				RequiredTypes: []reflect.Type{reflect.TypeOf(TaskStatusEventData{})},
			}
		}
	}
}

// Power specifications of each host.
type PowerSpecsBroadcastEventData map[string]struct {
	// POWER SPECIFICATIONS.
	// CPU power consumption when no tasks are running on the cluster.
	IdlePowerWattsCpu float64
	// DRAM power consumption when no tasks are running on the cluster.
	IdlePowerWattsDram float64
	// Thermal design power.
	TDPWatts float64
	// Maximum available power (cpu + dram).
	MaxCpuDramPowerWatts float64
}

func (d PowerSpecsBroadcastEventData) Get() interface{} {
	return d
}

func (d PowerSpecsBroadcastEventData) GetTaskID() string {
	return ""
}

func (d PowerSpecsBroadcastEventData) GetHostname() string {
	return ""
}

// Resource utilization data for each host.
type ResourceUtilizationUpdateEventData map[string]struct {
	// Total power consumed by CPU (Watts).
	TotalPowerCpu float64
	// Total power consumed by DRAM (Watts).
	TotalPowerDram float64
	// CPU utilization.
	CpuUtilization float64
}

func (d ResourceUtilizationUpdateEventData) Get() interface{} {
	return d
}

func (d ResourceUtilizationUpdateEventData) GetTaskID() string {
	return ""
}

func (d ResourceUtilizationUpdateEventData) GetHostname() string {
	return ""
}

// Latest allocation statistics for task.
type AllocationStatsUpdateEventData struct {
	AvgActualShareCpu   float64
	AvgActualSharePower float64
	baseEventData
}

func (d AllocationStatsUpdateEventData) Get() interface{} {
	return d
}

func (d AllocationStatsUpdateEventData) GetTaskID() string {
	return d.baseEventData.taskID
}

func (d AllocationStatsUpdateEventData) GetHostname() string {
	return d.baseEventData.hostname
}

func NewAllocationStatsUpdateEventData(options ...EventDataOptions) *AllocationStatsUpdateEventData {
	t := &AllocationStatsUpdateEventData{}
	for _, opt := range options {
		// applying options.
		if err := opt(t); err != nil {
			log.Fatal(err)
		}
	}
	return t
}

func WithAvgActualShareCpu(avgActualShareCpu float64) EventDataOptions {
	return func(e EventData) error {
		switch e.(type) {
		case *AllocationStatsUpdateEventData:
			e.(*AllocationStatsUpdateEventData).AvgActualShareCpu = avgActualShareCpu
			return nil
		default:
			return &InvalidEventDataOptionError{
				GivenType:     reflect.TypeOf(e),
				RequiredTypes: []reflect.Type{reflect.TypeOf(AllocationStatsUpdateEventData{})},
			}
		}
	}
}

func WithAvgActualSharePower(avgActualSharePower float64) EventDataOptions {
	return func(e EventData) error {
		switch e.(type) {
		case *AllocationStatsUpdateEventData:
			e.(*AllocationStatsUpdateEventData).AvgActualSharePower = avgActualSharePower
			return nil
		default:
			return &InvalidEventDataOptionError{
				GivenType:     reflect.TypeOf(e),
				RequiredTypes: []reflect.Type{reflect.TypeOf(AllocationStatsUpdateEventData{})},
			}
		}
	}
}

// Each EventDataOption needs to called to build a certain type of EventData.
// If there is a mismatch between data provided and the target EventData, this error is thrown.
type InvalidEventDataOptionError struct {
	GivenType     reflect.Type
	RequiredTypes []reflect.Type
}

// Return the error message.
func (e *InvalidEventDataOptionError) Error() string {
	errMsg := fmt.Sprintf("Invalid EventDataOption for EventData! Given: %s, Required: %v",
		e.GivenType.String(), e.RequiredTypes)
	return errMsg
}
