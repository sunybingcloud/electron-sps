package events

import (
	"bitbucket.org/sunybingcloud/electron/def"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPCPEventData_Get(t *testing.T) {
	pcpEventData := NewPCPEventData(WithPCPData("pcp data"))
	assert.Equal(t, pcpEventData.Get().(string), "pcp data")
}

func TestTaskOfferMatchEventData(t *testing.T) {
	inst := 1
	var task def.Task
	task = &def.SimpleTask{
		Name:      "test-task",
		CPU:       1.0,
		RAM:       1024,
		Instances: &inst,
	}

	offerId := "test-offerID"
	offer := &mesos.Offer{Id: &mesos.OfferID{Value: &offerId}}
	taskOfferMatchEventData := NewTaskOfferMatchEventData(
		WithTaskID("test-taskID"),
		WithHostname("test-hostname"),
		WithTask(task),
		WithOffer(offer))

	offerMatchEventTask := taskOfferMatchEventData.Get().(TaskOfferMatchEventData).Task
	assert.True(t, def.Compare(offerMatchEventTask, task))
	assert.True(t, taskOfferMatchEventData.GetTaskID() == "test-taskID")
	assert.True(t, taskOfferMatchEventData.GetHostname() == "test-hostname")
	assert.False(t, taskOfferMatchEventData.ClassMapWattsEnabled)
}

func TestTaskStatusEventData_Get(t *testing.T) {
	taskID := "test-taskID"
	taskState := mesos.TaskState_TASK_FINISHED
	taskStatusEventData := NewTaskStatusEventData(
		WithTaskStatus(&mesos.TaskStatus{
			TaskId: &mesos.TaskID{Value: &taskID},
			State:  &taskState,
		}))

	assert.Equal(t, taskStatusEventData.Get().(*mesos.TaskStatus).
		TaskId.GetValue(), taskID)
	assert.Equal(t, taskStatusEventData.Get().(*mesos.TaskStatus).
		State.String(), taskState.String())
}

func TestPowerSpecsBroadcastEventData_Get(t *testing.T) {
	powerSpecsBroadcastEventData := PowerSpecsBroadcastEventData{
		"test-hostname": {
			IdlePowerWattsCpu:    10.0,
			IdlePowerWattsDram:   3.0,
			TDPWatts:             180.0,
			MaxCpuDramPowerWatts: 210.0,
		},
	}

	powerSpecs := powerSpecsBroadcastEventData.Get().(PowerSpecsBroadcastEventData)
	assert.Equal(t, powerSpecs["test-hostname"].IdlePowerWattsCpu, 10.0)
	assert.Equal(t, powerSpecs["test-hostname"].IdlePowerWattsDram, 3.0)
	assert.Equal(t, powerSpecs["test-hostname"].TDPWatts, 180.0)
	assert.Equal(t, powerSpecs["test-hostname"].MaxCpuDramPowerWatts, 210.0)
}

func TestResourceUtilizationUpdateEventData_Get(t *testing.T) {
	resourceUtilizationEventData := ResourceUtilizationUpdateEventData{
		"test-hostname": {
			TotalPowerCpu:  70.0,
			TotalPowerDram: 25.0,
			CpuUtilization: 0.3,
		},
	}

	resourceUtil := resourceUtilizationEventData.Get().(ResourceUtilizationUpdateEventData)
	assert.Equal(t, resourceUtil["test-hostname"].TotalPowerCpu, 70.0)
	assert.Equal(t, resourceUtil["test-hostname"].TotalPowerDram, 25.0)
	assert.Equal(t, resourceUtil["test-hostname"].CpuUtilization, 0.3)
}

func TestAllocationStatsUpdateEventData_Get(t *testing.T) {
	allocStatsUpdateEventData := AllocationStatsUpdateEventData{
		baseEventData: baseEventData{
			taskID:   "test-taskID",
			hostname: "test-hostname",
		},
		AvgActualShareCpu:   30.0,
		AvgActualSharePower: 40.0,
	}

	allocStats := allocStatsUpdateEventData.Get().(AllocationStatsUpdateEventData)
	assert.Equal(t, allocStats.hostname, "test-hostname")
	assert.Equal(t, allocStats.taskID, "test-taskID")
	assert.Equal(t, allocStats.AvgActualShareCpu, 30.0)
	assert.Equal(t, allocStats.AvgActualSharePower, 40.0)
}
