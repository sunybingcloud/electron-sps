package taskQueue

import (
	"bitbucket.org/sunybingcloud/electron/def"
)

// TaskQueue is an abstraction for a task queue.
type TaskQueue interface {
	// EnqueueTask adds the given task to the queue.
	EnqueueTask(def.Task)
	// EnqueueTasks adds the given tasks to the queue.
	EnqueueTasks(...def.Task)
	// BeginIter returns iterator of the queue.
	BeginIter() Iterator
	// BeginReverseIter returns a reverse iterator for the queue.
	BeginReverseIter() ReverseIterator
	// Return the length of the queue.
	Len() int
	// IsEmpty returns boolean indicating whether the task is empty.
	IsEmpty() bool
	// GetTasks returns the tasks.
	GetTasks() []def.Task
	// At returns a pointer to the task at given index.
	// Returns nil if invalid index provided.
	At(index int) def.Task
	// RemoveAt removes the task present at the given index.
	RemoveAt(index int)
	// String returns the string representation of the task queue.
	String() string
	// Synchronization methods.
	Lock()
	Unlock()
}

// SortableTaskQueue is an abstraction for a task queue that allows explicit reordering of tasks.
type SortableTaskQueue interface {
	TaskQueue
	// Sort the task queue in non-decreasing order based on the sorting criteria.
	Sort(def.SortBy)
	// SortReverse sorts the task queue in non-increasing order based on the given sorting criteria.
	SortReverse(def.SortBy)
	// SortN sorts only the first N tasks of the queue in non-decreasing order based on the sorting criteria.
	SortN(int, def.SortBy)
	// SortNReverse sorts only first N tasks of the queue in the non-increasing order based on the sorting criteria.
	SortNReverse(int, def.SortBy)
	// SortNFrom sorts N tasks of the queue, starting from the given index, in non-decreasing order based on the sorting criteria.
	SortNFrom(int, int, def.SortBy) error
	// SortFromNReverse sorts N tasks of the queue, starting from the given index, in non-increasing order based on the sorting criteria.
	SortNFromReverse(int, int, def.SortBy) error
}

// PriorityTaskQueue is an abstraction for a task queue that maintains order of tasks
// based on their respective priorities.
// It does not allow random access/removal and iterating.
type PriorityTaskQueue interface {
	TaskQueue
	// EnqueueTaskItem adds the given task item to the queue.
	EnqueueTaskItem(*def.PQTaskItem)
	// EnqueueTaskItems adds the given task items to the queue.
	EnqueueTaskItems(...*def.PQTaskItem)
	// Pop and remove the task at the head of the priority queue.
	Pop() *def.PQTaskItem
	// Peek returns the top item in the queue without removing it.
	Peek() *def.PQTaskItem
	// Update the priority of the given task in the queue.
	Update(*def.PQTaskItem, int)
	// UpdateAll updates the priority of all the tasks in the queue
	// by applying the provided function.
	UpdateAll(func(def.PriorityTask))
	// WithPeriodicUpdates initiates the periodic update of the task priorities as
	// a cron job that runs on the given schedule. The priority of the task is updated
	// using the provided function.
	WithPeriodicUpdates(string, func(def.PriorityTask))
	// StartPeriodicUpdates starts the periodic updates of task priorities.
	// If the priority updater has not been setup, then this results in a no-op.
	StartPeriodicUpdates() error
	// StopPeriodicUpdates stops the periodic updates of task priorities.
	// If the priority updater has not been setup, then this results in a no-op.
	StopPeriodicUpdates()
}
