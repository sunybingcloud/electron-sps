package priority

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"bitbucket.org/sunybingcloud/electron/taskQueue/priority/testUtils"
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

var tasksToTest []def.Task
var pq taskQueue.PriorityTaskQueue

func createPriorityQueue() {
	inst1 := 10
	p1 := 1
	t1 := &def.DeadlineSensitiveTask{
		SimpleTask: def.SimpleTask{
			Name:      "t1",
			CPU:       1.0,
			RAM:       1024,
			Watts:     50.0,
			Image:     "t1-image",
			CMD:       "t1-command",
			Instances: &inst1,
		},
	}
	t1.SetPriority(p1)

	inst2 := 10
	p2 := 2
	t2 := &def.DeadlineSensitiveTask{
		SimpleTask: def.SimpleTask{
			Name:      "t2",
			CPU:       2.0,
			RAM:       2048,
			Watts:     50.0,
			Image:     "t2-image",
			CMD:       "t2-command",
			Instances: &inst2,
		},
	}
	t2.SetPriority(p2)

	tasksToTest = []def.Task{t1, t2}
	pq = NewPriorityQueue(tasksToTest).(taskQueue.PriorityTaskQueue)
}

func TestNewPriorityQueue(t *testing.T) {
	createPriorityQueue()
	assert.IsType(t, reflect.TypeOf(&Queue{}), reflect.TypeOf(pq))
	assert.False(t, pq.IsEmpty())
	assert.Equal(t, pq.Len(), 2)
}

func TestEmptyPriorityQueue(t *testing.T) {
	tasks := make([]def.Task, 0)
	pq = NewPriorityQueue(tasks).(taskQueue.PriorityTaskQueue)
	assert.IsType(t, reflect.TypeOf(&Queue{}), reflect.TypeOf(pq))
	assert.True(t, pq.IsEmpty())
	assert.Zero(t, pq.Len())
}

func TestPriorityQueue_EnqueueTask(t *testing.T) {
	createPriorityQueue()
	inst := 10
	p := 3
	t3 := &def.DeadlineSensitiveTask{
		SimpleTask: def.SimpleTask{
			Name:      "t3",
			CPU:       3.0,
			RAM:       3072,
			Watts:     50.0,
			Image:     "t3-image",
			CMD:       "t3-command",
			Instances: &inst,
		},
	}
	t3.SetPriority(p)
	testUtils.EnqueueTask(t, pq, t3)

	// Testing whether the priorities are ordered non-increasing order.
	testUtils.VerifyPriorityQueue(t, pq)
	// Testing whether task at position i has the expected priority.
	i := 0
	length := pq.Len()
	for !pq.IsEmpty() {
		pqItem := pq.Pop()
		assert.Equal(t, length-i, pqItem.Task.GetPriority())
		i++
	}
}

func TestPriorityQueue_EnqueueTasks(t *testing.T) {
	createPriorityQueue()
	inst1 := 10
	p1 := 3
	t1 := &def.DeadlineSensitiveTask{
		SimpleTask: def.SimpleTask{
			Name:      "t3",
			CPU:       3.0,
			RAM:       3072,
			Watts:     50.0,
			Image:     "t3-image",
			CMD:       "t3-command",
			Instances: &inst1,
		},
	}
	t1.SetPriority(p1)

	inst2 := 10
	p2 := 4
	t2 := &def.DeadlineSensitiveTask{
		SimpleTask: def.SimpleTask{
			Name:      "t4",
			CPU:       4.0,
			RAM:       4096,
			Watts:     50.0,
			Image:     "t4-image",
			CMD:       "t4-command",
			Instances: &inst2,
		},
	}
	t2.SetPriority(p2)
	testUtils.EnqueueTasks(t, pq, t1, t2)

	testUtils.VerifyPriorityQueue(t, pq)
	// Testing whether task at position i has the expected priority.
	i := 0
	length := pq.Len()
	for !pq.IsEmpty() {
		pqItem := pq.Pop()
		assert.Equal(t, length-i, pqItem.Task.GetPriority())
		i++
	}
}

func TestPriorityQueue_Peek(t *testing.T) {
	createPriorityQueue()
	taskItem := pq.Peek()
	assert.NotNil(t, taskItem)
	assert.Zero(t, taskItem.Index)
	assert.Equal(t, pq.Len(), taskItem.Task.GetPriority())
}

func TestPriorityQueue_Pop(t *testing.T) {
	createPriorityQueue()
	length := pq.Len()
	oldHead := pq.Pop()
	assert.NotNil(t, oldHead)
	assert.False(t, pq.IsEmpty())
	assert.Equal(t, length, oldHead.Task.GetPriority())
	newHead := pq.Peek()
	assert.NotNil(t, newHead)
	assert.Zero(t, newHead.Index)
	assert.Equal(t, pq.Len(), newHead.Task.GetPriority())
	assert.NotEqual(t, oldHead.Task.GetPriority(), newHead.Task.GetPriority())
	assert.NotEqual(t, oldHead.Task.GetName(), newHead.Task.GetName())
}

func TestPriorityQueue_Update(t *testing.T) {
	createPriorityQueue()
	inst3 := 10
	p3 := 3
	t3 := &def.DeadlineSensitiveTask{
		SimpleTask: def.SimpleTask{
			Name:      "t3",
			CPU:       3.0,
			RAM:       3072,
			Watts:     50.0,
			Image:     "t3-image",
			CMD:       "t3-command",
			Instances: &inst3,
		},
	}
	t3.SetPriority(p3)

	inst4 := 10
	p4 := 4
	t4 := &def.DeadlineSensitiveTask{
		SimpleTask: def.SimpleTask{
			Name:      "t4",
			CPU:       4.0,
			RAM:       4096,
			Watts:     50.0,
			Image:     "t4-image",
			CMD:       "t4-command",
			Instances: &inst4,
		},
	}
	t4.SetPriority(p4)
	testUtils.EnqueueTasks(t, pq, t3, t4)
	// Currently, tasks ordered by priority (non-inc) = {t4, t3, t2, t1}

	// Changing the priority of t4 to 2.
	// t3 should now be the new head of the priority queue.
	// Tasks ordered by priority (non-inc) should be = {t3, t4, t2, t1} OR {t3, t2, t4, t1}
	pq.Update(pq.Peek(), 2)
	// Testing whether the priority ordering is maintained.
	testUtils.VerifyPriorityQueue(t, pq)
	updatedPriorities := []int{3, 2, 2, 1}
	i := 0
	for !pq.IsEmpty() {
		taskItem := pq.Pop()
		assert.Equal(t, updatedPriorities[i], taskItem.Task.GetPriority())
		i++
		if (i == 1) || (i == 2) {
			secondTaskName := pq.Peek().Task.GetName()
			assert.True(t, (secondTaskName == "t2") || (secondTaskName == "t4"))
		}
	}
}

func TestPriorityQueue_UpdateAll(t *testing.T) {
	createPriorityQueue()
	inst3 := 10
	p3 := 3
	t3 := &def.DeadlineSensitiveTask{
		SimpleTask: def.SimpleTask{
			Name:      "t3",
			CPU:       3.0,
			RAM:       3072,
			Watts:     50.0,
			Image:     "t3-image",
			CMD:       "t3-command",
			Instances: &inst3,
		},
	}
	t3.SetPriority(p3)

	inst4 := 10
	p4 := 4
	t4 := &def.DeadlineSensitiveTask{
		SimpleTask: def.SimpleTask{
			Name:      "t4",
			CPU:       4.0,
			RAM:       4096,
			Watts:     50.0,
			Image:     "t4-image",
			CMD:       "t4-command",
			Instances: &inst4,
		},
	}
	t4.SetPriority(p4)
	testUtils.EnqueueTasks(t, pq, t3, t4)

	// Currently, tasks ordered by priority (non-inc) = {t4, t3, t2, t1}
	// Reversing the priorities of all tasks by updating them to (length - currentPriority + 1).
	length := pq.Len()
	pq.UpdateAll(func(task def.PriorityTask) {
		curPriority := task.GetPriority()
		task.SetPriority(length - curPriority + 1)
	})

	// First verifying whether priority ordered has been maintained.
	testUtils.VerifyPriorityQueue(t, pq)

	// The new order of tasks based on their updated priorities = {t1, t2, t3, t4}
	priorities := []int{4, 3, 2, 1}
	updatedTaskNames := []string{"t1", "t2", "t3", "t4"}
	i := 0
	for !pq.IsEmpty() {
		taskItem := pq.Pop()
		assert.Equal(t, priorities[i], taskItem.Task.GetPriority())
		assert.Equal(t, updatedTaskNames[i], taskItem.Task.GetName())
		i++
	}
}
