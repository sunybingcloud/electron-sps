package priority

import "bitbucket.org/sunybingcloud/electron/def"

// tasks implements heap.Interface.
// Cite - https://github.com/mailgun/holster/blob/master/priority_queue.go.
type tasks []*def.PQTaskItem

func (t tasks) Len() int {
	return len(t)
}

func (t tasks) Swap(i, j int) {
	// Swapping tasks at indices i and j.
	// Also, updating the indices to keep them consistent.
	t[i], t[j] = t[j], t[i]
	t[i].Index = i
	t[j].Index = j
}

// Less returns whether the preference between tasks at position i and j based on the priority.
// The tasks are ordered to form a max-heap.
func (t tasks) Less(i, j int) bool {
	return t[i].Task.GetPriority() > t[j].Task.GetPriority()
}

// Push the new item onto the heap.
// Assuming that the priority for the new item has already been set.
func (t *tasks) Push(item interface{}) {
	// Adding the task to the end of the queue.
	tItem := item.(*def.PQTaskItem)
	tItem.Index = t.Len()
	*t = append(*t, tItem)
}

// Removing and returning the task with the least priority from the queue.
func (t *tasks) Pop() interface{} {
	n := t.Len()
	task := (*t)[n-1]
	(*t)[n-1] = nil // preventing memory leaks.
	task.Index = -1 // invalidating the index.
	*t = (*t)[:n-1]
	return task
}

// INCOMPLETE...
func (t tasks) String() string {
	return ""
}
