package testUtils

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"github.com/stretchr/testify/assert"
	"testing"
)

func EnqueueTask(t *testing.T, q taskQueue.PriorityTaskQueue, task def.Task) {
	origLen := q.Len()
	q.EnqueueTask(task)
	assert.False(t, q.IsEmpty())
	assert.Equal(t, q.Len(), origLen+1)
}

// EnqueueTasks adds given task to the given task queue and tests.
// The task queue is then tested to make sure that enqueue was successful.
func EnqueueTasks(t *testing.T, q taskQueue.PriorityTaskQueue, tasks ...def.Task) {
	origLen := q.Len()
	q.EnqueueTasks(tasks...)
	assert.False(t, q.IsEmpty())
	assert.Equal(t, q.Len(), origLen+len(tasks))
}

func VerifyPriorityQueue(t *testing.T, q taskQueue.PriorityTaskQueue) {
	if !q.IsEmpty() {
		var poppedItems []*def.PQTaskItem
		firstTask := q.Pop()
		prevPriority := firstTask.Task.GetPriority()
		poppedItems = append(poppedItems, firstTask)
		for !q.IsEmpty() {
			nextTaskItem := q.Pop()
			nextPriority := nextTaskItem.Task.GetPriority()
			assert.Greater(t, prevPriority, nextPriority, "incorrect ordering of tasks based on priority")
			poppedItems = append(poppedItems, nextTaskItem)
		}

		// Adding all the tasks back.
		q.EnqueueTaskItems(poppedItems...)
	}
}
