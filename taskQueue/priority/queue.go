package priority

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"container/heap"
	"github.com/pkg/errors"
	"sync"
)

// Queue is a priority queue container for tasks.
// Cite - https://github.com/mailgun/holster/blob/master/priority_queue.go.
type Queue struct {
	pendingTasks *tasks
	// priority updater.
	ptyUpdater *TaskPriorityUpdater
	// periodicUpdateSchedule is the cron schedule on which the priority updater should be run.
	periodicUpdateSchedule string
	sync.Mutex
}

func NewPriorityQueue(ts []def.Task) taskQueue.TaskQueue {
	pTasks := &tasks{}
	for i, t := range ts {
		// Assigning the same priority to all the tasks in the beginning.
		*pTasks = append(*pTasks, &def.PQTaskItem{
			Task:  t.(def.PriorityTask),
			Index: i,
		})
	}

	heap.Init(pTasks)
	return &Queue{pendingTasks: pTasks}
}

func (q *Queue) EnqueueTask(task def.Task) {
	heap.Push(q.pendingTasks, &def.PQTaskItem{
		Task: task.(def.PriorityTask),
	})
}

func (q *Queue) EnqueueTasks(tasks ...def.Task) {
	for _, t := range tasks {
		heap.Push(q.pendingTasks, &def.PQTaskItem{
			Task: t.(def.PriorityTask),
		})
	}
}

func (q *Queue) EnqueueTaskItem(taskItem *def.PQTaskItem) {
	heap.Push(q.pendingTasks, taskItem)
}

func (q *Queue) EnqueueTaskItems(taskItems ...*def.PQTaskItem) {
	for _, item := range taskItems {
		heap.Push(q.pendingTasks, item)
	}
}

// Pop removes the head of the queue and returns the item.
func (q *Queue) Pop() *def.PQTaskItem {
	taskItem := heap.Pop(q.pendingTasks)
	return taskItem.(*def.PQTaskItem)
}

// Peek returns the top item in the queue without removing it.
func (q *Queue) Peek() *def.PQTaskItem {
	if q.IsEmpty() {
		return nil
	}
	return (*q.pendingTasks)[0]
}

// Update the priority of an item that currently exists in the queue with the new
// priority provided.
func (q *Queue) Update(taskItem *def.PQTaskItem, newPriority int) {
	// Safety check.
	if !q.IsEmpty() {
		heap.Remove(q.pendingTasks, taskItem.Index)
		taskItem.Task.SetPriority(newPriority)
		heap.Push(q.pendingTasks, taskItem)
	}
}

// UpdateAll updates the priority of all the tasks in the queue by
// applying the function provided. As we cannot just poll the queue, update, and
// add the task back, we first empty queue and then add back all the tasks with
// their respective priorities updated.
func (q *Queue) UpdateAll(updateFunc func(def.PriorityTask)) {
	var updatedItems []*def.PQTaskItem
	for !q.IsEmpty() {
		item := heap.Pop(q.pendingTasks).(*def.PQTaskItem)
		updateFunc(item.Task)
		updatedItems = append(updatedItems, item)
	}

	for _, item := range updatedItems {
		heap.Push(q.pendingTasks, item)
	}
}

// Len returns the length of the task queue.
func (q *Queue) Len() int {
	return q.pendingTasks.Len()
}

// IsEmpty returns whether the priority queue is empty.
func (q *Queue) IsEmpty() bool {
	return q.pendingTasks.Len() == 0
}

// ITERATION NOT SUPPORTED.
func (q Queue) BeginIter() taskQueue.Iterator {
	return nil
}

// ITERATION NOT SUPPORTED.
func (q Queue) BeginReverseIter() taskQueue.ReverseIterator {
	return nil
}

// GetTasks returns the tasks in the priorities as a slice.
func (q *Queue) GetTasks() []def.Task {
	var tasks []def.Task
	for _, t := range *q.pendingTasks {
		tasks = append(tasks, t.Task)
	}
	return tasks
}

// WithPeriodicUpdates sets periodic updating of task priorities to be run on the given schedule.
func (q *Queue) WithPeriodicUpdates(
	schedule string,
	updateFunc func(def.PriorityTask)) {

	q.periodicUpdateSchedule = schedule
	q.ptyUpdater = NewTaskPriorityUpdater(updateFunc, q)
	return
}

// StartPeriodicUpdates starts the periodic updating of task priorities.
func (q *Queue) StartPeriodicUpdates() error {
	if q.ptyUpdater != nil {
		err := q.ptyUpdater.Start(q.periodicUpdateSchedule)
		if err != nil {
			return errors.Wrap(err, "failed to start periodic task priority updater")
		}
	}
	return nil
}

// StopPeriodicUpdates stops the periodic updating of task priorities.
func (q *Queue) StopPeriodicUpdates() {
	if q.ptyUpdater != nil {
		q.ptyUpdater.Stop()
	}
}

// RANDOM ACCESS NOT SUPPORTED.
func (q Queue) At(int) def.Task {
	return nil
}

// RANDOM REMOVAL NOT SUPPORTED.
func (q Queue) RemoveAt(int) {}

// String returns the string representation of the priority queue.
func (q Queue) String() string {
	return q.pendingTasks.String()
}
