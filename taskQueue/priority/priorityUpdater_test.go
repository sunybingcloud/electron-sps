package priority

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"bitbucket.org/sunybingcloud/electron/taskQueue/priority/testUtils"
	"github.com/stretchr/testify/assert"
	"strconv"
	"sync/atomic"
	"testing"
)

var priorityQ taskQueue.PriorityTaskQueue

const ptyUpdateSchedule = "0/5 * * * * *" // updating priorities every 5 seconds.

const queueLength = 10

var ptyUpdateCount = int64(0)

func createTaskPriorityUpdater(t *testing.T) {

	var tasks []def.Task
	for i := 1; i <= queueLength; i++ {
		inst := i
		pty := i
		taskName := "t" + strconv.FormatInt(int64(i), 32)
		task := &def.DeadlineSensitiveTask{
			SimpleTask: def.SimpleTask{
				Name:      taskName,
				CPU:       1.0,
				RAM:       1024,
				Watts:     50.0,
				Image:     taskName + "-image",
				CMD:       taskName + "-command",
				Instances: &inst,
			},
			SubmissionToDeadlineSeconds: &i,
		}
		task.SetPriority(pty)
		tasks = append(tasks, task)
	}
	priorityQ = NewPriorityQueue(tasks).(taskQueue.PriorityTaskQueue)
	testUtils.VerifyPriorityQueue(t, priorityQ)
	priorityQ.WithPeriodicUpdates(ptyUpdateSchedule, getPriorityInverterFunc())
	return
}

func getPriorityInverterFunc() func(def.PriorityTask) {
	return func(task def.PriorityTask) {
		curPty := task.GetPriority()
		task.SetPriority((queueLength - curPty) + 1)
		atomic.AddInt64(&ptyUpdateCount, 1)
	}
}

func verifyUpdatedPriorityQueue(timesUpdated int) {
	var poppedItems []*def.PQTaskItem
	iter := 1
	var expectedTask def.PriorityTask
	var expectedTaskPty int
	for !priorityQ.IsEmpty() {
		expectedTaskName := "t" + strconv.FormatInt(int64(iter), 32)
		i := iter
		if (timesUpdated % 2) == 0 {
			expectedTaskPty = iter
		} else {
			expectedTaskPty = (queueLength - iter) + 1
		}

		expectedTask = &def.DeadlineSensitiveTask{
			SimpleTask: def.SimpleTask{
				Name:      expectedTaskName,
				CPU:       1.0,
				RAM:       1024,
				Watts:     50.0,
				Image:     expectedTaskName + "-image",
				CMD:       expectedTaskName + "-command",
				Instances: &i,
			},
			SubmissionToDeadlineSeconds: &i,
		}
		expectedTask.SetPriority(expectedTaskPty)

		item := priorityQ.Pop()
		assert.ObjectsAreEqualValues(item.Task, expectedTask)
		poppedItems = append(poppedItems, item)
		iter++
	}

	// Adding all the items back.
	priorityQ.EnqueueTaskItems(poppedItems...)
}

func TestTaskPriorityUpdater_CronUpdate(t *testing.T) {
	createTaskPriorityUpdater(t)
	// Allowing the priority updater to invert the task priorities 5 times.
	// We need to permit the priority updater each time.
	assert.NoError(t, priorityQ.StartPeriodicUpdates())

	successCount := 0
	iter := 0
	length := priorityQ.Len()
	// Permitting priorities of tasks to be updated.
	for iter < 5 {
		for atomic.LoadInt64(&ptyUpdateCount) < int64(length) {
		}
		priorityQ.Lock()
		iter++
		verifyUpdatedPriorityQueue(iter)
		// If here, then priority queue verification passed.
		successCount++
		atomic.StoreInt64(&ptyUpdateCount, 0) // restarting.
		priorityQ.Unlock()
	}
	priorityQ.StopPeriodicUpdates() // Stopping the task priority updater.
	assert.Equal(t, iter, successCount)
}
