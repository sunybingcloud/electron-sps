package priority

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"bitbucket.org/sunybingcloud/electron/utilities"
	"github.com/pkg/errors"
	"github.com/robfig/cron/v3"
	"sync"
)

// TaskPriorityUpdater updates the priority of the tasks in a task queue
// in regular intervals based on a cron schedule.
// TaskPriorityUpdater is a cron job runner and also implements cron.Job.
type TaskPriorityUpdater struct {
	// updateFunc is a function that updates the priority of the given PriorityTask.
	updateFunc func(def.PriorityTask)
	// taskQ is the task queue containing the PriorityTasks.
	taskQ taskQueue.PriorityTaskQueue

	// haltCh is used to get signalled to stop. If haltCh is closed, then
	// priority updater also stops executing and the cron job is stopped.
	haltCh *utilities.SignalChannel

	wg sync.WaitGroup
	// jobRunner is a cron job runner.
	jobRunner *cron.Cron
}

// NewTaskPriorityUpdater returns a priority updater that periodically
// updates the priorities of the tasks in the given queue.
//
// To avoid an update to ordering of tasks in the queue, while the scheduler
// is processing the queue, the priority updater operates by first acquiring the lock on the
// task queue. In addition, once the task priorities have been updated and the queue has been
// re-ordered, the lock is released.
func NewTaskPriorityUpdater(
	f func(def.PriorityTask),
	q taskQueue.PriorityTaskQueue) *TaskPriorityUpdater {

	return &TaskPriorityUpdater{
		updateFunc: f,
		taskQ:      q,
		haltCh:     utilities.NewSignalChannel(),
		jobRunner:  cron.New(cron.WithSeconds()),
	}
}

// Run the priority updater which updates the priorities of all tasks using the given function.
func (u *TaskPriorityUpdater) Run() {
	if u.haltCh.IsClosed() {
		u.wg.Done()
		return
	}
	u.taskQ.Lock()
	defer u.taskQ.Unlock()
	u.taskQ.UpdateAll(u.updateFunc)
}

// Start the priority updater to run on the given schedule.
func (u *TaskPriorityUpdater) Start(schedule string) error {
	_, err := u.jobRunner.AddJob(schedule, u)
	if err != nil {
		return errors.Wrap(err, "failed to add TaskPriorityUpdater cron job")
	}
	u.wg.Add(1)
	u.jobRunner.Start()
	return nil
}

// Stop the priority updater.
func (u *TaskPriorityUpdater) Stop() {
	u.haltCh.Close()
	u.wg.Wait()
	u.jobRunner.Stop()
}
