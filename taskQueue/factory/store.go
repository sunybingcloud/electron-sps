package factory

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"bitbucket.org/sunybingcloud/electron/taskQueue/fcfs"
	"bitbucket.org/sunybingcloud/electron/taskQueue/priority"
	"github.com/pkg/errors"
)

const (
	FCFS_Q          = "fcfs"
	FCFS_SORTABLE_Q = "fcfs-sortable"
	PRIORITY_Q      = "priority"
)

// taskQueues maps task queue types to their constructors.
var taskQueues = map[string]func([]def.Task) taskQueue.TaskQueue{
	FCFS_Q:          fcfs.NewFCFSQueue,
	FCFS_SORTABLE_Q: fcfs.NewFCFSSortableTaskQueue,
	PRIORITY_Q:      priority.NewPriorityQueue,
}

// GetTaskQueue returns the task queue of the given type with the given tasks
// constituting the internal queue.
func GetTaskQueue(queueType string, tasks []def.Task) (taskQueue.TaskQueue, error) {
	if constructor, ok := taskQueues[queueType]; !ok {
		return nil, errors.New("invalid task queue name provided")
	} else {
		return constructor(tasks), nil
	}
}
