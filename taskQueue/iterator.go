package taskQueue

import "bitbucket.org/sunybingcloud/electron/def"

type Iterator interface {
	// NextTask returns the next task pending allocation.
	NextTask() def.Task
	// HasNext returns whether there is another task in the queue.
	HasNext() bool
	// RemoveTraversedTask removes the task that was previously traversed
	// by the iterator.
	RemoveTraversedTask()
}

type ReverseIterator interface {
	Iterator
}
