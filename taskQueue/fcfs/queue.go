package fcfs

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"fmt"
	"sync"
)

// Queue represents a First-Come-First-Serve Task Queue.
type Queue struct {
	// tasks pending allocation.
	tasks []def.Task
	sync.Mutex
}

// NewFCFSQueue returns a Queue that contains the given tasks.
func NewFCFSQueue(tasks []def.Task) taskQueue.TaskQueue {
	q := &Queue{
		tasks: make([]def.Task, len(tasks)),
	}
	for i, t := range tasks {
		q.tasks[i] = t
	}
	return q
}

// EnqueueTask adds the given task to the back of the queue.
func (q *Queue) EnqueueTask(task def.Task) {
	q.tasks = append(q.tasks, task)
}

// EnqueueTasks adds the given tasks, in the given order, to the back of the queue.
func (q *Queue) EnqueueTasks(tasks ...def.Task) {
	for _, t := range tasks {
		q.EnqueueTask(t)
	}
}

// BeginIter returns an iterator for Queue.
func (q *Queue) BeginIter() taskQueue.Iterator {
	return &iterator{
		pos: 0,
		q:   q,
	}
}

// BeginReverseIter returns a reverse iterator for the queue.
func (q *Queue) BeginReverseIter() taskQueue.ReverseIterator {
	return &reverseIterator{
		pos: q.Len() - 1,
		q:   q,
	}
}

// Len returns the length of the queue.
func (q *Queue) Len() int {
	return len(q.tasks)
}

// GetTasks returns the internally stored task array.
func (q Queue) GetTasks() []def.Task {
	return q.tasks
}

// At returns a pointer to the task present at the given index.
// Returns nil if invalid index is provided.
func (q *Queue) At(index int) def.Task {
	if q.IsEmpty() || (index < 0) || (index >= q.Len()) {
		return nil
	}
	return q.tasks[index]
}

// RemoveAt removes the task present at the given index.
func (q *Queue) RemoveAt(index int) {
	if !q.IsEmpty() && (index >= 0) && (index < q.Len()) {
		q.tasks = append(q.tasks[:index], q.tasks[index+1:]...)
	}
}

// IsEmpty returns whether the queue is empty.
func (q *Queue) IsEmpty() bool {
	return q.Len() == 0
}

// String returns the string representation of the tasks in the queue.
func (q *Queue) String() string {
	return fmt.Sprintf("%v", q.tasks)
}
