package testUtils

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"github.com/stretchr/testify/assert"
	"testing"
)

func EnqueueTask(t *testing.T, q taskQueue.TaskQueue, task def.Task) {
	origLen := q.Len()
	q.EnqueueTask(task)
	assert.False(t, q.IsEmpty())
	assert.Equal(t, q.Len(), origLen+1)
}

// EnqueueTasks adds given task to the given task queue and tests.
// The task queue is then tested to make sure that enqueue was successful.
func EnqueueTasks(t *testing.T, q taskQueue.TaskQueue, tasks ...def.Task) {
	origLen := q.Len()
	q.EnqueueTasks(tasks...)
	assert.False(t, q.IsEmpty())
	assert.Equal(t, q.Len(), origLen+len(tasks))
}
