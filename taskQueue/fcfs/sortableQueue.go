package fcfs

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"errors"
	"math"
	"sort"
)

// SortableQueue is a First-Come-First-Serve Task Queue that allows
// explicit reordering of tasks.
type SortableQueue struct {
	queue *Queue
}

// NewFCFSSortableTaskQueue returns a sortable first-come-first-serve queue.
func NewFCFSSortableTaskQueue(tasks []def.Task) taskQueue.TaskQueue {
	q := &SortableQueue{queue: NewFCFSQueue(tasks).(*Queue)}
	return q
}

// Sort the tasks in the queue based on the given criteria.
func (q *SortableQueue) Sort(by def.SortBy) {
	q.sort(q.queue.tasks, by)
}

// SortReverse sorts the queue in non-increasing order based on the given criteria.
func (q *SortableQueue) SortReverse(by def.SortBy) {
	q.sortReverse(q.queue.tasks, by)
}

// Sort the first N tasks in the queue based on the given criteria.
func (q *SortableQueue) SortN(n int, by def.SortBy) {
	q.sort(q.queue.tasks[:n], by)
}

// SortNReverse sorts the first N tasks in the reverse order based on the given criteria.
func (q *SortableQueue) SortNReverse(n int, by def.SortBy) {
	q.sortReverse(q.queue.tasks[:n], by)
}

// SortNFrom sorts tasks in a given range(start, start+count).
// Value of count should always > 0, and the starting index should be within bounds.
// The tasks sorted would be in the boundary specified by [max(start, 0), min((max(start, 0))+count, q.len() - 1)].
func (q *SortableQueue) SortNFrom(start int, count int, by def.SortBy) error {
	q.queue.Lock()
	defer q.queue.Unlock()
	if (count <= 0) || (start >= (q.queue.Len() - 1)) {
		return errors.New("size of the subsequence to be sorted cannot be less than 1")
	}
	initPos := int(math.Max(float64(start), float64(0)))
	endPos := int(math.Min(float64(q.queue.Len()), float64(initPos+count)))
	q.sort(q.queue.tasks[initPos:endPos], by)
	return nil
}

// SortNFromReverse sorts tasks in a given range(start, start+count).
// Value of count should always > 0, and the starting index should be within bounds.
// The tasks sorted would be in the boundary specified by [max(start, 0), min((max(start, 0))+count, q.len() - 1)].
func (q *SortableQueue) SortNFromReverse(start int, count int, by def.SortBy) error {
	q.queue.Lock()
	defer q.queue.Unlock()
	if (count <= 0) || (start >= q.queue.Len()) {
		return errors.New("size of the subsequence to be sorted cannot be less than 1")
	}
	initPos := int(math.Max(float64(start), float64(0)))
	endPos := int(math.Min(float64(q.queue.Len()), float64(initPos+count)))
	q.sortReverse(q.queue.tasks[initPos:endPos], by)
	return nil
}

func (q *SortableQueue) sort(tasks []def.Task, by def.SortBy) {
	sort.SliceStable(tasks, func(i, j int) bool {
		return by(tasks[i]) <= by(tasks[j])
	})
}

func (q *SortableQueue) sortReverse(tasks []def.Task, by def.SortBy) {
	sort.SliceStable(tasks, func(i, j int) bool {
		return by(tasks[i]) >= by(tasks[j])
	})
}

// EnqueueTask adds the given task to the back of the queue.
func (q *SortableQueue) EnqueueTask(task def.Task) {
	q.queue.EnqueueTask(task)
}

// EnqueueTask adds the given tasks to the back of the queue.
func (q *SortableQueue) EnqueueTasks(tasks ...def.Task) {
	q.queue.EnqueueTasks(tasks...)
}

// BeginIter returns an iterator for the queue.
func (q *SortableQueue) BeginIter() taskQueue.Iterator {
	return q.queue.BeginIter()
}

// BeginReverseIter returns a reverse iterator for the queue.
func (q *SortableQueue) BeginReverseIter() taskQueue.ReverseIterator {
	return q.queue.BeginReverseIter()
}

// Len returns the Length of the queue.
func (q SortableQueue) Len() int {
	return q.queue.Len()
}

// GetTasks returns the internally stored task array.
func (q SortableQueue) GetTasks() []def.Task {
	return q.queue.tasks
}

// At returns a pointer to the task present at the given index.
// Returns nil if invalid index is provided.
func (q SortableQueue) At(index int) def.Task {
	if q.queue.IsEmpty() || (index < 0) || (index >= q.queue.Len()) {
		return nil
	}
	return q.queue.tasks[index]
}

// RemoveAt removes the task present at the given index.
func (q *SortableQueue) RemoveAt(index int) {
	if !q.queue.IsEmpty() && (index >= 0) && (index < q.queue.Len()) {
		q.queue.tasks = append(q.queue.tasks[:index], q.queue.tasks[index+1:]...)
	}
}

// IsEmpty returns whether the queue is empty.
func (q SortableQueue) IsEmpty() bool {
	return q.queue.IsEmpty()
}

// String returns the string representation of the queue.
func (q SortableQueue) String() string {
	return q.String()
}

func (q *SortableQueue) Lock() {
	q.queue.Lock()
}

func (q *SortableQueue) Unlock() {
	q.queue.Unlock()
}
