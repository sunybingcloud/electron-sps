package fcfs

import "bitbucket.org/sunybingcloud/electron/def"

// reverseIterator represents an iterator that can an FCFS task queue in the reverse order.
type reverseIterator struct {
	// pos of the iterator in the queue
	pos int
	// The task queue to iterate.
	q *Queue
}

// NextTask returns the next task pending allocation.
// Each time NextTask() is called, iterator is advanced (advanced => move one step back).
// Returns nil if either the queue is empty or all tasks have been traversed.
func (iter *reverseIterator) NextTask() def.Task {
	if iter.q.IsEmpty() {
		return nil
	}

	if !iter.isIterWithinBounds() {
		iter.pos = -1 // for safety.
		return nil
	}

	task := iter.q.tasks[iter.pos]
	iter.pos--
	return task
}

// HasNext returns whether there is another that the iterator is yet to traverse.
func (iter reverseIterator) HasNext() bool {
	return !iter.q.IsEmpty() && (iter.isIterWithinBounds())
}

// RemoveTraversedTask removes the task that was previously traversed by the iterator.
// If NextTask() was never called (iterator has not been advanced), then this would remove
// the last task.
func (iter *reverseIterator) RemoveTraversedTask() {
	if !iter.q.IsEmpty() {
		// If valid iterator.
		if iter.isIterWithinBounds() || (iter.pos == -1) {
			if iter.pos == (iter.q.Len() - 1) {
				iter.q.tasks = iter.q.tasks[:iter.pos]
			} else {
				iter.q.tasks = append(iter.q.tasks[:iter.pos+1], iter.q.tasks[iter.pos+2:]...)
			}
		}
		iter.fixIter()
	}
}

// fixIter repositions the iterator to prevent out of bounds when removing tasks.
func (iter *reverseIterator) fixIter() {
	if !iter.isIterWithinBounds() {
		if iter.pos < 0 {
			iter.pos++
		} else {
			iter.pos--
		}
	}
}

// isIterWithinBounds returns whether the iterator is within bounds.
func (iter reverseIterator) isIterWithinBounds() bool {
	return (iter.pos >= 0) && (iter.pos < iter.q.Len())
}
