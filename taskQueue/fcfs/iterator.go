package fcfs

import "bitbucket.org/sunybingcloud/electron/def"

// iterator represents an iterator that can traverse an FCFS task queue.
type iterator struct {
	// pos of the iterator in the queue.
	pos int
	// The task queue to iterate.
	q *Queue
}

// NextTask returns the next task pending allocation.
// Each time NextTask() is called, iterator is advanced.
// Returns nil if either the queue is empty or all tasks have been traversed.
func (iter *iterator) NextTask() def.Task {
	if iter.q.IsEmpty() {
		return nil
	}

	if !iter.isIterWithinBounds() {
		iter.pos = -1 // for safety.
		return nil
	}

	task := iter.q.tasks[iter.pos]
	iter.pos++
	return task
}

// HasNext return whether there is another task that the iterator is yet to traverse.
func (iter iterator) HasNext() bool {
	return !iter.q.IsEmpty() && (iter.isIterWithinBounds())
}

// RemoveTraversedTask removes the task that was previously traversed by the iterator.
// If NextTask() was never called (iterator has not been advanced), then this would remove
// the first task.
func (iter *iterator) RemoveTraversedTask() {
	if !iter.q.IsEmpty() {
		// If valid iterator.
		if iter.isIterWithinBounds() || (iter.pos == iter.q.Len()) {
			if iter.pos == 0 {
				iter.q.tasks = iter.q.tasks[1:]
			} else {
				iter.q.tasks = append(iter.q.tasks[:iter.pos-1], iter.q.tasks[iter.pos:]...)
			}
		}
		// If the iterator has gone out of bounds, fixing it to be within bounds.
		// Example, if RemoveTraversedTask() is called when pos=0, then pos would now be -1.
		// Similarly, if RemoveTraversedTask is called after HasNext() returns false, pos = q.Len().
		iter.fixIter()
	}
}

// fixIter repositions the iterator to prevent out of bounds access.
func (iter *iterator) fixIter() {
	if !iter.isIterWithinBounds() {
		if iter.pos >= iter.q.Len() {
			// If the last task was removed, moving the iterator to point to one
			// step beyond the current last task.
			// If last but one task was removed, moving the iterator to point to
			// the last task.
			iter.pos--
		}
	} else {
		iter.pos--
		if iter.pos == -1 {
			// Setting to 0 is fine as the queue is not empty.
			iter.pos = 0
		}
	}
}

// isIterWithinBounds checks whether the iterator is within bounds.
func (iter iterator) isIterWithinBounds() bool {
	return (iter.pos >= 0) && (iter.pos < iter.q.Len())
}
