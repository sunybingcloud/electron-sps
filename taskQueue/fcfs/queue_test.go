package fcfs

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"bitbucket.org/sunybingcloud/electron/taskQueue/fcfs/testUtils"
	"github.com/stretchr/testify/assert"
	"reflect"
	"strconv"
	"testing"
)

var tasks []def.Task
var fcfsQueue taskQueue.TaskQueue

func createFCFSQueue() {
	inst1 := 10
	inst2 := 10
	tasks = []def.Task{
		&def.SimpleTask{
			Name:      "t1",
			CPU:       1.0,
			RAM:       1024,
			Watts:     50.0,
			Image:     "t1-image",
			CMD:       "t1-command",
			Instances: &inst1,
		},
		&def.SimpleTask{
			Name:      "t2",
			CPU:       2.0,
			RAM:       2048,
			Watts:     50.0,
			Image:     "t2-image",
			CMD:       "t2-command",
			Instances: &inst2,
		},
	}
	fcfsQueue = NewFCFSQueue(tasks)
}

func TestNewFCFSTaskQueue(t *testing.T) {
	createFCFSQueue()
	assert.IsType(t, reflect.TypeOf(&Queue{}), reflect.TypeOf(fcfsQueue))
	assert.False(t, fcfsQueue.IsEmpty())
	assert.Equal(t, fcfsQueue.Len(), 2)
}

func TestFCFSTaskQueue_EnqueueTask(t *testing.T) {
	createFCFSQueue()
	inst := 10
	testUtils.EnqueueTask(t, fcfsQueue, &def.SimpleTask{
		Name:      "t3",
		CPU:       3.0,
		RAM:       3072,
		Watts:     50.0,
		Image:     "t3-image",
		CMD:       "t3-command",
		Instances: &inst,
	})
}

func TestFCFSTaskQueue_EnqueueTasks(t *testing.T) {
	createFCFSQueue()
	inst1 := 10
	inst2 := 10
	testUtils.EnqueueTasks(t, fcfsQueue,
		&def.SimpleTask{
			Name:      "t3",
			CPU:       3.0,
			RAM:       3072,
			Watts:     50.0,
			Image:     "t3-image",
			CMD:       "t3-command",
			Instances: &inst1,
		},
		&def.SimpleTask{
			Name:      "t4",
			CPU:       4.0,
			RAM:       4096,
			Watts:     50.0,
			Image:     "t4-image",
			CMD:       "t4-command",
			Instances: &inst2,
		})
}

func TestFCFSTaskQueue_Iteration(t *testing.T) {
	createFCFSQueue()
	inst1 := 10
	inst2 := 10
	testUtils.EnqueueTasks(t, fcfsQueue,
		&def.SimpleTask{
			Name:      "t3",
			CPU:       3.0,
			RAM:       3072,
			Watts:     50.0,
			Image:     "t3-image",
			CMD:       "t3-command",
			Instances: &inst1,
		},
		&def.SimpleTask{
			Name:      "t4",
			CPU:       4.0,
			RAM:       4096,
			Watts:     50.0,
			Image:     "t4-image",
			CMD:       "t4-command",
			Instances: &inst2,
		})

	var iter taskQueue.Iterator
	var task def.Task
	t.Run("HasNext() and NextTask()", func(t *testing.T) {
		// Testing HasNext.
		iter = fcfsQueue.BeginIter()
		for i := 0; i < fcfsQueue.Len(); i++ {
			assert.True(t, iter.HasNext())
			iter.NextTask() // advancing forward.
		}
		// Reached the end of the queue.
		// HasNext() should return false.
		assert.False(t, iter.HasNext())
		// NextTask() should return nil.
		assert.Nil(t, iter.NextTask())

		// Checking whether the tasks are in order and the task definition is accurate.
		count := 0
		for iter = fcfsQueue.BeginIter(); iter.HasNext(); {
			task = iter.NextTask()
			if task != nil {
				expectedTaskName := "t" + strconv.FormatInt(int64(count+1), 10)
				assert.Equal(t, expectedTaskName, task.GetName())
				expectedCPU := float64(count + 1)
				assert.Equal(t, expectedCPU, task.GetCpu())
				expectedRAM := 1024 * float64(count+1)
				assert.Equal(t, expectedRAM, task.GetRam())
				assert.Equal(t, 50.0, task.GetWatts())
				expectedImage := expectedTaskName + "-image"
				assert.Equal(t, expectedImage, task.GetImage())
				expectedCMD := expectedTaskName + "-command"
				assert.Equal(t, expectedCMD, task.GetCmd())
				assert.Equal(t, 10, task.GetInstances())
				count++
			}
		}

	})

	t.Run("update of instance count", func(t *testing.T) {
		// Reducing instance count for all tasks by 1.
		for iter = fcfsQueue.BeginIter(); iter.HasNext(); {
			task = iter.NextTask()
			if task != nil {
				task.DecrementInstances()
			}
		}

		// Checking instance count for all tasks to be 1 less than the previous value.
		for iter = fcfsQueue.BeginIter(); iter.HasNext(); {
			task = iter.NextTask()
			if task != nil {
				assert.Equal(t, 9, task.GetInstances())
			}
		}
	})

	t.Run("removal of tasks using iterator", func(t *testing.T) {
		var iter taskQueue.Iterator
		t.Run("remove tasks at the front of the queue", func(t *testing.T) {
			for iter = fcfsQueue.BeginIter(); iter.HasNext(); {
				origLen := fcfsQueue.Len()
				iter.RemoveTraversedTask()
				// Queue length should now also reflect the removal.
				assert.Equal(t, fcfsQueue.Len(), origLen-1)
			}
			assert.True(t, fcfsQueue.IsEmpty())
		})

		t.Run("remove tasks at the back of the queue", func(t *testing.T) {
			// Recreating the queue.
			createFCFSQueue()
			inst1 := 10
			inst2 := 10
			testUtils.EnqueueTasks(t, fcfsQueue,
				&def.SimpleTask{
					Name:      "t3",
					CPU:       3.0,
					RAM:       3072,
					Watts:     50.0,
					Image:     "t3-image",
					CMD:       "t3-command",
					Instances: &inst1,
				},
				&def.SimpleTask{
					Name:      "t4",
					CPU:       4.0,
					RAM:       4096,
					Watts:     50.0,
					Image:     "t4-image",
					CMD:       "t4-command",
					Instances: &inst2,
				})

			for iter = fcfsQueue.BeginIter(); iter.HasNext(); iter.NextTask() {
				// Taking the iterator across to the end of the queue.
			}
			// At this point, pos = fcfsQueue.Len().
			// We should be able to call RemoveTraversedTask() fcfsQueue.Len() times.
			n := fcfsQueue.Len()
			for n > 0 {
				iter.RemoveTraversedTask()
				n--
			}
			assert.True(t, fcfsQueue.IsEmpty())
		})

		t.Run("remove internal tasks of the queue", func(t *testing.T) {
			// Recreating the queue.
			createFCFSQueue()
			testUtils.EnqueueTasks(t, fcfsQueue,
				&def.SimpleTask{
					Name:      "t3",
					CPU:       3.0,
					RAM:       3072,
					Watts:     50.0,
					Image:     "t3-image",
					CMD:       "t3-command",
					Instances: &inst1,
				},
				&def.SimpleTask{
					Name:      "t4",
					CPU:       4.0,
					RAM:       4096,
					Watts:     50.0,
					Image:     "t4-image",
					CMD:       "t4-command",
					Instances: &inst2,
				})

			count := fcfsQueue.Len() / 2 // Number of tasks to traverse before removing any.
			for iter = fcfsQueue.BeginIter(); iter.HasNext(); {
				iter.NextTask()
				count--
				if count == 0 {
					break
				}
			}

			// Emptying the queue from the middle.
			for !fcfsQueue.IsEmpty() {
				iter.RemoveTraversedTask()
			}
		})
	})
}

func TestFCFSTaskQueue_ReverseIteration(t *testing.T) {
	createFCFSQueue()
	inst1 := 10
	inst2 := 10
	testUtils.EnqueueTasks(t, fcfsQueue,
		&def.SimpleTask{
			Name:      "t3",
			CPU:       3.0,
			RAM:       3072,
			Watts:     50.0,
			Image:     "t3-image",
			CMD:       "t3-command",
			Instances: &inst1,
		},
		&def.SimpleTask{
			Name:      "t4",
			CPU:       4.0,
			RAM:       4096,
			Watts:     50.0,
			Image:     "t4-image",
			CMD:       "t4-command",
			Instances: &inst2,
		})

	var iter taskQueue.ReverseIterator
	var task def.Task
	t.Run("HasNext() and NextTask()", func(t *testing.T) {
		// Testing HasNext.
		iter = fcfsQueue.BeginReverseIter()
		for i := 0; i < fcfsQueue.Len(); i++ {
			assert.True(t, iter.HasNext())
			iter.NextTask() // advancing forward.
		}
		// Reached the end of the queue.
		// HasNext() should return false.
		assert.False(t, iter.HasNext())
		// NextTask() should return nil.
		assert.Nil(t, iter.NextTask())

		// Checking whether the tasks are traversed in order and the task
		// definition is accurate.
		count := 0
		for iter = fcfsQueue.BeginReverseIter(); iter.HasNext(); {
			task = iter.NextTask()
			if task != nil {
				expectedTaskName := "t" + strconv.FormatInt(int64(fcfsQueue.Len()-count), 10)
				assert.Equal(t, expectedTaskName, task.GetName())
				expectedCPU := float64(fcfsQueue.Len() - count)
				assert.Equal(t, expectedCPU, task.GetCpu())
				expectedRAM := 1024 * float64(fcfsQueue.Len()-count)
				assert.Equal(t, expectedRAM, task.GetRam())
				assert.Equal(t, 50.0, task.GetWatts())
				expectedImage := expectedTaskName + "-image"
				assert.Equal(t, expectedImage, task.GetImage())
				expectedCMD := expectedTaskName + "-command"
				assert.Equal(t, expectedCMD, task.GetCmd())
				assert.Equal(t, 10, task.GetInstances())
				count++
			}
		}
	})

	t.Run("update of instance count", func(t *testing.T) {
		// Reducing instance count for all tasks by 1.
		for iter = fcfsQueue.BeginReverseIter(); iter.HasNext(); {
			task = iter.NextTask()
			if task != nil {
				task.DecrementInstances()
			}
		}

		// Checking instance count for all tasks to be 1 less than the previous value.
		for iter = fcfsQueue.BeginReverseIter(); iter.HasNext(); {
			task = iter.NextTask()
			if task != nil {
				assert.Equal(t, 9, task.GetInstances())
			}
		}
	})

	t.Run("removal of tasks using reverse iterator", func(t *testing.T) {
		var iter taskQueue.Iterator
		t.Run("remove tasks at the back of the queue", func(t *testing.T) {
			for iter = fcfsQueue.BeginReverseIter(); iter.HasNext(); {
				origLen := fcfsQueue.Len()
				iter.RemoveTraversedTask()
				// Queue length should now reflect the removal.
				assert.Equal(t, fcfsQueue.Len(), origLen-1)
			}
			assert.True(t, fcfsQueue.IsEmpty())
		})

		t.Run("remove tasks at the front of the queue", func(t *testing.T) {
			// Recreating the queue.
			createFCFSQueue()
			inst1 := 10
			inst2 := 10
			testUtils.EnqueueTasks(t, fcfsQueue,
				&def.SimpleTask{
					Name:      "t3",
					CPU:       3.0,
					RAM:       3072,
					Watts:     50.0,
					Image:     "t3-image",
					CMD:       "t3-command",
					Instances: &inst1,
				},
				&def.SimpleTask{
					Name:      "t4",
					CPU:       4.0,
					RAM:       4096,
					Watts:     50.0,
					Image:     "t4-image",
					CMD:       "t4-command",
					Instances: &inst2,
				})

			for iter = fcfsQueue.BeginReverseIter(); iter.HasNext(); iter.NextTask() {
				// Taking the iterator across to the beginning of the queue.
			}
			// At this point, pos = -1.
			// We should be able to call RemoveTraversedTask() fcfsQueue.Len() times.
			n := fcfsQueue.Len()
			for n > 0 {
				iter.RemoveTraversedTask()
				n--
			}
			assert.True(t, fcfsQueue.IsEmpty())
		})

		t.Run("remove internal tasks of the queue", func(t *testing.T) {
			// Recreating the queue.
			createFCFSQueue()
			testUtils.EnqueueTasks(t, fcfsQueue,
				&def.SimpleTask{
					Name:      "t3",
					CPU:       3.0,
					RAM:       3072,
					Watts:     50.0,
					Image:     "t3-image",
					CMD:       "t3-command",
					Instances: &inst1,
				},
				&def.SimpleTask{
					Name:      "t4",
					CPU:       4.0,
					RAM:       4096,
					Watts:     50.0,
					Image:     "t4-image",
					CMD:       "t4-command",
					Instances: &inst2,
				})

			count := fcfsQueue.Len() / 2 // Number of tasks to traverse before removing any.
			for iter = fcfsQueue.BeginReverseIter(); iter.HasNext(); {
				iter.NextTask()
				count--
				if count == 0 {
					break
				}
			}

			// Emptying the queue from the middle.
			for !fcfsQueue.IsEmpty() {
				iter.RemoveTraversedTask()
			}
		})
	})
}
