package fcfs

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/taskQueue"
	"bitbucket.org/sunybingcloud/electron/taskQueue/fcfs/testUtils"
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

var fcfsSortableQueue taskQueue.TaskQueue

func createFCFSSortableTaskQueue() {
	inst1 := 10
	inst2 := 10
	// Tasks ordered such that ti.CPU > ti+1.CPU and ti.RAM <= ti+1.RAM.
	tasks = []def.Task{
		&def.SimpleTask{
			Name:      "t1",
			CPU:       4.0,
			RAM:       1024,
			Watts:     50.0,
			Image:     "t1-image",
			CMD:       "t1-command",
			Instances: &inst1,
		},
		&def.SimpleTask{
			Name:      "t2",
			CPU:       3.0,
			RAM:       2048,
			Watts:     50.0,
			Image:     "t2-image",
			CMD:       "t2-command",
			Instances: &inst2,
		},
	}
	fcfsSortableQueue = NewFCFSSortableTaskQueue(tasks)
}

func TestNewFCFSSortableTaskQueue(t *testing.T) {
	createFCFSSortableTaskQueue()
	assert.IsType(t, reflect.TypeOf(&SortableQueue{}), reflect.TypeOf(fcfsSortableQueue))
	assert.False(t, fcfsSortableQueue.IsEmpty())
	assert.Equal(t, fcfsSortableQueue.Len(), 2)
}

func TestFCFSSortableTaskQueue_Sort(t *testing.T) {
	createFCFSSortableTaskQueue()
	inst1 := 10
	inst2 := 10
	// Tasks ordered such that ti.CPU > ti+1.CPU and ti.RAM <= ti+1.RAM.
	testUtils.EnqueueTasks(t, fcfsSortableQueue,
		&def.SimpleTask{
			Name:      "t3",
			CPU:       2.0,
			RAM:       3072,
			Watts:     50.0,
			Image:     "t3-image",
			CMD:       "t3-command",
			Instances: &inst1,
		},
		&def.SimpleTask{
			Name:      "t4",
			CPU:       1.0,
			RAM:       4096,
			Watts:     50.0,
			Image:     "t4-image",
			CMD:       "t4-command",
			Instances: &inst2,
		})

	t.Run("sorting all tasks", func(t *testing.T) {
		// Sorting the tasks based on CPU.
		fcfsSortableQueue.(taskQueue.SortableTaskQueue).Sort(def.SortByCPU)

		// Checking if the tasks have indeed been sorted.
		var prevTask def.Task
		iter := fcfsSortableQueue.BeginIter()
		for iter.HasNext() {
			curTask := iter.NextTask()
			if prevTask != nil {
				// Check sorted order.
				assert.LessOrEqual(t, prevTask.GetCpu(), curTask.GetCpu())
			}
			prevTask = curTask
		}
	})

	// At this point, the task queue should look like as shown below.
	// [t4, t3, t2, t1] => Sorted in non-decreasing order of CPU.
	t.Run("sorting first N tasks", func(t *testing.T) {
		// Sorting the first N tasks based on RAM.
		fcfsSortableQueue.(taskQueue.SortableTaskQueue).SortN(3, def.SortByRAM)
		// Checking whether the fourth task has been left untouched.
		t.Run("checking if last N tasks have been left untouched", func(t *testing.T) {
			iter := fcfsSortableQueue.BeginIter()
			var task def.Task
			for iter.HasNext() {
				task = iter.NextTask()
			}
			assert.NotNil(t, task)
			// task refers to the last pending task.
			assert.True(t, assert.ObjectsAreEqualValues(&def.SimpleTask{
				Name:      "t1",
				CPU:       4.0,
				RAM:       1024,
				Watts:     50.0,
				Image:     "t1-image",
				CMD:       "t1-command",
				Instances: &inst1,
			}, task))
		})

		t.Run("checking sorted order of first N tasks", func(t *testing.T) {
			// Checking sorted order of first N tasks.
			var prevTask def.Task
			count := 0
			iter := fcfsSortableQueue.BeginIter()
			for count < 3 {
				curTask := iter.NextTask()
				if prevTask != nil {
					// Check sorted order.
					assert.LessOrEqual(t, prevTask.GetRam(), curTask.GetRam())
				}
				prevTask = curTask
				count++
			}
		})
	})
}

func TestFCFSSortableTaskQueue_SortReverse(t *testing.T) {
	createFCFSSortableTaskQueue()
	inst1 := 10
	inst2 := 10
	// Tasks ordered such that ti.CPU > ti+1.CPU and ti.RAM <= ti+1.RAM.
	testUtils.EnqueueTasks(t, fcfsSortableQueue,
		&def.SimpleTask{
			Name:      "t3",
			CPU:       2.0,
			RAM:       3072,
			Watts:     50.0,
			Image:     "t3-image",
			CMD:       "t3-command",
			Instances: &inst1,
		},
		&def.SimpleTask{
			Name:      "t4",
			CPU:       1.0,
			RAM:       4096,
			Watts:     50.0,
			Image:     "t4-image",
			CMD:       "t4-command",
			Instances: &inst2,
		})

	t.Run("sorting all tasks in non-inc order", func(t *testing.T) {
		// Sorting the tasks based on CPU.
		fcfsSortableQueue.(taskQueue.SortableTaskQueue).SortReverse(def.SortByCPU)

		// Checking if the tasks have indeed been sorted.
		var prevTask def.Task
		iter := fcfsSortableQueue.BeginIter()
		for iter.HasNext() {
			curTask := iter.NextTask()
			if prevTask != nil {
				// Check sorted order.
				assert.GreaterOrEqual(t, prevTask.GetCpu(), curTask.GetCpu())
			}
			prevTask = curTask
		}
	})

	// At this point, the task queue should look like as shown below.
	// [t1, t2, t3, t4] => Sorted in non-increasing order of CPU.
	t.Run("sorting first N tasks in non-inc order", func(t *testing.T) {
		// Sorting the first N tasks based on RAM.
		fcfsSortableQueue.(taskQueue.SortableTaskQueue).SortNReverse(3, def.SortByRAM)
		// Checking whether the fourth task has been left untouched.
		t.Run("checking if last N tasks are as is", func(t *testing.T) {
			iter := fcfsSortableQueue.BeginIter()
			var task def.Task
			for iter.HasNext() {
				task = iter.NextTask()
			}
			assert.NotNil(t, task)
			// task refers to the last pending task.
			assert.True(t, assert.ObjectsAreEqualValues(&def.SimpleTask{
				Name:      "t4",
				CPU:       1.0,
				RAM:       4096,
				Watts:     50.0,
				Image:     "t4-image",
				CMD:       "t4-command",
				Instances: &inst2,
			}, task.(*def.SimpleTask)))
		})
	})
}

func TestFCFSSortableTaskQueue_SortNFrom(t *testing.T) {
	createFCFSSortableTaskQueue()
	inst1 := 10
	inst2 := 10
	// Tasks ordered such that ti.CPU > ti+1.CPU and ti.RAM <= ti+1.RAM.
	testUtils.EnqueueTasks(t, fcfsSortableQueue,
		&def.SimpleTask{
			Name:      "t3",
			CPU:       2.0,
			RAM:       3072,
			Watts:     50.0,
			Image:     "t3-image",
			CMD:       "t3-command",
			Instances: &inst1,
		},
		&def.SimpleTask{
			Name:      "t4",
			CPU:       1.0,
			RAM:       4096,
			Watts:     50.0,
			Image:     "t4-image",
			CMD:       "t4-command",
			Instances: &inst2,
		})

	t.Run("error expected when range length is negative", func(t *testing.T) {
		// Sorting the tasks based on CPU.
		assert.NotNil(t, fcfsSortableQueue.(taskQueue.SortableTaskQueue).SortNFrom(1, -1, def.SortByCPU))
	})

	t.Run("error expected when range length is greater than or equal to queue length.", func(t *testing.T) {
		// Sorting the tasks based on CPU.
		assert.NotNil(t, fcfsSortableQueue.(taskQueue.SortableTaskQueue).SortNFrom(1, -1, def.SortByCPU))
	})

	t.Run("sorting internal tasks in non-dec order", func(t *testing.T) {
		// Sorting the tasks based on CPU.
		assert.Nil(t, fcfsSortableQueue.(taskQueue.SortableTaskQueue).SortNFrom(1, 2, def.SortByCPU))

		// Checking if the tasks have indeed been sorted.
		iter := fcfsSortableQueue.BeginIter()
		expectedOutput := [4]float64{4, 2, 3, 1}
		i := 0
		for iter.HasNext() {
			curTask := iter.NextTask()
			assert.Equal(t, expectedOutput[i], curTask.GetCpu())
			i++
		}
	})

	t.Run("sorting first N tasks in non-dec order", func(t *testing.T) {
		// Sorting the tasks based on CPU.
		assert.Nil(t, fcfsSortableQueue.(taskQueue.SortableTaskQueue).SortNFrom(-1, 3, def.SortByCPU))

		// Checking if the tasks have indeed been sorted.
		iter := fcfsSortableQueue.BeginIter()
		expectedOutput := [4]float64{2, 3, 4, 1}
		i := 0
		for iter.HasNext() {
			curTask := iter.NextTask()
			assert.Equal(t, expectedOutput[i], curTask.GetCpu())
			i++
		}
	})

	t.Run("sorting last N tasks in non-dec order", func(t *testing.T) {
		// Sorting the tasks based on CPU.
		assert.Nil(t, fcfsSortableQueue.(taskQueue.SortableTaskQueue).SortNFrom(1, 10, def.SortByCPU))

		// Checking if the tasks have indeed been sorted.
		iter := fcfsSortableQueue.BeginIter()
		expectedOutput := [4]float64{2, 1, 3, 4}
		i := 0
		for iter.HasNext() {
			curTask := iter.NextTask()
			assert.Equal(t, expectedOutput[i], curTask.GetCpu())
			i++
		}
	})
}

func TestFCFSSortableTaskQueue_SortNFromReverse(t *testing.T) {
	createFCFSSortableTaskQueue()
	inst1 := 10
	inst2 := 10
	// Tasks ordered such that ti.CPU > ti+1.CPU and ti.RAM <= ti+1.RAM.
	testUtils.EnqueueTasks(t, fcfsSortableQueue,
		&def.SimpleTask{
			Name:      "t3",
			CPU:       5.0,
			RAM:       3072,
			Watts:     50.0,
			Image:     "t3-image",
			CMD:       "t3-command",
			Instances: &inst1,
		},
		&def.SimpleTask{
			Name:      "t4",
			CPU:       6.0,
			RAM:       4096,
			Watts:     50.0,
			Image:     "t4-image",
			CMD:       "t4-command",
			Instances: &inst2,
		})

	t.Run("error expected when range length is negative", func(t *testing.T) {
		// Sorting the tasks based on CPU.
		assert.NotNil(t, fcfsSortableQueue.(taskQueue.SortableTaskQueue).SortNFromReverse(1, -1, def.SortByCPU))
	})

	t.Run("error expected when range length is greater than or equal to queue length.", func(t *testing.T) {
		// Sorting the tasks based on CPU.
		assert.NotNil(t, fcfsSortableQueue.(taskQueue.SortableTaskQueue).SortNFromReverse(1, -1, def.SortByCPU))
	})

	t.Run("sorting second and third task in dec order", func(t *testing.T) {
		// Sorting the tasks based on CPU.
		assert.Nil(t, fcfsSortableQueue.(taskQueue.SortableTaskQueue).SortNFromReverse(1, 2, def.SortByCPU))

		// Checking if the tasks have indeed been sorted.
		iter := fcfsSortableQueue.BeginIter()
		expectedOutput := [4]float64{4, 5, 3, 6}
		i := 0
		for iter.HasNext() {
			curTask := iter.NextTask()
			assert.Equal(t, expectedOutput[i], curTask.GetCpu())
			i++
		}
	})

	t.Run("sorting first N tasks in dec order", func(t *testing.T) {
		// Sorting the tasks based on CPU.
		assert.Nil(t, fcfsSortableQueue.(taskQueue.SortableTaskQueue).SortNFromReverse(-1, 3, def.SortByCPU))

		// Checking if the tasks have indeed been sorted.
		iter := fcfsSortableQueue.BeginIter()
		expectedOutput := [4]float64{5, 4, 3, 6}
		i := 0
		for iter.HasNext() {
			curTask := iter.NextTask()
			assert.Equal(t, expectedOutput[i], curTask.GetCpu())
			i++
		}
	})

	t.Run("sorting last N tasks in dec order", func(t *testing.T) {
		// Sorting the tasks based on CPU.
		assert.Nil(t, fcfsSortableQueue.(taskQueue.SortableTaskQueue).SortNFromReverse(1, 10, def.SortByCPU))

		// Checking if the tasks have indeed been sorted.
		iter := fcfsSortableQueue.BeginIter()
		expectedOutput := [4]float64{5, 6, 4, 3}
		i := 0
		for iter.HasNext() {
			curTask := iter.NextTask()
			assert.Equal(t, expectedOutput[i], curTask.GetCpu())
			i++
		}
	})
}
