package metrics

import (
	"bitbucket.org/sunybingcloud/electron/events"
	elecLogDef "bitbucket.org/sunybingcloud/electron/logging/def"
	"bytes"
	"errors"
	"fmt"
	"reflect"
	"sync"
)

// HostInfoListener maintains mapping between Hostname and Slave ID.
//
// Although hostnames are not tightly coupled with Slave IDs, a mapping is maintained to facilitate
// linking of PCP data to resource allocations. PCP data contains hostnames, while Mesos status update messages
// contain only Slave IDs. The hostname associated with a Slave ID is retrieved from Mesos resource offers.
type HostInfoListener struct {
	eventTypes []events.EventType
	sync.Mutex
	// Maintain a mapping between hostname and slave Id.
	// This is required as PCP data contains hostname, Mesos resource offers
	// contain hostname and slave Id and task statuses contain only slave Id.
	slaveToHost map[string]string
	hostToSlave map[string]string
	// Send the type of the message to be logged
	logMsgType chan elecLogDef.LogMessageType
	// Send the message to be logged
	logMsg chan string
}

var hilInstance *HostInfoListener

func hostInfoListenerbaseInstance() *HostInfoListener {
	return &HostInfoListener{
		eventTypes: []events.EventType{
			events.TASK_OFFER_MATCH,
		},
		slaveToHost: make(map[string]string),
		hostToSlave: make(map[string]string),
	}
}

func NewHostInfoListenerInstance(lmt chan elecLogDef.LogMessageType, lmsg chan string) events.EventListener {
	if hilInstance != nil {
		return hilInstance
	}
	hilInstance = hostInfoListenerbaseInstance()
	hilInstance.logMsgType = lmt
	hilInstance.logMsg = lmsg
	return hilInstance
}

func GetHostInfoListenerInstance() events.EventListener {
	return hilInstance
}

func (l *HostInfoListener) GetEventTypes() []events.EventType {
	return l.eventTypes
}

func (l *HostInfoListener) GetSlaveID(hostname string) (string, error) {
	l.Lock()
	defer l.Unlock()
	if slaveID, ok := l.hostToSlave[hostname]; !ok {
		return "", errors.New(fmt.Sprintf("No mapping from hostname to slave ID for %s", hostname))
	} else {
		return slaveID, nil
	}
}

func (l *HostInfoListener) GetHostname(slaveID string) (string, error) {
	l.Lock()
	defer l.Unlock()
	if hostname, ok := l.slaveToHost[slaveID]; !ok {
		return "", errors.New(fmt.Sprintf("No mapping from slave ID to hostname for %s", slaveID))
	} else {
		return hostname, nil
	}
}

func (l *HostInfoListener) Update(et events.EventType, data events.EventData) error {
	l.Lock()
	defer l.Unlock()
	switch et {
	case events.TASK_OFFER_MATCH:
		if _, ok := data.(*events.TaskOfferMatchEventData); ok {
			d := data.Get().(events.TaskOfferMatchEventData)

			slaveID := *d.Offer.SlaveId.Value
			// Add new mapping between hostname and slave Id if none exists.
			// Remove any existing mapping for hostname or slave Id (now stale).
			hostname := d.GetHostname()
			if _, ok := l.hostToSlave[hostname]; !ok {
				if _, ok := l.slaveToHost[slaveID]; !ok {
					l.hostToSlave[hostname] = slaveID
					l.slaveToHost[slaveID] = hostname
				} else {
					oldHost := l.slaveToHost[slaveID]
					// Remove stale entry for oldHost.
					delete(l.hostToSlave, oldHost)
					// Remove existing mapping from slave Id to host (now stale).
					delete(l.slaveToHost, slaveID)
					// Add new mapping between slave Id and hostname.
					l.slaveToHost[slaveID] = hostname
					l.hostToSlave[hostname] = slaveID
				}
			} else if _, ok := l.slaveToHost[slaveID]; !ok {
				oldSlave := l.hostToSlave[hostname]
				// Remove stale entry for oldSlave.
				delete(l.slaveToHost, oldSlave)
				// Remove existing mapping from hostname to slave Id (now stale).
				delete(l.hostToSlave, hostname)
				// Add new mapping between slave Id and hostname.
				l.slaveToHost[slaveID] = hostname
				l.hostToSlave[hostname] = slaveID
			} else {
				// There is an existing mapping between hostname and slave Id.
				// In this case, just update the mapping.
				l.hostToSlave[hostname] = slaveID
				l.slaveToHost[slaveID] = hostname
			}
		} else {
			return &events.InvalidDataForEventError{
				GivenType:    reflect.TypeOf(data),
				RequiredType: reflect.TypeOf(events.TaskOfferMatchEventData{}),
			}
		}
	default:
		return &events.InvalidEventTypeError{
			GivenType:     et,
			RequiredTypes: events.TASK_OFFER_MATCH.String(),
		}
	}
	return nil
}

func (l *HostInfoListener) String() string {
	var buf bytes.Buffer
	buf.WriteString("SLAVE --> HOST mapping\n---------------------\n")
	for slaveID, hostname := range l.slaveToHost {
		buf.WriteString(fmt.Sprintf("[%s] --> [%s]\n", slaveID, hostname))
	}
	buf.WriteString("\n")
	buf.WriteString("HOST --> SLAVE mapping\n---------------------\n")
	for hostname, slaveID := range l.hostToSlave {
		buf.WriteString(fmt.Sprintf("[%s] --> [%s]\n", hostname, slaveID))
	}
	return buf.String()
}

func (l *HostInfoListener) WriteLogs() {}

func (l *HostInfoListener) Stop() {}
