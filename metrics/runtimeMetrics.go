package metrics

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/events"
	elecLogDef "bitbucket.org/sunybingcloud/electron/logging/def"
	"bitbucket.org/sunybingcloud/electron/utilities/mesosUtils"
	"bitbucket.org/sunybingcloud/electron/utilities/offerUtils"
	"bytes"
	"fmt"
	"github.com/jinzhu/copier"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	"github.com/sajari/regression"
	"reflect"
	"strings"
	"sync"
	"time"
)

type RuntimeMetricsListener struct {
	eventTypes []events.EventType
	sync.Mutex
	// Runtime metrics data for all tasks.
	// This internally also maintains a mapping between taskIDs and task names.
	rData *runtimeData
	// Send the type of the message to be logged
	logMsgType chan elecLogDef.LogMessageType
	// Send the message to be logged
	logMsg chan string

	logsPersisted bool

	// Storing allocationStats for finished tasks, keyed by taskID.
	allocationStatsTask map[string]events.AllocationStatsUpdateEventData
}

// Runtime data for all tasks.
type runtimeData struct {
	// 3 data stores - observed, unobserved, incomplete.
	// State transitions.
	// 1. incomplete -> unobserved: When a task completes execution, the corresponding entry is moved
	// 	from the incomplete store to the unobserved store. Now, this task is a candidate to be observed.
	// 2. unobserved -> observed: When sufficient number of tasks fill the unobserved store, regression is
	// 	repeated considering the tasks across observed and unobserved stores. Then, the entries for the task
	// 	from unobserved store are moved to the observed store.
	// Note: An entry for a task can only be in one of the three stores.
	//
	// Metrics are stored using 2-level index.
	// First level index stores the task name. The second level index stores the entire taskID of the task.

	// Store the runtime data for tasks that have completed execution and the execution information of which
	// has been observed during regression.
	observedTasks map[string]*taskRuntimeData
	// Store the runtime data for tasks that have completed execution but the execution information
	// has not yet been used for regression.
	// Once there are sufficient number of unobserved tasks, regression can be repeated considering the corresponding
	// entries both in unobserved and observed.
	unobservedTasks map[string]*taskRuntimeData
	// Store the runtime data for tasks that are yet to complete execution.
	incompleteTasks map[string]*taskRuntimeData

	// Store the mapping between taskIDs to task name.
	// There may be multiple instances of the same task. Each instance would have a different taskID.
	// However, all the instances would correspond to the same task and thus would have the same name.
	taskIDToName map[string]string

	// Observations (data points) for regression analysis.
	// Each data point corresponds to a task.
	// Key = task name.
	recordedDataPoints map[string]regression.DataPoints
	// Number of newly added data points for each task.
	numNewDataPoints map[string]int
}

// This number corresponds to the ratio of #unobservedTasks to #observedTasks that is the criteria for repeating
// the regression analysis for execution time predictions.
const regressionRepeatThreshold = 0.1

// Storing runtime data for all the instances of a task.
type taskRuntimeData struct {
	instData map[string]*runtimeMetrics
	r        *regression.Regression
}

// Runtime metrics for a task
type runtimeMetrics struct {
	// Host on which task was launched.
	// Note: This is not a runtime metric but is stored so as to facilitate retrieval of other information
	// associated with this task, such as allocation metrics (where data is stored keyed by Host and taskID).
	Host string
	// Power class of the Host on which the task was run.
	PowerClass string
	// Execution time.
	// Elapsed time between TASK_STARTING and TASK_FINISHED.
	ExecutionStart  time.Time
	ExecutionFinish time.Time
	ExecutionTime   time.Duration
	// Submitted time.
	// We are currently storing this value just to be able to calculate wait-time.
	SubmittedTime time.Time
	// Wait time.
	// Elapsed time between submission and TASK_STARTING.
	//
	// Note: There is a small delay between task launch and receipt of the TASK_STARTING
	// status update from Mesos. This is why we consider the TASK_STARTING message
	// to be the indicator for the task starting execution on a worker node.
	WaitTime time.Duration
	// Flag indicating whether the task has completed execution.
	HasFinished bool
}

var rmlInstance *RuntimeMetricsListener

func runtimeListenerBaseInstance() *RuntimeMetricsListener {
	return &RuntimeMetricsListener{
		eventTypes: []events.EventType{
			events.TASK_OFFER_MATCH,
			events.TASK_STATUS_UPDATE,
			events.ALLOCATION_STATS_UPDATE,
		},

		rData:               newRuntimeData(),
		logsPersisted:       false,
		allocationStatsTask: make(map[string]events.AllocationStatsUpdateEventData),
	}
}

func newRuntimeData() *runtimeData {
	return &runtimeData{
		incompleteTasks:    make(map[string]*taskRuntimeData),
		unobservedTasks:    make(map[string]*taskRuntimeData),
		observedTasks:      make(map[string]*taskRuntimeData),
		taskIDToName:       make(map[string]string),
		recordedDataPoints: make(map[string]regression.DataPoints),
		numNewDataPoints:   make(map[string]int),
	}

}

func NewRuntimeMetricsListener(lmt chan elecLogDef.LogMessageType, lmsg chan string) events.EventListener {
	if rmlInstance != nil {
		return rmlInstance
	}
	rmlInstance = runtimeListenerBaseInstance()
	rmlInstance.logMsgType = lmt
	rmlInstance.logMsg = lmsg
	return rmlInstance
}

func GetRuntimeMetricsListenerInstance() events.EventListener {
	return rmlInstance
}

func (l RuntimeMetricsListener) GetEventTypes() []events.EventType {
	return l.eventTypes
}

// Updated the task runtime metrics.
// The data provided must be of type TaskStatusEventData.
func (l *RuntimeMetricsListener) Update(eType events.EventType, eData events.EventData) error {
	l.Lock()
	defer l.Unlock()
	switch eType {
	case events.TASK_OFFER_MATCH:
		if _, ok := eData.(*events.TaskOfferMatchEventData); ok {
			d := eData.Get().(events.TaskOfferMatchEventData)
			l.rData.taskSubmitted(d.GetTaskID(), d.Task, d.Offer)
		} else {
			return &events.InvalidDataForEventError{
				GivenType:    reflect.TypeOf(eData),
				RequiredType: reflect.TypeOf(events.TaskOfferMatchEventData{}),
			}
		}

	case events.TASK_STATUS_UPDATE:
		if _, ok := eData.(*events.TaskStatusEventData); ok {
			status := eData.Get().(*mesos.TaskStatus)
			taskID := *status.TaskId.Value
			// Runtime metrics data for this taskID should only be in incompleteTasks store.
			taskName, _ := l.rData.taskNameFor(taskID)
			if dataSameTaskInstances, ok := l.rData.incompleteTasks[taskName]; ok {
				rm, _ := dataSameTaskInstances.instData[taskID]
				// Safety check. Still unsure in what scenario rm would be nil.
				// This safety check was put in place due to a segfault in the below described scenario.
				// TASK_FAILED message received for a task.
				// Looking at the console logs, it was observed that Electron did not receive TASK_STARTING
				// status update for the task.
				if rm != nil { // safety check. Still unsure when rm would be nil.
					allocStats, _ := l.allocationStatsTask[taskID]
					l.rData.taskStatusUpdate(status, rm, taskID, allocStats)
				}
				if mesosUtils.IsTerminal(status.State) {
					// Data point has already been recorded/discarded for task.
					// We can get rid of allocation stats.
					// This reduces memory usage.
					delete(l.allocationStatsTask, taskID)
				}
			} else {
				// Shouldn't be here.
			}
		} else {
			return &events.InvalidDataForEventError{
				GivenType:    reflect.TypeOf(eData),
				RequiredType: reflect.TypeOf(events.TaskStatusEventData{}),
			}
		}
	case events.ALLOCATION_STATS_UPDATE:
		if _, ok := eData.(*events.AllocationStatsUpdateEventData); ok {
			allocStats := eData.Get().(events.AllocationStatsUpdateEventData)
			// Updating/Recording allocation stats for task.
			l.allocationStatsTask[allocStats.GetTaskID()] = allocStats
		} else {
			return &events.InvalidDataForEventError{
				GivenType:    reflect.TypeOf(eData),
				RequiredType: reflect.TypeOf(events.AllocationStatsUpdateEventData{}),
			}
		}

	default:
		return &events.InvalidEventTypeError{
			GivenType: eType,
			RequiredTypes: strings.Join([]string{events.TASK_OFFER_MATCH.String(),
				events.TASK_STATUS_UPDATE.String()}, ","),
		}
	}
	return nil
}

// Determine if the task is going to complete execution within now and N seconds into the future.
// This is using the latest predicted execution time for this task.
func (l *RuntimeMetricsListener) WillFinishIn(taskID string, n float64, now time.Time) bool {
	l.Lock()
	defer l.Unlock()
	taskName, _ := l.rData.taskNameFor(taskID)
	if incompleteTaskRmData, ok := l.rData.incompleteTasks[taskName]; ok {
		rm, _ := incompleteTaskRmData.instData[taskID]
		if rm == nil {
			// Task has already completed execution.
			return true
		}
		if allocStats, ok := l.allocationStatsTask[taskID]; !ok {
			// No stats available for task.
			return false
		} else {
			// Check whether an instance (at least) of this task has completed execution.
			if observedTaskRmData, ok := l.rData.observedTasks[taskName]; ok {
				// Predicting the execution time of this task given,
				// 1. power-class of the host on which it is currently running.
				// 2. current average of the actual share of CPU resources that the task has been
				// allocated.
				predExecTime, _ := observedTaskRmData.r.Predict([]float64{
					offerUtils.PowerClassRank(rm.PowerClass),
					allocStats.AvgActualShareCpu,
				})
				// Checking whether task will complete in the next N (given) seconds.
				elapsedTime := now.Sub(rm.ExecutionStart).Seconds()
				if (predExecTime - elapsedTime) < n {
					return true
				}
				return false
			} else {
				// No historic observed data available for any prior run of this task.
				// Return false as we cannot estimate the execution time.
				return false
			}
		}
	} else {
		// Task with the given taskID not currently running on any host.
		if unobservedTaskRmData, ok := l.rData.unobservedTasks[taskName]; ok {
			if _, ok := unobservedTaskRmData.instData[taskID]; ok {
				// Task has already completed execution.
				return true
			}
		}
		if observedTaskRmData, ok := l.rData.observedTasks[taskName]; ok {
			if _, ok := observedTaskRmData.instData[taskID]; ok {
				// Task has already completed execution.
				return true
			}
		}
		// Task with given taskID was never launched.
		// Invalid taskID.
		return false
	}
}

// Get runtime data for all tasks that have completed execution.
func (rmd runtimeData) get() (observed, unobserved map[string]*taskRuntimeData) {
	observed = rmd.observedTasks
	unobserved = rmd.unobservedTasks
	return
}

// Record submission time for task.
func (rmd *runtimeData) taskSubmitted(taskID string, task def.Task, offer *mesos.Offer) {
	// Record mapping between taskID and task name.
	rmd.mapTaskIDToName(taskID, task.GetName())
	// Saving submission time (either via CLI or HTTP).
	// Note: This is a workaround to be able to record submissions-time.
	// Ideally, one should be able to record all of this in one place.
	if dataSameNameTasks, found := rmd.incompleteTasks[task.GetName()]; !found {
		// First time encountering task.Name.
		// Need to add new entry for task.
		rmd.incompleteTasks[task.GetName()] = &taskRuntimeData{
			instData: map[string]*runtimeMetrics{
				taskID: {
					SubmittedTime: task.GetSubmittedTime(),
					HasFinished:   false,
					Host:          offer.GetHostname(),
					PowerClass:    offerUtils.PowerClass(offer),
				},
			},
		}
	} else {
		// Check if submission time recorded for this particular task.
		if _, ok := dataSameNameTasks.instData[taskID]; !ok {
			dataSameNameTasks.instData[taskID] = &runtimeMetrics{
				SubmittedTime: task.GetSubmittedTime(),
				HasFinished:   false,
				Host:          offer.GetHostname(),
				PowerClass:    offerUtils.PowerClass(offer),
			}
		} else {
			// Submission time already recorded.
		}
	}
}

func (rmd *runtimeData) removeFromStore(store map[string]*taskRuntimeData, taskID string) {
	taskName, _ := rmd.taskNameFor(taskID)
	// Deleting entry for taskID in incomplete store.
	store[taskName].removeEntryForInst(taskID)
	// If no data for any instances of the task, then delete entry for task in given store.
	if len(store[taskName].instData) == 0 {
		delete(store, taskName)
	}
}

func (rmd *runtimeData) addToUnobservedTasks(taskID string, rm *runtimeMetrics) {
	taskName, _ := rmd.taskNameFor(taskID)
	if _, ok := rmd.unobservedTasks[taskName]; !ok {
		rmd.unobservedTasks[taskName] = &taskRuntimeData{
			instData: make(map[string]*runtimeMetrics),
			r:        nil,
		}
	}
	var rmCopy *runtimeMetrics = &runtimeMetrics{}
	copier.Copy(rmCopy, rm)
	rmd.unobservedTasks[taskName].instData[taskID] = rmCopy
	// Entry needs to be removed from incompleteTasks.
	rmd.removeFromStore(rmd.incompleteTasks, taskID)
}

func (rmd *runtimeData) addToObservedTasks(taskID string) {
	taskName, _ := rmd.taskNameFor(taskID)
	if _, ok := rmd.observedTasks[taskName]; !ok {
		rmd.observedTasks[taskName] = &taskRuntimeData{
			instData: make(map[string]*runtimeMetrics),
			r:        nil, // Will be initialized before training the model.
		}
	}
	// Need to copy all entries from unobservedTasks.
	for taskID, rm := range rmd.unobservedTasks[taskName].instData {
		var rmCopy *runtimeMetrics = &runtimeMetrics{}
		copier.Copy(rmCopy, rm)
		rmd.observedTasks[taskName].instData[taskID] = rmCopy
		// Need to delete corresponding entry in unobservedTasks.
		rmd.removeFromStore(rmd.unobservedTasks, taskID)
	}
}

func (rmd *runtimeData) recordNewDataPoint(
	rm *runtimeMetrics,
	taskID string,
	allocStats events.AllocationStatsUpdateEventData) {

	// Set up data point for regression analysis.
	taskName, _ := rmd.taskNameFor(taskID)

	o := regression.DataPoint(rm.ExecutionTime.Seconds(), []float64{
		offerUtils.PowerClassRank(rm.PowerClass),
		allocStats.AvgActualShareCpu,
	})

	rmd.recordedDataPoints[taskName] = append(rmd.recordedDataPoints[taskName], o)
	if _, ok := rmd.numNewDataPoints[taskName]; !ok {
		rmd.numNewDataPoints[taskName] = 1
	} else {
		rmd.numNewDataPoints[taskName]++
	}
}

func (rmd *runtimeData) taskStatusUpdate(
	status *mesos.TaskStatus,
	rm *runtimeMetrics,
	taskID string,
	allocStats events.AllocationStatsUpdateEventData) {
	// Updating metrics.
	switch *status.State {
	case mesos.TaskState_TASK_STARTING:
		startTime := time.Now()
		// Determining wait time.
		rm.calcWaitTime(startTime)
	case mesos.TaskState_TASK_FINISHED,
		mesos.TaskState_TASK_FAILED,
		mesos.TaskState_TASK_KILLED,
		mesos.TaskState_TASK_LOST,
		mesos.TaskState_TASK_ERROR:

		finishTime := time.Now()
		// Determining execution time.
		rm.calcExecutionTime(finishTime)
		// Task has finished execution.
		// Runtime metrics and runtime environment information for this task can now be used to predict the
		// execution times of instances of the same task that are yet to complete execution.
		rm.HasFinished = true
		// Task moves from incompleteTasks to unobservedTasks.
		// Passing rm to reduce overhead of retrieving the *runtimeMetrics object for taskID.
		rmd.addToUnobservedTasks(taskID, rm)

		// We are recording new data points and using the data for regression analysis only if the task
		// successfully completed execution.
		if *status.State == mesos.TaskState_TASK_FINISHED {
			// New data available for regression analysis.
			rmd.recordNewDataPoint(rm, taskID, allocStats)
			taskName, _ := rmd.taskNameFor(taskID)
			// If sufficient number of new data points added, then we train a new regression model with the
			// union of the current and the new data.
			totalRecordedDataPoints := len(rmd.recordedDataPoints[taskName])
			if float64(rmd.numNewDataPoints[taskName]) >=
				float64(regressionRepeatThreshold*float64(totalRecordedDataPoints)) {
				// Move entries for all instances for this task from unobserved into observed.
				// Data for all instances of this task is going to be used to train a new regression model.
				rmd.addToObservedTasks(taskID)
				// Need to reinitialize the regression object. Otherwise, there would be no change in the
				// predictions made. This is because, regression.Regression#Run() can be called only once.
				// For confirmation, see https://github.com/sajari/regression/issues/21.
				rmd.observedTasks[taskName].r = newRegressionObject()
				// rmd.observedTasks[taskName].r.Train(rmd.recordedDataPoints[taskName]...)
				// rmd.observedTasks[taskName].r.Run()
				// Resetting the number of new data points.
				rmd.numNewDataPoints[taskName] = 0
			}
		}
	default:
	}
}

// Get name of the task for the given taskID.
func (rmd runtimeData) taskNameFor(taskID string) (string, bool) {
	if taskName, ok := rmd.taskIDToName[taskID]; !ok {
		return "", false
	} else {
		return taskName, true
	}
}

// Map taskID to task name.
func (rmd *runtimeData) mapTaskIDToName(taskID string, taskName string) {
	if _, ok := rmd.taskIDToName[taskID]; !ok {
		rmd.taskIDToName[taskID] = taskName
	} else {
		// Mapping already exists.
	}
}

func (t *taskRuntimeData) removeEntryForInst(taskID string) {
	delete(t.instData, taskID)
}

func newRegressionObject() *regression.Regression {
	r := new(regression.Regression)
	r.SetObserved("Execution Time")
	r.SetVar(0, "Power Class")
	r.SetVar(1, "Average actual share of CPU resources")
	return r
}

// Determine the wait time for task.
// Wait time = time elapsed between submission time and startTime.
func (m *runtimeMetrics) calcWaitTime(startTime time.Time) float64 {
	m.ExecutionStart = startTime
	wt := m.ExecutionStart.Sub(m.SubmittedTime)
	m.WaitTime = wt
	return wt.Seconds()
}

// Determine the execution time for task.
// Execution time = time elapsed between execution start time and finishTime.
func (m *runtimeMetrics) calcExecutionTime(finishTime time.Time) float64 {
	m.ExecutionFinish = finishTime
	et := m.ExecutionFinish.Sub(m.ExecutionStart)
	m.ExecutionTime = et
	return et.Seconds()
}

// String returns a string representation of all the runtime data collected and
// recorded for each and every task that was submitted and scheduled by the framework.
// The returned string would be in a CSV format with the first line containing the headers.
// Note: Only considering tasks that have completed execution.
func (l RuntimeMetricsListener) String() string {
	var dataString bytes.Buffer
	// Column Headings.
	dataString.WriteString(fmt.Sprintln("TaskID,SubmitTime(yyyy/mm/dd hh:mm:ss),WaitTime(seconds)," +
		"ExecusionStartTime(yyyy/mm/dd hh:mm:ss),ExecutionFinishTime(yyyy/mm/dd hh:mm:ss),ExecutionTime(seconds)"))
	values := []string{}
	observed, unobserved := l.rData.get()
	dataStores := []map[string]*taskRuntimeData{observed, unobserved}
	count := 0
	for _, store := range dataStores {
		for _, taskRmData := range store {
			for taskID, rm := range taskRmData.instData {
				count++
				values = append(values, strings.Join([]string{
					taskID,
					(*rm).SubmittedTime.Format("2006/01/02 15:04:05"),
					fmt.Sprintf("%.2f", (*rm).WaitTime.Seconds()),
					(*rm).ExecutionStart.Format("2006/01/02 15:04:05"),
					(*rm).ExecutionFinish.Format("2006/01/02 15:04:05"),
					fmt.Sprintf("%.2f", (*rm).ExecutionTime.Seconds()),
				}, ","))
			}
		}
	}
	dataString.WriteString(strings.Join(values, fmt.Sprintln()))
	return dataString.String()
}

func (l *RuntimeMetricsListener) WriteLogs() {
	l.Lock()
	defer l.Unlock()
	if !l.logsPersisted {
		l.logMsgType <- elecLogDef.RUNTIME_METRICS
		l.logMsg <- l.String()
		l.logsPersisted = true
	}
}

func (l *RuntimeMetricsListener) Stop() {}
