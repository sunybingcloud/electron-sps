package metrics

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/events"
	elecLogDef "bitbucket.org/sunybingcloud/electron/logging/def"
	"github.com/jinzhu/copier"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	"github.com/pkg/errors"
)

type AllocationMetricsListener struct {
	eventTypes []events.EventType
	sync.Mutex

	// Resources allocated and actual share  of resources available can be different for each running task.
	// Mapping hostname to the allocation information for each task.
	// These are the tasks for which the TASK_STARTING status has been received but terminal status update
	// has not yet been received (meaning the task is currently active and running).
	// Example: {host: myHost => {taskID: tid1 => allocationInfo: {...}}}
	resourceAllocationInfoCluster map[string]map[string]*AllocationInfo
	// Maintaining history of resource allocation information and statistics.
	// When a task completes execution, the entry for the task in ResourceAllocationInfoCluster will be stored as
	// part of the history.
	resourceAllocationInfoClusterHistory map[string]map[string]*AllocationInfo
	// Tasks which have been fit into offers but TASK_STARTING status update has not yet
	// been received.
	// Example: {host: myHost => {taskID: tid1 => {cpu: c, ram: m, tolerance: t, watts: w}}}
	// Note: Tasks that haven't yet started will not be added to the ResourceAllocationInfo as they are
	// not currently affecting the actual share of resources that the currently running tasks get.
	fitTasks map[string]map[string]*FitTaskInfo

	// Send the type of message to be logged.
	logMsgType chan elecLogDef.LogMessageType
	// Send the message to be logged.
	logMsg chan string

	// As allocation metrics are constantly updated for tasks, there would be a lot of overhead data is logged
	// every time there is an update. Instead, allocation metrics are logged every second.
	persistDataTimer *time.Ticker
	// Information pertaining to taskID and hostname for persisting stats.
	statsForChan chan statsFor
	// Stop periodic persisting of data. This channel should be closed when Stop() is called.
	stop chan struct{}

	eventListeners map[events.EventType][]events.EventListener

	// Latest external event data. External events are events originating from external sources and
	// not created by this listener.
	// Overwritten upon every Update(...) call for specific event on the listener.
	externalEventData map[events.EventType]events.EventData
}

// Information corresponding to resource allocations and actual share of the corresponding
// resource that a task gets.
// Allocation include CPU, memory and power.
// TODO include memory allocation information also.
type AllocationInfo struct {
	// Hostname of host on which resources have been allocated.
	Host string
	// The cpu share allocated when scheduling.
	// If tasks are not resized, this this value would be same as def.Task#CPU in the workload.
	// See https://docs.docker.com/config/containers/resource_constraints/#configure-the-default-cfs-scheduler to
	// understand --cpu-shares for docker.
	CpuShare float64
	// Fraction of CPU resources that the task gets once launched.
	// Corresponds to the portion of cpu-time that this task gets.
	// Depends on the cpu-share allocations of other tasks on the same host.
	ActualShareCpu float64
	// Share of processor power (Watts) that the task gets.
	// This is an approximation based on the actual share of cpu resources that the task gets.
	ActualSharePower float64
	// Max share of processing power (Watts) for the task.
	// This is calculated as (ActualShareCpu * TDP).
	// Note: This assumes proportionate distribution of processing power amongst the running tasks.
	MaxSharePower float64
	// Tolerance
	Tolerance float64

	// Allocation Stats.
	Stats *AllocationStats
}

// Allocation Statistics.
type AllocationStats struct {
	// Storing historic values.
	historicActualShareCpu   []float64
	historicActualSharePower []float64
	historicMaxSharePower    []float64
	// Average share of CPU resources that a task gets during its execution (%).
	sumActualShareCpu float64 // To facilitate constant-time average calculation.
	avgActualShareCpu float64
	// Average amount of processing power that a task gets during its execution (Watts).
	sumActualSharePower float64 // To facilitate constant-time average calculation.
	avgActualSharePower float64
	// Average of the max-share of processing power that a task has during its execution (Watts).
	sumMaxSharePower float64 // To facilitate constant-time average calculation.
	avgMaxSharePower float64
}

// Information corresponding to tasks that have just been fit into offers but have not yet started executing
// on a node (TASK_RUNNING status message hasn't yet been received for the task).
type FitTaskInfo struct {
	// CPU share of the task.
	// Value specified in the workload unless task is resized.
	CPU float64
	// Memory allocated.
	RAM float64
	// Watts requirement of task.
	// Value specified in the workload unless task is resized.
	Watts float64
	// Tolerance.
	Tolerance float64
}

// Update allocation information given all the tasks running on a host.
// When a new task starts running on a host, the actual share of CPU will change.
func (aInfo *AllocationInfo) update(
	resAllocInfoHost map[string]*AllocationInfo,
	tdp float64,
	totalPowerCpu float64) {

	aInfo.updateCpuResourceAllocation(resAllocInfoHost)
	aInfo.updatePowerResourceAllocation(totalPowerCpu, tdp)
}

func (aInfo *AllocationInfo) updateCpuResourceAllocation(resAllocInfoHost map[string]*AllocationInfo) {
	totalCpuShareAllocated := 0.0
	for _, ai := range resAllocInfoHost {
		totalCpuShareAllocated += ai.CpuShare
	}
	// If no other tasks running on host, then totalCpuShareAllocated = current task's cpu share.
	if totalCpuShareAllocated == 0.0 {
		totalCpuShareAllocated = aInfo.CpuShare
	}
	aInfo.ActualShareCpu = aInfo.CpuShare / totalCpuShareAllocated
}

func (aInfo *AllocationInfo) updatePowerResourceAllocation(totalPowerCpu, tdp float64) {
	// Let the power consumed by the CPU be P.
	// Let the actual share of cpu resources that task t gets be CSRt.
	// Let the share of CPU power that task t gets be Pt.
	// Pt = CSRt * P.
	// Note: This does not consider the influence of the on-demand governor dynamically changing the operating
	// frequencies of the processors.
	// Note: It is possible that there is a minute delay between TASK_RUNNING status update and the next PCP event.
	// However, in the time between now and the next PCP event, the state of the cluster might change (other tasks
	// might complete execution or new tasks might be launched). Hence, we are approximating the actual share of power
	// consumption based on the latest data from the utilization metrics listener.
	aInfo.ActualSharePower = aInfo.ActualShareCpu * totalPowerCpu
	aInfo.MaxSharePower = aInfo.ActualShareCpu * tdp
	aInfo.Stats.update(aInfo)
}

// Note: Allocation stats are not returned as part of the string.
func (aInfo *AllocationInfo) String() string {
	cpuShareString := strconv.FormatFloat(aInfo.CpuShare, 'f', 3, 64)
	// Converting actualShareCpu to percentage.
	actualShareCpuString := strconv.FormatFloat(aInfo.ActualShareCpu*100.0, 'f', 3, 64)
	actualSharePowerString := strconv.FormatFloat(aInfo.ActualSharePower, 'f', 3, 64)
	maxSharePowerString := strconv.FormatFloat(aInfo.MaxSharePower, 'f', 3, 64)
	return strings.Join([]string{cpuShareString, actualShareCpuString, actualSharePowerString, maxSharePowerString}, ",")
}

// Update allocation statistics and update history.
func (aStats *AllocationStats) update(aInfo *AllocationInfo) {
	aStats.historicActualShareCpu = append(aStats.historicActualShareCpu, aInfo.ActualShareCpu)
	aStats.historicActualSharePower = append(aStats.historicActualSharePower, aInfo.ActualSharePower)
	aStats.historicMaxSharePower = append(aStats.historicMaxSharePower, aInfo.MaxSharePower)

	aStats.sumActualShareCpu += aInfo.ActualShareCpu
	aStats.sumActualSharePower += aInfo.ActualSharePower
	aStats.sumMaxSharePower += aInfo.MaxSharePower
	// Calculating averages.
	aStats.avgActualShareCpu = aStats.sumActualShareCpu / float64(len(aStats.historicActualShareCpu))
	aStats.avgActualSharePower = aStats.sumActualSharePower / float64(len(aStats.historicActualSharePower))
	aStats.avgMaxSharePower = aStats.sumMaxSharePower / float64(len(aStats.historicMaxSharePower))
}

func (aStats *AllocationStats) String() string {
	avgActualShareCpuString := strconv.FormatFloat(aStats.avgActualShareCpu*100, 'f', 3, 64)
	avgActualSharePowerString := strconv.FormatFloat(aStats.avgActualSharePower, 'f', 3, 64)
	avgMaxSharePowerString := strconv.FormatFloat(aStats.avgMaxSharePower, 'f', 3, 64)
	return strings.Join([]string{avgActualShareCpuString, avgActualSharePowerString, avgMaxSharePowerString}, ",")
}

var amlInstance *AllocationMetricsListener

func allocationMetricsListenerBaseInstance() *AllocationMetricsListener {
	return &AllocationMetricsListener{
		eventTypes: []events.EventType{
			events.TASK_STATUS_UPDATE,
			events.TASK_OFFER_MATCH,
			events.POWER_SPECS_BROADCAST,
			events.RESOURCE_UTILIZATION_UPDATE,
		},
		resourceAllocationInfoCluster:        make(map[string]map[string]*AllocationInfo),
		resourceAllocationInfoClusterHistory: make(map[string]map[string]*AllocationInfo),
		fitTasks:                             make(map[string]map[string]*FitTaskInfo),
		statsForChan:                         make(chan statsFor),
		persistDataTimer:                     time.NewTicker(1 * time.Second),
		stop:                                 make(chan struct{}),
		eventListeners:                       make(map[events.EventType][]events.EventListener),
		externalEventData:                    make(map[events.EventType]events.EventData),
	}
}

func NewAllocationMetricsListener(lmt chan elecLogDef.LogMessageType,
	lmsg chan string) events.EventListener {
	if amlInstance != nil {
		return amlInstance
	}
	amlInstance = allocationMetricsListenerBaseInstance()
	amlInstance.logMsgType = lmt
	amlInstance.logMsg = lmsg
	// Starting to periodically persist allocation metrics data.
	amlInstance.startPersistingData()
	// Starting to persist allocation stats.
	amlInstance.startPersistingAllocationStats()
	return amlInstance
}

func newAllocationInfoInstance() *AllocationInfo {
	return &AllocationInfo{
		Stats: &AllocationStats{
			historicActualShareCpu:   make([]float64, 0, 600),
			historicActualSharePower: make([]float64, 0, 600),
			historicMaxSharePower:    make([]float64, 0, 600),
			sumActualShareCpu:        0.0,
			sumActualSharePower:      0.0,
			sumMaxSharePower:         0.0,
			avgActualShareCpu:        0.0,
			avgActualSharePower:      0.0,
			avgMaxSharePower:         0.0,
		},
	}
}

func GetAllocationMetricsListener() events.EventListener {
	return amlInstance
}

func (l *AllocationMetricsListener) GetEventTypes() []events.EventType {
	return l.eventTypes
}

// Retrieve allocation information of the currently running tasks and just fit tasks.
func (l *AllocationMetricsListener) GetAllAllocationMetricsHost(host string) (
	resAllocInfo map[string]AllocationInfo, fitTasksInfo map[string]FitTaskInfo) {

	var err error
	resAllocInfo, err = l.getResAllocationInfoHost(host)
	if err != nil {
		l.LogWarn(err.Error())
	}
	fitTasksInfo, err = l.getFitTasksInfoHost(host)
	if err != nil {
		l.LogWarn(err.Error())
	}
	return
}

// Returning a copy of the allocation information of currently running tasks on the given host.
func (l *AllocationMetricsListener) getResAllocationInfoHost(host string) (map[string]AllocationInfo, error) {
	l.Lock()
	defer l.Unlock()
	resAllocInfoHostCopy := make(map[string]AllocationInfo)
	var err error
	for taskID, allocInfo := range l.resourceAllocationInfoCluster[host] {
		var allocInfoCopy AllocationInfo
		copyErr := copier.Copy(&allocInfoCopy, allocInfo)
		if copyErr != nil {
			err = copyErr
		}
		resAllocInfoHostCopy[taskID] = allocInfoCopy
	}
	if len(resAllocInfoHostCopy) == 0 {
		return nil, errors.New(fmt.Sprintf("No allocation information available for running tasks on host %s", host))
	}
	if err != nil {
		return nil, errors.Wrap(err, "failed to get resource allocation information")
	}
	return resAllocInfoHostCopy, nil
}

// Returning a copy of information corresponding to the just fit tasks.
func (l *AllocationMetricsListener) getFitTasksInfoHost(host string) (map[string]FitTaskInfo, error) {
	l.Lock()
	defer l.Unlock()
	fitTasksHostCopy := make(map[string]FitTaskInfo)
	var err error
	for taskID, info := range l.fitTasks[host] {
		var infoCopy FitTaskInfo
		copyErr := copier.Copy(&infoCopy, info)
		if copyErr != nil {
			err = copyErr
		}
		fitTasksHostCopy[taskID] = infoCopy
	}
	if len(fitTasksHostCopy) == 0 {
		return nil, errors.New(fmt.Sprintf("No allocation information available for fit "+
			"but yet to start tasks on host %s", host))
	}
	if err != nil {
		return nil, errors.Wrap(err, "failed to get fit tasks information")
	}
	return fitTasksHostCopy, nil
}

func (l *AllocationMetricsListener) updateAllocationInfoAllRunningTasksOnHost(hostname string, taskID string) {
	powerSpecsFound := false
	var powerSpecs events.PowerSpecsBroadcastEventData
	resourceUtilFound := false
	var resourceUtil events.ResourceUtilizationUpdateEventData
	if d, found := l.externalEventData[events.POWER_SPECS_BROADCAST]; found {
		powerSpecs = d.Get().(events.PowerSpecsBroadcastEventData)
		powerSpecsFound = found
	}
	if d, found := l.externalEventData[events.RESOURCE_UTILIZATION_UPDATE]; found {
		resourceUtil = d.Get().(events.ResourceUtilizationUpdateEventData)
		resourceUtilFound = found
	}

	// Updating allocation information other tasks still running on host.
	if resAllocInfoHost, ok := l.resourceAllocationInfoCluster[hostname]; ok {
		if powerSpecsFound && resourceUtilFound {
			tdpHost := 0.0
			totalPowerCpu := 0.0
			if psHost, ok := powerSpecs[hostname]; ok {
				if ruHost, ok := resourceUtil[hostname]; ok {
					tdpHost = psHost.TDPWatts
					totalPowerCpu = ruHost.TotalPowerCpu
					for _, allocInfoTask := range l.resourceAllocationInfoCluster[hostname] {
						allocInfoTask.update(
							resAllocInfoHost,
							tdpHost,
							totalPowerCpu)

						// Notifying listeners about the update in allocation statistics
						// for task.
						l.NotifyListeners(events.ALLOCATION_STATS_UPDATE,
							events.NewAllocationStatsUpdateEventData(
								events.WithAvgActualShareCpu((*allocInfoTask.Stats).avgActualShareCpu),
								events.WithAvgActualSharePower((*allocInfoTask.Stats).avgActualSharePower),
								events.WithHostname(hostname),
								events.WithTaskID(taskID),
							))
					}
				}
			}
		}
	} else {
		// There are no other tasks running on host.
	}
}

func (l *AllocationMetricsListener) Update(et events.EventType, data events.EventData) error {
	l.Lock()
	defer l.Unlock()

	switch et {
	case events.TASK_OFFER_MATCH:
		switch data.(type) {
		case *events.TaskOfferMatchEventData:
			offerMatchData := data.Get().(events.TaskOfferMatchEventData)
			// A new task has been fit into an offer.
			host := offerMatchData.GetHostname()
			taskID := offerMatchData.GetTaskID()
			taskWatts, _ := def.WattsToConsider(offerMatchData.Task,
				offerMatchData.ClassMapWattsEnabled, offerMatchData.Offer)
			info := &FitTaskInfo{
				CPU:       offerMatchData.Task.GetCpu(),
				RAM:       offerMatchData.Task.GetRam(),
				Watts:     taskWatts,
				Tolerance: offerMatchData.Task.GetTolerance(),
			}

			// Adding this task to the fit tasks.
			if _, ok := l.fitTasks[host]; !ok {
				l.fitTasks[host] = map[string]*FitTaskInfo{
					taskID: info,
				}
			} else {
				l.fitTasks[host][taskID] = info
			}
		default:
			return &events.InvalidDataForEventError{
				GivenType:    reflect.TypeOf(data),
				RequiredType: reflect.TypeOf(events.TaskOfferMatchEventData{}),
			}
		}
	case events.TASK_STATUS_UPDATE:
		hostInfoListener := GetHostInfoListenerInstance().(*HostInfoListener)
		switch data.(type) {
		case *events.TaskStatusEventData:
			status := data.Get().(*mesos.TaskStatus)
			switch *status.State {
			case mesos.TaskState_TASK_RUNNING:
				// Task has started running.
				// TASK_STARTING is optional and hence, updating allocation information on TASK_RUNNING.
				// As a result, if a task fails prior to the receipt of TASK_RUNNING status update, then no
				// allocation information or stats would be recorded for the task.
				taskID := *status.TaskId.Value
				hostname, err := hostInfoListener.GetHostname(*status.SlaveId.Value)
				if err == nil {
					// New task being launched on corresponding host.
					newAllocInfo := newAllocationInfoInstance()
					newAllocInfo.Host = hostname
					newAllocInfo.CpuShare = l.fitTasks[hostname][taskID].CPU
					newAllocInfo.Tolerance = l.fitTasks[hostname][taskID].Tolerance
					if _, ok := l.resourceAllocationInfoCluster[hostname]; !ok {
						// Adding new entry for host.
						l.resourceAllocationInfoCluster[hostname] = map[string]*AllocationInfo{
							taskID: newAllocInfo,
						}
					} else {
						// Adding entry for new task running on host.
						l.resourceAllocationInfoCluster[hostname][taskID] = newAllocInfo
					}

					l.updateAllocationInfoAllRunningTasksOnHost(hostname, taskID)

					// Removing entry for task in FitTasks.
					delete(l.fitTasks[hostname], taskID)
					// If no more fit tasks for hosts, then removing entry for host.
					if len(l.fitTasks[hostname]) == 0 {
						delete(l.fitTasks, hostname)
					}
				} else {
					// Should not be here.
				}

			case mesos.TaskState_TASK_FINISHED,
				mesos.TaskState_TASK_FAILED,
				mesos.TaskState_TASK_ERROR,
				mesos.TaskState_TASK_KILLED,
				mesos.TaskState_TASK_LOST:
				// Terminal status message received for task.
				// Task is no longer running.
				// For all the tasks that are still running on the host, there is an increase
				// in the actual share of CPU resources that they now have access to.
				taskID := *status.TaskId.Value
				hostname, err := hostInfoListener.GetHostname(*status.SlaveId.Value)
				if err == nil {

					// Safety check to make sure that resource allocation info has been recorded for the task.
					// If not, then the task has failed even before TASK_RUNNING status was received. In such a
					// scenario, we have no allocation info to store in the history and no allocation stats to persist.
					if _, hasEntryForHost := l.resourceAllocationInfoCluster[hostname]; hasEntryForHost {
						if _, hasEntryForTaskID := l.resourceAllocationInfoCluster[hostname][taskID]; hasEntryForTaskID {
							// Moving entry for task to history.
							if _, ok := l.resourceAllocationInfoClusterHistory[hostname]; !ok {
								// Adding new entry for host in history.
								l.resourceAllocationInfoClusterHistory[hostname] = map[string]*AllocationInfo{
									taskID: l.resourceAllocationInfoCluster[hostname][taskID],
								}
							} else {
								l.resourceAllocationInfoClusterHistory[hostname][taskID] =
									l.resourceAllocationInfoCluster[hostname][taskID]
							}

							// Allocation stats to be persisted.
							allocStats := l.resourceAllocationInfoCluster[hostname][taskID].Stats.String()
							// Logging allocation stats to file.
							l.statsForChan <- statsFor{taskID: taskID, hostname: hostname, stats: allocStats}

							// Removing entry for completed task.
							delete(l.resourceAllocationInfoCluster[hostname], taskID)
							// Removing entry for host if no more tasks running on host.
							if len(l.resourceAllocationInfoCluster[hostname]) == 0 {
								delete(l.resourceAllocationInfoCluster, hostname)
							}

							l.updateAllocationInfoAllRunningTasksOnHost(hostname, taskID)
						}
					}

					// In scenarios where allocation information was not recorded for the task (if task fails prior
					// to receipt of TASK_RUNNING status update), the task needs to be removed from the set of fit tasks.
					delete(l.fitTasks[hostname], taskID)
					// If no more fit tasks for hosts, then removing entry for host.
					if len(l.fitTasks[hostname]) == 0 {
						delete(l.fitTasks, hostname)
					}

				} else {
					// Should not be here.
				}
			}

		default:
			return &events.InvalidDataForEventError{
				GivenType:    reflect.TypeOf(data),
				RequiredType: reflect.TypeOf(events.TaskStatusEventData{}),
			}
		}

	case events.POWER_SPECS_BROADCAST:
		switch data.(type) {
		case *events.PowerSpecsBroadcastEventData:
			powerSpecs := data.Get().(events.PowerSpecsBroadcastEventData)
			// Updating the latest specs for each host, if any.
			if curPowerSpecsData, ok := l.externalEventData[events.POWER_SPECS_BROADCAST]; ok {
				curData := curPowerSpecsData.Get().(events.PowerSpecsBroadcastEventData)
				for host, powerSpecsData := range powerSpecs {
					// Works as maps are passed by reference.
					curData[host] = powerSpecsData
				}
			} else {
				// Adding data for all hosts.
				l.externalEventData[events.POWER_SPECS_BROADCAST] = &powerSpecs
			}
		default:
			return &events.InvalidDataForEventError{
				GivenType:    reflect.TypeOf(data),
				RequiredType: reflect.TypeOf(events.PowerSpecsBroadcastEventData{}),
			}
		}

	case events.RESOURCE_UTILIZATION_UPDATE:
		switch data.(type) {
		case *events.ResourceUtilizationUpdateEventData:
			newResUtilData := data.Get().(events.ResourceUtilizationUpdateEventData)
			// Updating the latest resource utilization information of each host, if any.
			if _, ok := l.externalEventData[events.RESOURCE_UTILIZATION_UPDATE]; ok {
				for host, resUtilData := range newResUtilData {
					l.externalEventData[events.RESOURCE_UTILIZATION_UPDATE].
						Get().(events.ResourceUtilizationUpdateEventData)[host] = resUtilData

					// Note: power specifications should have been received for this host.
					powerSpecsHost, _ := l.externalEventData[events.POWER_SPECS_BROADCAST].
						Get().(events.PowerSpecsBroadcastEventData)[host]
					// Update the actual share of processing power that every task running on the
					// host gets.
					// Note: not updating actual share of cpu resource as that gets updated only
					// on the occurrence of TASK_OFFER_MATCH event.
					if rAIHost, ok := l.resourceAllocationInfoCluster[host]; ok {
						for taskID, allocInfoTask := range rAIHost {
							allocInfoTask.updatePowerResourceAllocation(
								resUtilData.TotalPowerCpu,
								powerSpecsHost.TDPWatts)

							// Notifying listeners about the update in allocation statistics
							// for task.
							l.NotifyListeners(events.ALLOCATION_STATS_UPDATE,
								events.NewAllocationStatsUpdateEventData(
									events.WithAvgActualShareCpu((*allocInfoTask.Stats).avgActualShareCpu),
									events.WithAvgActualSharePower((*allocInfoTask.Stats).avgActualSharePower),
									events.WithHostname(host),
									events.WithTaskID(taskID),
								))
						}
					}
				}
			} else {
				// Adding data for all hosts.
				l.externalEventData[events.RESOURCE_UTILIZATION_UPDATE] = data
			}
		default:
			return &events.InvalidDataForEventError{
				GivenType:    reflect.TypeOf(data),
				RequiredType: reflect.TypeOf(events.ResourceUtilizationUpdateEventData{}),
			}
		}

	default:
		return &events.InvalidEventTypeError{
			GivenType: et,
			RequiredTypes: strings.Join([]string{events.TASK_OFFER_MATCH.String(),
				events.TASK_STATUS_UPDATE.String()}, ","),
		}
	}
	return nil
}

func (l *AllocationMetricsListener) String() string {
	// Placeholder
	return ""
}

// Headers for task resource allocation metrics.
// The header corresponds to the format of the data persisted.
// header = <host1>:<taskID1>:<allocationInfo...>-<taskID2>:<allocationInfo...>|<host2>...
const allocationMetricsHeader = "<host1>:<taskID1>:CPU_SHARE,ACTUAL_SHARE_CPU(%),ACTUAL_SHARE_POWER(Watts),MAX_SHARE_POWER(Watts)-" +
	"<taskID2>:CPU_SHARE,ACTUAL_SHARE_CPU(%),ACTUAL_SHARE_POWER(Watts),MAX_SHARE_POWER(Watts)|<host2>..."

// Headers for task resource allocation stats.
// header = <host>:<taskID>:AVG_ACTUAL_SHARE_CPU,AVG_ACTUAL_SHARE_POWER,AVG_MAX_SHARE_POWER
const allocationStatsHeader = "<host>:<taskID>:AVG_ACTUAL_SHARE_CPU(%),AVG_ACTUAL_SHARE_POWER(Watts),AVG_MAX_SHARE_POWER(Watts)"

// Periodically persist allocation metrics to file.
func (l *AllocationMetricsListener) startPersistingData() {
	go func() {
		// Persisting the headers.
		l.persistAllocationMetricsHeaders()
		for {
			select {
			case <-l.persistDataTimer.C:
				l.persistAllocationMetricsData()
			case <-l.stop:
				return
			}
		}
	}()
}

// Encapsulating taskID and the name of the host on which it was launched.
type statsFor struct {
	taskID   string
	hostname string
	stats    string
}

// Persist allocation stats to file when task completes execution.
func (l *AllocationMetricsListener) startPersistingAllocationStats() {
	go func() {
		// Persisting the headers.
		l.persistAllocationStatsHeaders()
		for {
			select {
			case s := <-l.statsForChan:
				l.persistAllocationStats(s)
			case <-l.stop:
				return
			}
		}
	}()
}

// Write allocation metrics headers.
func (l *AllocationMetricsListener) persistAllocationMetricsHeaders() {
	l.logMsgType <- elecLogDef.ALLOCATION_METRICS
	l.logMsg <- allocationMetricsHeader
}

// Write allocation stats headers.
func (l *AllocationMetricsListener) persistAllocationStatsHeaders() {
	l.logMsgType <- elecLogDef.ALLOCATION_STATS
	l.logMsg <- allocationStatsHeader
}

// Write current allocation metrics to file.
func (l *AllocationMetricsListener) persistAllocationMetricsData() {
	l.Lock()
	defer l.Unlock()
	var valuesAllHosts []string
	for host, rAIHost := range l.resourceAllocationInfoCluster {
		var valuesTasks []string
		for taskID, aInfo := range rAIHost {
			// <taskID>:<allocationInfo...>
			valuesTasks = append(valuesTasks, strings.Join([]string{taskID, aInfo.String()}, ":"))
		}

		// Grouping hostname with metrics for all tasks running on that host.
		// <host>:<taskID1>:<allocationInfo...>-<taskID2>:<allocationInfo...>-<taskID3>...
		valueHost := strings.Join([]string{host, strings.Join(valuesTasks, "-")}, ":")
		valuesAllHosts = append(valuesAllHosts, valueHost)
	}
	line := strings.Join(valuesAllHosts, "|")
	l.logMsgType <- elecLogDef.ALLOCATION_METRICS
	l.logMsg <- line
}

// Write allocation stats to file.
func (l *AllocationMetricsListener) persistAllocationStats(s statsFor) {
	// Writing stats to file.
	if s.stats != "" {
		taskIDWithStatsString := strings.Join([]string{s.taskID, s.stats}, ":")
		line := strings.Join([]string{s.hostname, taskIDWithStatsString}, ":")
		l.logMsgType <- elecLogDef.ALLOCATION_STATS
		l.logMsg <- line
	}
}

func (*AllocationMetricsListener) WriteLogs() {}

func (l *AllocationMetricsListener) Stop() {
	// Stopping periodic persist of allocation metrics.
	// Adding a sleep of 1 second to make sure that the stats for the last task is persisted.
	time.Sleep(1)
	close(l.stop)
}

func (l *AllocationMetricsListener) Attach(listener events.EventListener) {
	for _, et := range listener.GetEventTypes() {
		if _, ok := l.eventListeners[et]; ok {
			l.eventListeners[et] = append(l.eventListeners[et], listener)
		} else {
			l.eventListeners[et] = []events.EventListener{listener}
		}
	}
}

func (l *AllocationMetricsListener) NotifyListeners(et events.EventType, d events.EventData) {
	go func() {
		for _, listener := range l.eventListeners[et] {
			if err := listener.Update(et, d); err != nil {
				l.LogError(err)
			}
		}
	}()
}

func (l *AllocationMetricsListener) LogError(e error) {
	l.logMsgType <- elecLogDef.ERROR
	l.logMsg <- e.Error()
}

func (l *AllocationMetricsListener) LogWarn(msg string) {
	l.logMsgType <- elecLogDef.WARNING
	l.logMsg <- msg
}
